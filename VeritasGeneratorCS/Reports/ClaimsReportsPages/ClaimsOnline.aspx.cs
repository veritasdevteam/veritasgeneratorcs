﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace VeritasGeneratorCS.Reports.ClaimsReportsPages
{
    public partial class ClaimsOnline : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            GetServerInfo();
            CheckToDo();
            if (!IsPostBack)
            {
                rdpStart.SelectedDate = DateTime.Today.AddDays(-7);
                rdpEnd.SelectedDate = DateTime.Today;
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
                rgClaim.Rebind();
            }
            if (hfError.Value == "Visible")
            {
                rwError.VisibleOnPageLoad = true;
            }
            else
            {
                rwError.VisibleOnPageLoad = false;
            }
        }
        
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO.clsDBO clTD = new clsDBO.clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnClaimsReports.Enabled = false;
            btnSalesReports.Enabled = false;
            btnAccountingReports.Enabled = false;
            btnCustomReports.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true) 
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true) 
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true) 
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true) 
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true) 
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true) 
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true) 
                {
                    btnSalesReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true) 
                {
                    btnAccountingReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true) 
                {
                    btnClaimsReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true) 
                {
                    btnCustomReports.Enabled = true;
                    btnReports.Enabled = true;
                }
            }
        }
        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }
        protected void btnUsers_Click(object sender, EventArgs e) {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e) {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e) {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e) {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e) {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSalesReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/salesreportssearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/claimsreports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e) {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountingReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/AccountingReports.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e) {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e) {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnPhoneReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/phonereports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnCustomReports_Click(object sender, EventArgs e)
        {

        }

        protected void rgClaim_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToCsv")
            {
                rgClaim.ExportSettings.ExportOnlyData = true;
                rgClaim.ExportSettings.IgnorePaging = true;
                rgClaim.ExportSettings.OpenInNewWindow = true;
                rgClaim.ExportSettings.UseItemStyles = true;
                rgClaim.ExportSettings.FileName = "ClaimsOnline";
                rgClaim.ExportSettings.Csv.FileExtension = "csv";
                rgClaim.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Comma;
                rgClaim.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
                rgClaim.MasterTableView.ExportToCSV();
            }
            if (e.CommandName == "ExportToExcel")
            {
                rgClaim.ExportSettings.ExportOnlyData = true;
                rgClaim.ExportSettings.IgnorePaging = true;
                rgClaim.ExportSettings.OpenInNewWindow = true;
                rgClaim.ExportSettings.UseItemStyles = true;
                rgClaim.ExportSettings.FileName = "ClaimsOnline";
                rgClaim.ExportSettings.Excel.FileExtension = "xlsx";
                rgClaim.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), GridExcelExportFormat.Xlsx.ToString());
                rgClaim.MasterTableView.ExportToExcel();
            }
            if (e.CommandName == "RebindGrid" || e.CommandName == "Sort")
            {
                RunQuery();
            }
        }

        protected void btnRun_Click(object sender, EventArgs e)
        {
            RunQuery();
        }

        private void RunQuery()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();

            SQL = "select cl.claimid, cl.claimno, contractno, d.DealerNo, d.DealerName, sc.ServiceCenterNo, sc.ServiceCenterName, cl.Credate, sum(cd.reqamt) as ReqAmt, case when vc.claimid is null then 'No' else 'Yes' end as OnlineSubmission from claim cl " +
                  "inner join claimdetail cd on cd.claimid = cl.claimid " +
                  "inner join contract c on c.contractid  = cl.contractid " +
                  "inner join ServiceCenter sc on sc.ServiceCenterID = cl.ServiceCenterID " +
                  "left join Veritasclaims.dbo.claim vc on vc.ServiceCenterID = cl.ServiceCenterID and vc.ProcessClaimDate = cl.CreDate " +
                  "inner join dealer d on d.Dealerid = c.Dealerid " +
                  "where cl.claimid > 0 ";
            if (rdpStart.SelectedDate != null)
            {
                SQL = SQL + "and cl.credate >= '" + rdpStart.SelectedDate + "' ";
            }
            if (rdpEnd.SelectedDate != null)
            {
                SQL = SQL + "and cl.credate <= '" + rdpEnd.SelectedDate + "' ";
            }
            if (chkAutoNation.Checked)
            {
                SQL = SQL + "and sc.dealerno like '2%' ";
            }
            if (chkOnline.Checked)
            {
                SQL = SQL + "and cl.claimno in (select claimno from Veritasclaims.dbo.claim) ";
            }
            SQL = SQL + "group by cl.claimid, cl.claimno, contractno, d.DealerNo, d.DealerName, cl.CreDate, sc.ServiceCenterNo, sc.ServiceCenterName, vc.ClaimID ";
            rgClaim.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString).Tables[0];
            rgClaim.Rebind();
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            lblRecordCount.Text = clR.RowCount().ToString();
        }

    }
}