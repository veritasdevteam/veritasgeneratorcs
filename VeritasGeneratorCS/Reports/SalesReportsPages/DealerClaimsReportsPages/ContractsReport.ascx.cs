﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace VeritasGeneratorCS.Reports.SalesReportsPages.DealerClaimsReportsPages
{
    public partial class ContractsReport : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            GetServerInfo();
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        
        protected void btnRun_Click(object sender, EventArgs e)
        {
            RunQuery();
        }

        private void RunQuery()
        {
            if (Session["DealerClaimsReportsSelectedDealer"] != null)
            {
                lblErrorMsg.Text = "";
                hfDealerID.Value = Session["DealerClaimsReportsSelectedDealer"].ToString();
                rgContracts.Rebind();
            }
            else
            {
                lblErrorMsg.Text = "You must select a dealer\b\r";
            }
        }
        protected void rgContracts_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToCsv")
            {
                rgContracts.ExportSettings.ExportOnlyData = false;
                rgContracts.ExportSettings.IgnorePaging = true;
                rgContracts.ExportSettings.OpenInNewWindow = true;
                rgContracts.ExportSettings.UseItemStyles = true;
                rgContracts.ExportSettings.FileName = "DealersContractsEarnAmt";
                rgContracts.ExportSettings.Csv.FileExtension = "csv";
                rgContracts.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Comma;
                rgContracts.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
                rgContracts.MasterTableView.ExportToCSV();
            }
            if (e.CommandName == "ExportToExcel")
            {
                rgContracts.ExportSettings.ExportOnlyData = false;
                rgContracts.ExportSettings.IgnorePaging = true;
                rgContracts.ExportSettings.OpenInNewWindow = true;
                rgContracts.ExportSettings.UseItemStyles = true;
                rgContracts.ExportSettings.FileName = "DealersContractsEarnAmt";
                rgContracts.ExportSettings.Excel.FileExtension = "xlsx";
                rgContracts.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), GridExcelExportFormat.Xlsx.ToString());
                rgContracts.MasterTableView.ExportToExcel();
            }
            if (e.CommandName == "RebindGrid" || e.CommandName == "Sort")
            {
                RunQuery();
            }
        }
        protected void rgContracts_DataBound(object sender, EventArgs e)
        {
            for (int cnt = 0; cnt <= rgContracts.Items.Count - 1; cnt++)
            {
                try
                {
                    rgContracts.Items[cnt]["EarnAmt"].Text = Convert.ToDouble(rgContracts.Items[cnt]["EarnAmt"].Text).ToString("N2");
                }
                catch
                {

                }
            }
        }

        protected void rgContracts_PreRender(object sender, EventArgs e)
        {
            if (rgContracts.IsExporting)
            {
                foreach (GridFilteringItem item in rgContracts.MasterTableView.GetItems(GridItemType.FilteringItem))
                {
                    item.Visible = false;
                }
            }
        }
    }
}