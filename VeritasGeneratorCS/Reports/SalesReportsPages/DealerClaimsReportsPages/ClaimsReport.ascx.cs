﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace VeritasGeneratorCS.Reports.SalesReportsPages.DealerClaimsReportsPages
{
    public partial class ClaimsReport : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            GetServerInfo();
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        
        protected void btnRun_Click(object sender, EventArgs e)
        {
            RunQuery();
        }

        private void RunQuery()
        {
            if (Session["DealerClaimsReportsSelectedDealer"] != null)
            {
                lblErrorMsg.Text = "";
                hfDealerID.Value = Session["DealerClaimsReportsSelectedDealer"].ToString();
                rgClaims.Rebind();
            }
            else
            {
                lblErrorMsg.Text = "You must select a dealer\b\r";
            }
        }
        protected void rgClaims_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToCsv")
            {
                rgClaims.ExportSettings.ExportOnlyData = false;
                rgClaims.ExportSettings.IgnorePaging = true;
                rgClaims.ExportSettings.OpenInNewWindow = true;
                rgClaims.ExportSettings.UseItemStyles = true;
                rgClaims.ExportSettings.FileName = "DealersClaimsReport";
                rgClaims.ExportSettings.Csv.FileExtension = "csv";
                rgClaims.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Comma;
                rgClaims.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
                rgClaims.MasterTableView.ExportToCSV();
            }
            if (e.CommandName == "ExportToExcel")
            {
                rgClaims.ExportSettings.ExportOnlyData = false;
                rgClaims.ExportSettings.IgnorePaging = true;
                rgClaims.ExportSettings.OpenInNewWindow = true;
                rgClaims.ExportSettings.UseItemStyles = true;
                rgClaims.ExportSettings.FileName = "DealersClaimsReport";
                rgClaims.ExportSettings.Excel.FileExtension = "xlsx";
                rgClaims.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), GridExcelExportFormat.Xlsx.ToString());
                rgClaims.MasterTableView.ExportToExcel();
            }
            if (e.CommandName == "RebindGrid" || e.CommandName == "Sort")
            {
                RunQuery();
            }
        }
        protected void rgClaims_DataBound(object sender, EventArgs e)
        {
            for (int cnt = 0; cnt <= rgClaims.Items.Count - 1; cnt++)
            {
                try
                {
                    rgClaims.Items[cnt]["PaidAmt"].Text = Convert.ToDouble(rgClaims.Items[cnt]["PaidAmt"].Text).ToString("N2");
                } 
                catch
                {

                }
            }
            
        }

        protected void rgClaims_PreRender(object sender, EventArgs e)
        {
            if (rgClaims.IsExporting)
            {
                foreach (GridFilteringItem item in rgClaims.MasterTableView.GetItems(GridItemType.FilteringItem))
                {
                    item.Visible = false;
                }
            }
        }
    }
}