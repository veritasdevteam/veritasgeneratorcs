﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractsReport.ascx.cs" Inherits="VeritasGeneratorCS.Reports.SalesReportsPages.DealerClaimsReportsPages.ContractsReport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="lblErrorMsg" runat="server" ForeColor="Red" Font-Bold="true" Text=""></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="btnRun" OnClick="btnRun_Click" runat="server" Text="Run" BackColor="#1eabe2" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgContracts">
                <telerik:RadGrid ID="rgContracts" OnPreRender="rgContracts_PreRender" OnItemCommand="rgContracts_ItemCommand" OnDataBound="rgContracts_DataBound" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                    AllowSorting="true" AllowPaging="true"  Width="99%" ShowFooter="true" DataSourceID="SqlDataSource1">
                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" PageSize="10" ShowFooter="true" CommandItemDisplay="Top">
                    <CommandItemSettings ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowAddNewRecordButton="false" />
                        <Columns>
                            <telerik:GridBoundColumn DataField="ContractNo" UniqueName="ContractNo" HeaderText="Contract Number"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="SaleDate" UniqueName="SaleDate" HeaderText="Sale Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Status" UniqueName="Status" HeaderText="Status"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="EarnAmt" UniqueName="EarnAmt" HeaderText="Earn Amount"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="false">
                        <Selecting AllowRowSelect="false" />
                        <Resizing EnableRealTimeResize="true" AllowColumnResize="true" ClipCellContentOnResize="true" ResizeGridOnColumnResize="true"/>
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:SqlDataSource ID="SqlDataSource1"
                ProviderName="System.Data.SqlClient" 
                SelectCommand="select con.ContractNo, con.SaleDate, con.Status, con.EarnAmt from Contract con 
                               where DealerID = @DealerID " runat="server">
                <SelectParameters>
                    <asp:ControlParameter ControlID="hfDealerID" Name="DealerID" PropertyName="Value" Type="Int32" />
                </SelectParameters>
                </asp:SqlDataSource>
            </telerik:RadAjaxPanel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfDealerID" runat="server" />

