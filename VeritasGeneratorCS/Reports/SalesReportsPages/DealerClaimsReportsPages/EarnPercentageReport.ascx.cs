﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace VeritasGeneratorCS.Reports.SalesReportsPages.DealerClaimsReportsPages
{
    public partial class EarnPercentageReport : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            GetServerInfo();
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        
        protected void btnRun_Click(object sender, EventArgs e)
        {
            RunQuery();
        }

        private void RunQuery()
        {
            if (Session["DealerClaimsReportsSelectedDealer"] != null)
            {
                lblErrorMsg.Text = "";
                hfDealerID.Value = Session["DealerClaimsReportsSelectedDealer"].ToString();
                rgEarnPer.Rebind();
            }
            else
            {
                lblErrorMsg.Text = "You must select a dealer\b\r";
            }
        }
        protected void rgEarnPer_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToCsv")
            {
                rgEarnPer.ExportSettings.ExportOnlyData = false;
                rgEarnPer.ExportSettings.IgnorePaging = true;
                rgEarnPer.ExportSettings.OpenInNewWindow = true;
                rgEarnPer.ExportSettings.UseItemStyles = true;
                rgEarnPer.ExportSettings.FileName = "DealerEarnPercentage";
                rgEarnPer.ExportSettings.Csv.FileExtension = "csv";
                rgEarnPer.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Comma;
                rgEarnPer.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
                rgEarnPer.MasterTableView.ExportToCSV();
            }
            if (e.CommandName == "ExportToExcel")
            {
                rgEarnPer.ExportSettings.ExportOnlyData = false;
                rgEarnPer.ExportSettings.IgnorePaging = true;
                rgEarnPer.ExportSettings.OpenInNewWindow = true;
                rgEarnPer.ExportSettings.UseItemStyles = true;
                rgEarnPer.ExportSettings.FileName = "DealerEarnPercentage";
                rgEarnPer.ExportSettings.Excel.FileExtension = "xlsx";
                rgEarnPer.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), GridExcelExportFormat.Xlsx.ToString());
                rgEarnPer.MasterTableView.ExportToExcel();
            }
            if (e.CommandName == "RebindGrid" || e.CommandName == "Sort")
            {
                RunQuery();
            }
        }
        protected void rgEarnPer_DataBound(object sender, EventArgs e)
        {
            for (int cnt = 0; cnt <= rgEarnPer.Items.Count - 1; cnt++)
            {
                try
                {
                    rgEarnPer.Items[cnt]["EarnAmtSum"].Text = Convert.ToDouble(rgEarnPer.Items[cnt]["EarnAmtSum"].Text).ToString("N2");
                    rgEarnPer.Items[cnt]["ClaimsPaidAmt"].Text = Convert.ToDouble(rgEarnPer.Items[cnt]["ClaimsPaidAmt"].Text).ToString("N2");
                    rgEarnPer.Items[cnt]["EarnPerAmt"].Text = Convert.ToDouble(rgEarnPer.Items[cnt]["EarnPerAmt"].Text).ToString("P2");
                }
                catch
                { 

                } 
            }
        }

        protected void rgEarnPer_PreRender(object sender, EventArgs e)
        {
            if (rgEarnPer.IsExporting)
            {
                foreach (GridFilteringItem item in rgEarnPer.MasterTableView.GetItems(GridItemType.FilteringItem))
                {
                    item.Visible = false;
                }
            }
        }
    }
}