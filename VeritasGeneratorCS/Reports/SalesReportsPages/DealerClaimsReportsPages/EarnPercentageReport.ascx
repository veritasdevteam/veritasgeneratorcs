﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EarnPercentageReport.ascx.cs" Inherits="VeritasGeneratorCS.Reports.SalesReportsPages.DealerClaimsReportsPages.EarnPercentageReport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="lblErrorMsg" runat="server" ForeColor="Red" Font-Bold="true" Text=""></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="btnRun" OnClick="btnRun_Click" runat="server" Text="Run" BackColor="#1eabe2" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgEarnPer">
                <telerik:RadGrid ID="rgEarnPer" OnPreRender="rgEarnPer_PreRender" OnItemCommand="rgEarnPer_ItemCommand" OnDataBound="rgEarnPer_DataBound" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="false" 
                    AllowSorting="false" AllowPaging="true" Width="99%" ShowFooter="true" DataSourceID="SqlDataSource1">
                    <GroupingSettings CaseSensitive="false" />
                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="false" PageSize="10" ShowFooter="true" ShowHeader="true" CommandItemDisplay="Top">
                    <CommandItemSettings ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowAddNewRecordButton="false" />
                        <Columns>
                            <telerik:GridBoundColumn DataField="EarnAmtSum" UniqueName="EarnAmtSum" HeaderText="Earn Amt" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ClaimsPaidAmt" UniqueName="ClaimsPaidAmt" HeaderText="Claims Amt" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="EarnPerAmt" UniqueName="EarnPerAmt" HeaderText="Earn Per" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="false">
                        <Selecting AllowRowSelect="false" />
                        <Resizing EnableRealTimeResize="true" AllowColumnResize="true" ClipCellContentOnResize="true" ResizeGridOnColumnResize="true"/>
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:SqlDataSource ID="SqlDataSource1"
                ProviderName="System.Data.SqlClient" 
                SelectCommand="select EarnAmtSum, ClaimsPaidAmt, CAST(ROUND(ClaimsPaidAmt*1.00/EarnAmtSum, 5) AS FLOAT) as [EarnPerAmt] from ( 
                               select (select sum(EarnAmt) from Contract 
                               where DealerID = @DealerID) as EarnAmtSum, (select sum(query.PaidAmt) from (select cl.ClaimNo, cl.Status, cl.LossDate, sum(cd.PaidAmt) as PaidAmt from Claim cl 
                               join ClaimDetail cd on cl.claimid = cd.claimid 
                               where cl.ContractID in (select clm.ContractID from claim clm 
                               join Contract con on clm.ContractID = con.ContractID 
                               where con.DealerID = @DealerID) 
                               group by cl.ClaimNo, cl.status, cl.LossDate) query) as ClaimsPaidAmt) as EarnAndClaims " runat="server">
                <SelectParameters>
                    <asp:ControlParameter ControlID="hfDealerID" Name="DealerID" PropertyName="Value" Type="Int32" />
                </SelectParameters>
                </asp:SqlDataSource>
            </telerik:RadAjaxPanel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfDealerID" runat="server" />

