﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace VeritasGeneratorCS.Reports.SalesReports
{
    public partial class ContractBucket : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            GetServerInfo();
            CheckToDo();
            if (!IsPostBack)
            {
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                rdpStart.SelectedDate = DateTime.Today.AddDays(-7);
                rdpEnd.SelectedDate = DateTime.Today;
                FillContractType();
                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
            }
        }
        private void FillContractType()
        {
            cboContractType.Items.Add("All");
            cboContractType.Items.Add("Red Shield");
            cboContractType.Items.Add("Veritas");
            cboContractType.Items.Add("Express Protect");
            cboContractType.Items.Add("AutoNation");
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO.clsDBO clTD = new clsDBO.clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnClaimsReports.Enabled = false;
            btnSalesReports.Enabled = false;
            btnAccountingReports.Enabled = false;
            btnCustomReports.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true) 
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true) 
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true) 
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true) 
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true) 
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true) 
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true) 
                {
                    btnSalesReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true) 
                {
                    btnAccountingReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true) 
                {
                    btnClaimsReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true) 
                {
                    btnCustomReports.Enabled = true;
                    btnReports.Enabled = true;
                }
            }
        }

        protected void rgClaim_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToExcel")
            {
                rgClaim.ExportSettings.ExportOnlyData = false;
                rgClaim.ExportSettings.IgnorePaging = true;
                rgClaim.ExportSettings.OpenInNewWindow = true;
                rgClaim.ExportSettings.UseItemStyles = true;
                rgClaim.ExportSettings.Excel.FileExtension = "xlsx";
                rgClaim.ExportSettings.FileName = "R0018";
                rgClaim.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), GridExcelExportFormat.Xlsx.ToString());
                rgClaim.MasterTableView.ExportToExcel();
            }
            if (e.CommandName == "RebindGrid" || e.CommandName == "Sort")
            {
                //FillGrid();
            }
        }
        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }
        protected void btnUsers_Click(object sender, EventArgs e) {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e) {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e) {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e) {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e) {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSalesReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/salesreportssearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/claimsreports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e) {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountingReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/AccountingReports.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e) {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e) {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnPhoneReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/phonereports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnCustomReports_Click(object sender, EventArgs e)
        {

        }

        protected void btnRunReport_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select c.contractid, d.dealerno, d.dealername, d.state as DealerSTate, c.datepaid, a.agentno, a.AgentName, c.CustomerCost,  " +
                  "c.status, p.programname, pt0.PlanType, c.termmile, c.termmonth, " +
                  "case when dcc.dealercost is null then MoxyDealerCost else dcc.dealercost end as DealerCost, " +
                  "case when cr.amt is null then 0 else cr.amt end as ClaimReserve, " +
                  "case when sr.amt is null then 0 else sr.amt end as ReserveSurcharge, " +
                  "case when hl.amt is null then 0 else hl.amt end as Highline, " +
                  "case when awd.amt is null then 0 else awd.amt end as AWD, " +
                  "case when diesel.amt is null then 0 else diesel.amt end as Diesel, " +
                  "case when t.amt is null then 0 else t.amt end as Turbo, " +
                  "case when comm.amt is null then 0 else comm.amt end as Commercial, " +
                  "case when ab.amt is null then 0 else ab.amt end as AirBlatter, " +
                  "case when hyd.amt is null then 0 else hyd.amt end as Hydraulic, " +
                  "case when lux.amt is null then 0 else lux.amt end as Luxury, " +
                  "case when hyb.amt is null then 0 else hyb.amt end as Hybrid, " +
                  "case when lk.amt is null then 0 else lk.amt end as LiftKit, " +
                  "case when lkl.amt is null then 0 else lkl.amt end as LargeLiftKit, " +
                  "case when sg.amt is null then 0 else sg.amt end as Seals, " +
                  "case when sp.amt is null then 0 else sp.amt end as SnowPlow, " +
                  "case when ft.amt is null then 0 else ft.amt end as FullTerm, " +
                  "case when lt.amt is null then 0 else lt.amt end as LongTerm, " +
                  "case when lmf.amt is null then 0 else lmf.amt end as LMF, " +
                  "case when ded0.amt is null then 0 else ded0.amt end as Ded0, " +
                  "case when ded50.amt is null then 0 else ded50.amt end as Ded50, " +
                  "case when af.amt is null then 0 else af.amt end as AdminFee, " +
                  "case when r.amt is null then 0 else r.amt end as Roadside, " +
                  "case when s.amt is null then 0 else s.amt end as Software, " +
                  "case when m.amt is null then 0 else m.amt end as Moxy, " +
                  "case when pt.amt is null then 0 else pt.amt end as PremTax, " +
                  "case when cy.amt is null then 0 else cy.amt end as ContingencyFee, " +
                  "case when cf.amt is null then 0 else cf.amt end as ClipFee, " +
                  "case when o.amt is null then 0 else o.amt end as ObligorFee, " +
                  "case when pif.amt is null then 0 else pif.amt end as PIF, " +
                  "case when dcc.dealercost is null then MoxyDealerCost else dcc.dealercost end " +
                  "- case when cr.amt is null then 0 else cr.amt end " +
                  "- case when sr.amt is null then 0 else sr.amt end " +
                  "- case when af.amt is null then 0 else af.amt end " +
                  "- case when r.amt is null then 0 else r.amt end " +
                  "- case when s.amt is null then 0 else s.amt end " +
                  "- case when cy.amt is null then 0 else cy.amt end " +
                  "- case when cf.amt is null then 0 else cf.amt end " +
                  "- case when o.amt is null then 0 else o.amt end " +
                  "- case when m.amt is null then 0 else m.amt end " +
                  "- case when pif.amt is null then 0 else pif.amt end " +
                  "- case when awd.amt is null then 0 else awd.amt end " +
                  "- case when diesel.amt is null then 0 else diesel.amt end " +
                  "- case when t.amt is null then 0 else t.amt end " +
                  "- case when comm.amt is null then 0 else comm.amt end " +
                  "- case when ded0.amt is null then 0 else ded0.amt end " +
                  "- case when ded50.amt is null then 0 else ded50.amt end " +
                  "- case when ft.amt is null then 0 else ft.amt end " +
                  "- case when lt.amt is null then 0 else lt.amt end " +
                  "- case when lmf.amt is null then 0 else lmf.amt end " +
                  "- case when ab.amt is null then 0 else ab.amt end " +
                  "- case when hyd.amt is null then 0 else hyd.amt end " +
                  "- case when lux.amt is null then 0 else lux.amt end " +
                  "- case when hyb.amt is null then 0 else hyb.amt end " +
                  "- case when lk.amt is null then 0 else lk.amt end " +
                  "- case when sg.amt is null then 0 else sg.amt end " +
                  "- case when sp.amt is null then 0 else sp.amt end " +
                  "- case when hl.amt is null then 0 else hl.amt end " +
                  "- case when lkl.amt is null then 0 else lkl.amt end " +
                  "- case when pt.amt is null then 0 else pt.amt end as Commission " +
                  "from contract c " +
                  "left join vwclaimreserve cr on cr.contractid = c.ContractID " +
                  "left join vwSurchargeReserve sr on sr.contractid = c.contractid " +
                  "left join vwAdminFee AF on af.contractid = c.ContractID " +
                  "left join vwDealerCostCancel dcc on dcc.contractid = c.contractid " +
                  "left join vwRoadside R on r.contractid = c.ContractID " +
                  "left join vwSoftware S on s.contractid = c.ContractID " +
                  "left join vwPremTax PT on pt.contractid = c.ContractID " +
                  "left join vwContingency CY on cy.contractid = c.ContractID " +
                  "left join vwClipFee CF on cf.contractid = c.contractid " +
                  "left join vwObligorFee O on o.contractid = c.contractid " +
                  "left join vwMoxy M on m.contractid = c.ContractID " +
                  "left join vwPIF pif on pif.contractid = c.contractid " +
                  "left join vwAWD AWD on awd.contractid = c.ContractID " +
                  "left join vwDiesel Diesel on diesel.contractid = c.ContractID " +
                  "left join vwTurbo T on t.contractid = c.contractid " +
                  "left join vwCommercial comm on comm.contractid = c.contractid " +
                  "left join vwDed0 Ded0 on ded0.contractid = c.contractid " +
                  "left join vwDed50 Ded50 on ded50.contractid = c.contractid " +
                  "left join vwFullTerm FT on ft.contractid = c.contractid " +
                  "left join vwLongTerm LT on lt.contractid = c.contractid " +
                  "left join vwLMF lmf on lmf.contractid = c.contractid " +
                  "left join vwAirBlatter ab on ab.contractid = c.contractid " +
                  "left join vwHydraulic hyd on hyd.contractid = c.contractid " +
                  "left join vwLuxury lux on lux.contractid = c.contractid " +
                  "left join vwHybrid Hyb on hyb.contractid = c.contractid " +
                  "left join vwLiftKit lk on lk.contractid = c.contractid " +
                  "left join vwSeals SG on sg.contractid = c.contractid " +
                  "left join vwSnow SP on sp.contractid = c.contractid " +
                  "left join vwHighLine hl on hl.contractid = c.contractid " +
                  "left join vwLargeLiftKit lkl on lkl.contractid = c.contractid " +
                  "left join dealer d on d.dealerid = c.dealerid " +
                  "left join program p on p.ProgramID = c.ProgramID " +
                  "left join plantype pt0 on pt0.PlanTypeID = c.PlanTypeID " +
                  "left join Agents A on a.AgentID = c.AgentsID " +
                  "where not c.ProgramID in (47,48,51,54,55,56,57,58,59,60) ";
            if (rdpStart.SelectedDate != null) {
                SQL = SQL + "and c.datepaid >= '" + rdpStart.SelectedDate + "' ";
            }
            if (rdpEnd.SelectedDate != null)
            {
                SQL = SQL + "and c.datepaid <= '" + rdpEnd.SelectedDate + "' ";
            }
            if (cboContractType.Text == "Red Shield") {
                SQL = SQL + "and (contractno like 'r%' or contractno like 'Vel%') ";
            }
            if (cboContractType.Text == "Veritas") {
                SQL = SQL + "and (contractno like 'v%' and not contractno like 'Vel%') ";
            }
            if (cboContractType.Text == "Express Protect") {
                SQL = SQL + "and dealerno like '2%' and dealerno like '%cc' ";
            }
            if (cboContractType.Text == "AutoNation") {
                SQL = SQL + "and dealerno like '2%' and not dealerno like '%cc' ";
            }
            SQL = SQL + "order by SaleDate desc ";
            rgClaim.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString).Tables[0];
            rgClaim.Rebind();
        }
    }
}