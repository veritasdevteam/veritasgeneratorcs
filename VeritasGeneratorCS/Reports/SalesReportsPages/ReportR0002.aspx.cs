﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.GridExcelBuilder;

namespace VeritasGeneratorCS.Reports.SalesReports
{
    public partial class ReportR0002 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            dsSales.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            GetServerInfo();
            CheckToDo();
            if (!IsPostBack)
            {
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
            }
            dsSales.SelectCommand = hfSQL.Value;
            if (hfError.Value == "Visible")
            {
                rwError.VisibleOnPageLoad = true;
            }
            else
            {
                rwError.VisibleOnPageLoad = false;
            }
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO.clsDBO clTD = new clsDBO.clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnClaimsReports.Enabled = false;
            btnSalesReports.Enabled = false;
            btnAccountingReports.Enabled = false;
            btnCustomReports.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true) 
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true) 
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true) 
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true) 
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true) 
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true) 
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true) 
                {
                    btnSalesReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true) 
                {
                    btnAccountingReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true) 
                {
                    btnClaimsReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true) 
                {
                    btnCustomReports.Enabled = true;
                    btnReports.Enabled = true;
                }
            }
        }
        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void rgReport_ExcelMLWorkBookCreated(object sender, Telerik.Web.UI.GridExcelBuilder.GridExcelMLWorkBookCreatedEventArgs e)
        {
            foreach (RowElement row in e.WorkBook.Worksheets[0].Table.Rows)
            {
                row.Cells[0].StyleValue = "Style1";
            }

            StyleElement style = new StyleElement("Style1");
            style.InteriorStyle.Pattern = InteriorPatternType.Solid;
            style.InteriorStyle.Color = System.Drawing.Color.LightGray;
            e.WorkBook.Styles.Add(style);
        }

        protected void rgReport_InsertCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToCsv")
            {
                rgReport.ExportSettings.ExportOnlyData = true;
                rgReport.ExportSettings.IgnorePaging = true;
                rgReport.ExportSettings.OpenInNewWindow = true;
                rgReport.ExportSettings.FileName = "R002";
                rgReport.ExportSettings.Csv.FileExtension = "csv";
                rgReport.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Comma;
                rgReport.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
                rgReport.MasterTableView.ExportToCSV();
            }
            if (e.CommandName == "ExportToExcel")
            {
                rgReport.ExportSettings.ExportOnlyData = true;
                rgReport.ExportSettings.IgnorePaging = true;
                rgReport.ExportSettings.OpenInNewWindow = true;
                rgReport.ExportSettings.Excel.FileExtension = "xlsx";
                rgReport.ExportSettings.FileName = "R002";
                rgReport.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), GridExcelExportFormat.Xlsx.ToString());
                rgReport.MasterTableView.ExportToExcel();
            }
            if (e.CommandName == "RebindGrid" || e.CommandName == "Sort")
            {
                rgReport.Rebind();
            }
        }
        protected void btnUsers_Click(object sender, EventArgs e) {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e) {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e) {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e) {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e) {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSalesReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/salesreportssearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/claimsreports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e) {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountingReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/AccountingReports.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e) {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e) {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnPhoneReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/phonereports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnCustomReports_Click(object sender, EventArgs e)
        {

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtCustState.Text = "";
            txtDealerNo.Text = "";
            txtDealerName.Text = "";
            txtVIN.Text = "";
            txtMake.Text = "";
            txtModel.Text = "";
            rdpSoldEnd.SelectedDate = null;
            rdpSoldStart.SelectedDate = null;
            rdpPaidDate.SelectedDate = null;
            rdpPaidEnd.SelectedDate = null;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select c.ContractID, CustomerID, " +
                  "CustomerFName, CustomerLName, CustomerAddr1, CustomerAddr2, CustomerCity, CustomerState, CustomerZip, " +
                  "CustomerPhone, AgentName, MGA, DealerNo, DealerName, ec.DealerCost, ec.CustomerCost, ec.Markup, TotalSurcharge, " +
                  "AgentFee, ClassFee, PPAFee, VSCPlan, SoldDate, Deductible, FINTerm, ec.TermMonth, ec.TermMile, DecForm, TC, " +
                  "ec.Class, ec.VIN, ec.ContractNo, ec.Year, ec.Make, ec.Model, ec.Trim, surDiesel, surHydraulic, sur4WD, surTurbo, surHybrid, " +
                  "surCommercial, surAirBladder, surLiftKit, surLiftKit78, surLuxury, surSnowPlow, surSeals, DealStatus, " +
                  "PaidDate, PaidStatus, BasePrice, DateAdded, DateUpdated, SaleMiles, Program, ec.EffDate, CancelDate, " +
                  "ec.EffMile, CancelDate, PlanCode, AgentNo, c.DatePaid " +
                  "from VeritasMoxy.dbo.epcontract ec " +
                  "left join contract c on ec.contractno = c.contractno " +
                  "where c.contractid > 0 ";
            if (txtCustState.Text.Length > 0)
            {
                SQL = SQL + "and customerstate like '%" + txtCustState.Text + "%' ";
            }
            if (txtDealerNo.Text.Length > 0) 
            {
                SQL = SQL + "and dealerno like '%" + txtDealerNo.Text + "%' ";
            }
            if (txtDealerName.Text.Length > 0) 
            {
                SQL = SQL + "and dealername like '%" + txtDealerName.Text + "%' ";
            }
            if (txtVIN.Text.Length > 0) 
            {
                SQL = SQL + "and ec.vin like '%" + txtVIN.Text + "%' ";
            }
            if (txtMake.Text.Length > 0) 
            {
                SQL = SQL + "and ec.make like '%" + txtMake.Text + "%' ";
            }
            if (txtModel.Text.Length > 0) 
            {
                SQL = SQL + "and ec.model like '%" + txtModel.Text + "%' ";
            }
            if (rdpSoldStart.SelectedDate != null) 
            {
                SQL = SQL + "and solddate >= '" + rdpSoldStart.SelectedDate + "' ";
            }
            if (rdpSoldEnd.SelectedDate != null) 
            {
                SQL = SQL + "and solddate < '" + DateTime.Parse(rdpSoldEnd.SelectedDate.ToString()).AddDays(1) + "' ";
            }
            if (rdpPaidDate.SelectedDate != null)
            {
                SQL = SQL + "and datepaid >= '" + rdpPaidDate.SelectedDate + "' ";
            }
            if (rdpPaidEnd.SelectedDate != null) {
                SQL = SQL + "and datepaid < '" + DateTime.Parse(rdpPaidEnd.SelectedDate.ToString()).AddDays(1) + "' ";
            }
            //rgReport.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            //rgReport.Rebind();
            dsSales.SelectCommand = SQL;
            rgReport.Rebind();
            hfSQL.Value = SQL;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            txtRecCnt.Text = long.Parse(clR.RowCount().ToString()).ToString("#,##0");
        }
    }
}