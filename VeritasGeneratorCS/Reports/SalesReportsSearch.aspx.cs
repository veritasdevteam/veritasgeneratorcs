﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Reports
{
    public partial class SalesReportsSearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            GetServerInfo();
            CheckToDo();
            if (!IsPostBack)
            {
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
            }

            if (hfError.Value == "Visible")
            {
                rwError.VisibleOnPageLoad = true;
            }
            else
            {
                rwError.VisibleOnPageLoad = false;
            }
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO.clsDBO clTD = new clsDBO.clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnClaimsReports.Enabled = false;
            btnSalesReports.Enabled = false;
            btnAccountingReports.Enabled = false;
            btnCustomReports.Enabled = false;
            btnPhoneReports.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true) 
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true) 
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true) 
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true) 
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("phonereports")) == true) 
                {
                    btnPhoneReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true) 
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true) 
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true) 
                {
                    btnSalesReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true) 
                {
                    btnAccountingReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true) 
                {
                    btnClaimsReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true) 
                {
                    btnCustomReports.Enabled = true;
                    btnReports.Enabled = true;
                }
            }
        }
        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }
        protected void btnUsers_Click(object sender, EventArgs e) {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e) {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e) {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e) {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e) {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSalesReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/salesreportssearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/claimsreports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e) {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountingReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/AccountingReports.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e) {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e) {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnPhoneReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/phonereports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnCustomReports_Click(object sender, EventArgs e)
        {

        }

        protected void rgSalesReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(rgSalesReport.SelectedValue) == 1) 
            {
                Response.Redirect("~/reports/SalesReportsPages/reportR0001.aspx?sid=" + hfID.Value + "&ReportID=" + rgSalesReport.SelectedValue);
            }
            if (Convert.ToInt32(rgSalesReport.SelectedValue) == 2) 
            {
                Response.Redirect("~/reports/SalesReportsPages/reportR0002.aspx?sid=" + hfID.Value + "&ReportID=" + rgSalesReport.SelectedValue);
            }
            if (Convert.ToInt32(rgSalesReport.SelectedValue) == 1003) 
            {
                Response.Redirect("~/reports/SalesReportsPages/r0004.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgSalesReport.SelectedValue) == 1004) 
            {
                Response.Redirect("~/reports/SalesReportsPages/r0005.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgSalesReport.SelectedValue) == 1005) 
            {
                Response.Redirect("~/reports/SalesReportsPages/r0006.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgSalesReport.SelectedValue) == 1009) 
            {
                Response.Redirect("~/reports/SalesReportsPages/r0010.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgSalesReport.SelectedValue) == 1013) 
            {
                Response.Redirect("~/reports/SalesReportsPages/Cancellations.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgSalesReport.SelectedValue) == 1016) 
            {
                Response.Redirect("~/reports/SalesReportsPages/PaidCancel.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgSalesReport.SelectedValue) == 1019) 
            {
                Response.Redirect("~/reports/SalesReportsPages/r0020.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgSalesReport.SelectedValue) == 1023) 
            {
                Response.Redirect("~/reports/SalesReportsPages/ContractSale.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgSalesReport.SelectedValue) == 1024) 
            {
                Response.Redirect("~/reports/SalesReportsPages/ContractsPaid.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgSalesReport.SelectedValue) == 1025) 
            {
                Response.Redirect("~/reports/SalesReportsPages/ContractBucket.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgSalesReport.SelectedValue) == 1050) 
            {
                Response.Redirect("~/reports/SalesReportsPages/CancellationShift.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgSalesReport.SelectedValue) == 1070)
            {
                Response.Redirect("~/reports/SalesReportsPages/FandIReport.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgSalesReport.SelectedValue) == 1071) 
            {
                Response.Redirect("~/reports/SalesReportsPages/DealerClaimsReports.aspx?sid=" + hfID.Value);
            }
            if (Convert.ToInt32(rgSalesReport.SelectedValue) == 1073)
            {
                Response.Redirect("~/reports/SalesReportsPages/DealerNoProduction.aspx?sid=" + hfID.Value);
            }
        }
    }
}