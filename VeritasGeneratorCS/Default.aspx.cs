﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;

public partial class Default : System.Web.UI.Page
{
    private long lUserID;
    private long lTeamID;
    private bool bManager;
    protected void Page_Load(object sender, EventArgs e)
    {
        string SQL;
        tbTodo.Width = pnlHeader.Width;
        if (!IsPostBack)
        {
            GetServerInfo();
            CheckToDo();
            LockButtons();
            if (lUserID == 0)
            {
                hfSigninPopup.Value = "Visible";
                string Script;
                Script = "function f(){$find(\"" + rwSignIn.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwSignIn", Script, true);
                rwSignIn.VisibleOnPageLoad = true;
                //LockButtons();
            }
            else
            {
                clsDBO.clsDBO clR = new clsDBO.clsDBO();
                SQL = "select * from usersecurityinfo where userid = " + lUserID;
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() > 0)
                {
                    clR.GetRow();
                    if (clR.GetFields("teamid") == "2" || clR.GetFields("teamid") == "3" || clR.GetFields("teamid") == "4" || clR.GetFields("teamid") == "11")
                    {
                        if (clR.GetFields("teamlead") == "False")
                        {
                            ucAgentDashboards.sID = hfID.Value;
                            ucAgentDashboards.Update();
                            pvAgentDashboard.Selected = true;
                        } 
                        else 
                        {
                            ucManagerDashboard.ID = hfID.Value;
                            ucManagerDashboard.Update();
                            pvManagementDashboard.Selected = true;
                        }
                    }
                }
            }
            UnlockButtons();
            if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
            {
                pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
            }
            else
            {
                pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
            }
        }
    }
    private void CheckToDo()
    {
        hlToDo.Visible = false;
        hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
        string SQL;
        clsDBO.clsDBO clTD = new clsDBO.clsDBO();
        SQL = "select * from usermessage " +
              "where toid = " + lUserID + " " +
              "and completedmessage = 0 " +
              "and deletemessage = 0 ";
        clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        if (clTD.RowCount() > 0)
        {
            hlToDo.Visible = true;
        }
        else
        {
            hlToDo.Visible = false;
        }
    }
    private void GetServerInfo()
    {
        string SQL;
        clsDBO.clsDBO clSI = new clsDBO.clsDBO();
        DateTime sStartDate;
        DateTime sEndDate;
        sStartDate = DateTime.Today;
        sEndDate = DateTime.Today.AddDays(1);
        hfID.Value = Request.QueryString["sid"];

        SQL = "select * from serverinfo " +
              "where systemid = '" + hfID.Value + "' " +
              "and signindate >= '" + sStartDate + "' " +
              "and signindate <= '" + sEndDate + "' ";
        clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        lUserID = 0;
        if (clSI.RowCount() > 0)
        {
            clSI.GetRow();
            lUserID = long.Parse(clSI.GetFields("userid"));
        }
    }
    private void LockButtons()
    {
        btnAccounting.Enabled = false;
        btnAgents.Enabled = false;
        btnClaim.Enabled = false;
        btnDealer.Enabled = false;
        btnContract.Enabled = false;
        btnSettings.Enabled = false;
        btnUsers.Enabled = false;
        btnUsers.Enabled = false;
        btnContract.Enabled = false;
        btnReports.Enabled = false;
    }
    private void UnlockButtons()
    {
        string SQL;
        clsDBO.clsDBO clSI = new clsDBO.clsDBO();
        SQL = "select * from usersecurityinfo where userid = " + lUserID;
        clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        if (clSI.RowCount() > 0)
        {
            clSI.GetRow();
            btnUsers.Enabled = true;
            if (Convert.ToBoolean(clSI.GetFields("accounting")) == true)
            {
                btnAccounting.Enabled = true;
            }
            if (Convert.ToBoolean(clSI.GetFields("Settings")) == true)
            {
                btnSettings.Enabled = true;
            }
            if (Convert.ToBoolean(clSI.GetFields("Agents")) == true)
            {
                btnAgents.Enabled = true;
            }
            if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true)
            {
                btnDealer.Enabled = true;
            }
            if (Convert.ToBoolean(clSI.GetFields("claim")) == true)
            {
                btnClaim.Enabled = true;
            }
            if (Convert.ToBoolean(clSI.GetFields("contract")) == true)
            {
                btnContract.Enabled = true;
            }
            if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true)
            {
                btnReports.Enabled = true;
            }
            if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true)
            {
                btnReports.Enabled = true;
            }
            if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true)
            {
                btnReports.Enabled = true;
            }
            if (Convert.ToBoolean(clSI.GetFields("customreports")) == true)
            {
                btnReports.Enabled = true;
            }
        }
    }
    protected void btnOK_Click(object sender, EventArgs e)
    {
        string SQL;
        lblError.Text = "";
        if (txtEMail.Text == "")
        {
            lblError.Text = "No E-Mail was entered.";
            ShowError();
            return;
        }
        if (txtPassword.Text == "")
        {
            lblError.Text = "No Password was entered.";
            ShowError();
            return;
        }
        SQL = "select * from userinfo " +
              "where email = '" + txtEMail.Text + "' " +
              "and password = '" + txtPassword.Text + "' " +
              "and active <> 0";
        clsDBO.clsDBO clU = new clsDBO.clsDBO();
        clU.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        if (clU.RowCount() == 0)
        {
            lblError.Text = "Cannot locate user name/password.";
            ShowError();
            return;
        }
        clU.GetRow();
        lUserID = long.Parse(clU.GetFields("userid"));
        hfID.Value = System.Guid.NewGuid().ToString();
        clsDBO.clsDBO clSI = new clsDBO.clsDBO();
        //'        SQL = "delete serverinfo where userid = " & lUserID
        //'clSI.RunSQL(SQL, sCON)
        SQL = "select * from serverinfo where userid = " + lUserID;
        clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        clSI.NewRow();
        clSI.SetFields("userid", lUserID.ToString());
        clSI.SetFields("systemid", hfID.Value);
        clSI.SetFields("signindate", DateTime.Now.ToString());
        clSI.AddRow();
        clSI.SaveDB();
        hfSigninPopup.Value = "";
        string script = "function f(){$find(\"" + rwSignIn.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        UnlockButtons();
        CheckToDo();

        clsDBO.clsDBO clR = new clsDBO.clsDBO();
        SQL = "select * from usersecurityinfo where userid = " + lUserID;
        clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        if (clR.RowCount() > 0)
        {
            clR.GetRow();
            if (clR.GetFields("teamid") == "2" || clR.GetFields("teamid") == "3" || clR.GetFields("teamid") == "4" || clR.GetFields("teamid") == "11")
            {
                if (clR.GetFields("teamlead") == "False")
                {
                    ucAgentDashboards.sID = hfID.Value;
                    ucAgentDashboards.Update();
                    pvAgentDashboard.Selected = true;
                } 
                else
                { 
                    ucManagerDashboard.ID = hfID.Value;
                    ucManagerDashboard.Update();
                    pvManagementDashboard.Selected = true;
                }
            }
        }
    }
    private void ShowError()
    {
        hfError.Value = "Visible";
        string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwError", script, true);
    }
    protected void btnErrorOK_Click(object sender, EventArgs e)
    {
        hfError.Value = "";
        string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        script = "function f(){$find(\"" + rwSignIn.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwSignIn", script, true);
        rwSignIn.VisibleOnPageLoad = true;
    }

    protected void btnHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/default.aspx?sid=" + hfID.Value);
    }
    protected void btnAgents_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
    }
    protected void btnDealer_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
    }

    protected void btnContract_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
    }

    protected void btnClaim_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
    }

    protected void btnAccounting_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
    }

    protected void btnReports_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
    }

    protected void btnSettings_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
    }

    protected void btnUsers_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
    }

    protected void btnLogOut_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/default.aspx");
    }

}


