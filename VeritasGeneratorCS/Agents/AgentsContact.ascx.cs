﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Agents
{
    public partial class AgentsContact : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            GetServerInfo();
            if (!IsPostBack)
            {
                pnlControl.Visible = true;
                pnlDetail.Visible = false;
                hfAgentID.Value = Request.QueryString["AgentID"];
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void rgAgentContact_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlControl.Visible = false;
            pnlDetail.Visible = true;
            hfAgentContactID.Value = rgAgentContact.SelectedValue.ToString();
            FillAgentContact();
        }
        private void FillAgentContact()
        {
            string SQL;
            clsDBO.clsDBO clAC = new clsDBO.clsDBO();
            SQL = "select * from agentscontact where agentcontactid = " + hfAgentContactID.Value;
            clAC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clAC.RowCount() > 0)
            {
                clAC.GetRow();
                txtEMail.Text = clAC.GetFields("email");
                txtFName.Text = clAC.GetFields("fname");
                txtLName.Text = clAC.GetFields("lname");
                txtPhone.Text = clAC.GetFields("phone");
                txtPosition.Text = clAC.GetFields("position");
            }
            else
            {
                txtEMail.Text = "";
                txtFName.Text = "";
                txtLName.Text = "";
                txtPhone.Text = "";
                txtPosition.Text = "";
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            pnlControl.Visible = false;
            pnlDetail.Visible = true;
            hfAgentContactID.Value = 0.ToString();
            FillAgentContact();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlControl.Visible = true;
            pnlDetail.Visible = false;
            rgAgentContact.Rebind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clAC = new clsDBO.clsDBO();
            SQL = "select * from agentscontact where agentcontactid = " + hfAgentContactID.Value;
            clAC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clAC.RowCount() == 0)
            {
                clAC.NewRow();
            }
            else
            {
                clAC.GetRow();
            }
            clAC.SetFields("agentid", hfAgentID.Value);
            clAC.SetFields("fname", txtFName.Text);
            clAC.SetFields("lname", txtLName.Text);
            clAC.SetFields("phone", txtPhone.Text);
            clAC.SetFields("email", txtEMail.Text);
            clAC.SetFields("position", txtPosition.Text);
            if (clAC.RowCount() == 0)
            {
                clAC.AddRow();
            }
            clAC.SaveDB();
            pnlControl.Visible = true;
            pnlDetail.Visible = false;
            rgAgentContact.Rebind();
        }

        protected void rgAgentContact_DataBound(object sender, EventArgs e)
        {
            int cnt;
            string PhoneNumber, FormattedPhoneNumber;
            for (cnt = 0; cnt <= rgAgentContact.Items.Count - 1; cnt++)
            {
                PhoneNumber = rgAgentContact.Items[cnt]["Phone"].Text;
                if (PhoneNumber.Length == 10)
                {
                    FormattedPhoneNumber = "(" + PhoneNumber.Substring(0, 3) + ") " + PhoneNumber.Substring(3,3) + "-" + PhoneNumber.Substring(6,4);
                    rgAgentContact.Items[cnt]["Phone"].Text = FormattedPhoneNumber;
                }
                else if (PhoneNumber.Length == 11)
                {
                    FormattedPhoneNumber = PhoneNumber.Substring(0,1) + " (" + PhoneNumber.Substring(1, 3) + ") " + PhoneNumber.Substring(4,3) + "-" + PhoneNumber.Substring(7,4);
                    rgAgentContact.Items[cnt]["Phone"].Text = FormattedPhoneNumber;
                }
                else if (PhoneNumber.Length == 7)
                {
                    FormattedPhoneNumber = PhoneNumber.Substring(0, 3) + "-" + PhoneNumber.Substring(3,4);
                    rgAgentContact.Items[cnt]["Phone"].Text = FormattedPhoneNumber;
                }
            }
        }
    }
}