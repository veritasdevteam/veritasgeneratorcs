﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Agents
{
    public partial class SubAgentModify : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsStates.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsAgentStatus.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            GetServerInfo();
            hfSubAgentID.Value = Request.QueryString["subagentID"];
            pnlModify.Visible = true;
            pnlSearch.Visible = false;
            if (!IsPostBack)
            {
                FillSubAgent();
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private void FillSubAgent()
        {
            string SQL;
            clsDBO.clsDBO clA = new clsDBO.clsDBO();
            SQL = "select * from Subagents where subagentid = " + hfSubAgentID.Value;
            clA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                txtAddr1.Text = clA.GetFields("addr1");
                txtAddr2.Text = clA.GetFields("addr2");
                txtSubAgentName.Text = clA.GetFields("subagentname");
                txtSubAgentNo.Text = clA.GetFields("subagentno");
                txtCity.Text = clA.GetFields("city");
                txtEMail.Text = clA.GetFields("email");
                txtPhone.Text = clA.GetFields("phone");
                txtContact.Text = clA.GetFields("contact");
                txtDBA.Text = clA.GetFields("dba");
                txtEIN.Text = clA.GetFields("ein");
                cboAgentStatus.SelectedValue = clA.GetFields("statusid");
                hfAgentID.Value = clA.GetFields("agentid");
                txtAgent.Text = Functions.GetAgentInfo(long.Parse(clA.GetFields("agentid")));
                cboState.SelectedValue = clA.GetFields("state");
                txtZip.Text = clA.GetFields("zip");
            }
            else
            {
                txtAddr1.Text = "";
                txtAddr2.Text = "";
                txtSubAgentName.Text = "";
                txtSubAgentNo.Text = "";
                txtCity.Text = "";
                txtDBA.Text = "";
                txtEIN.Text = "";
                txtContact.Text = "";
                hfAgentID.Value = 0.ToString();
                txtEMail.Text = "";
                txtAgent.Text = "";
                cboState.Text = "";
                txtZip.Text = "";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            FillSubAgent();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clA = new clsDBO.clsDBO();

            SQL = "select * from subagents where subagentid = " + hfSubAgentID.Value;
            clA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                clA.SetFields("moddate", DateTime.Today.ToString());
                clA.SetFields("modby", hfUserID.Value);
            }
            else
            {
                clA.NewRow();
                clA.SetFields("credate", DateTime.Today.ToString());
                clA.SetFields("creby", hfUserID.Value);
            }
            clA.SetFields("addr1", txtAddr1.Text);
            clA.SetFields("addr2", txtAddr2.Text);
            clA.SetFields("city", txtCity.Text);
            clA.SetFields("email", txtEMail.Text);
            clA.SetFields("subagentname", txtSubAgentName.Text);
            clA.SetFields("subagentno", txtSubAgentNo.Text);
            clA.SetFields("dba", txtDBA.Text);
            clA.SetFields("ein", txtEIN.Text);
            clA.SetFields("agentid", hfAgentID.Value);
            clA.SetFields("statusid", cboAgentStatus.SelectedValue);
            clA.SetFields("state", cboState.SelectedValue);
            clA.SetFields("phone", txtPhone.Text);
            clA.SetFields("contact", txtContact.Text);
            clA.SetFields("zip", txtZip.Text);
            if (clA.RowCount() == 0)
            {
                clA.AddRow();
            }
            clA.SaveDB();
            RedirectSubAgent();
        }

        private void RedirectSubAgent()
        {
            if (hfSubAgentID.Value != "0")
            {
                Response.Redirect("subagent.aspx?sid=" + hfID.Value + "&subAgentID=" + hfSubAgentID.Value);
            }
            else
            {
                Response.Redirect("subagent.aspx?sid=" + hfID.Value + "&subAgentID=" + GetSubAgentID());
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            pnlModify.Visible = false;
            pnlSearch.Visible = true;
        }

        protected void rgAgents_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfAgentID.Value = rgAgents.SelectedValue.ToString();
            txtAgent.Text = Functions.GetAgentInfo(long.Parse(hfAgentID.Value));
            pnlModify.Visible = true;
            pnlSearch.Visible = false;
        }
        private long GetSubAgentID()
        {
            string SQL;
            clsDBO.clsDBO clA = new clsDBO.clsDBO();
            SQL = "select max(subagentid) as subagentid from subagents " +
                  "where creby = " + hfUserID.Value + " " + "";
            clA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                return long.Parse(clA.GetFields("subagentid"));
            }
                return 0;
        }
    }
}