﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS
{
    public partial class ManagerDashboard : System.Web.UI.UserControl
    {
        public long UserID;
        public string ID;

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void Update()
        {
            hfID.Value = ID;
            GetServerInfo();
            GetTeamInfo();
            SetDashboard();
        }
        private void GetTeamInfo()
        {

        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        public void SetDashboard()
        {
            if (hfTeamID.Value.Length == 0)
            {
                return;
            }
            try
            {
                string sSQL;
                clsDBO.clsDBO clR = new clsDBO.clsDBO();
                htmlClaimsOpened.Visible = true;
                htmlInbound.Visible = true;
                htmlNumOfClaimsMovedFromOpen.Visible = true;
                htmlDnD.Visible = true;
                htmlOutBound.Visible = true;
                sSQL = "select ui.FName + ' ' +  ui.LName as names, ui.UserID from UserInfo ui left join UserSecurityInfo usi on usi.UserID = ui.UserID where TeamID =" + hfTeamID.Value + " and Active = 1";
                ddlAgents.DataSource = clR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                ddlAgents.DataBind();
                ddlAgents.DataTextField = "names";
                ddlAgents.DataValueField = "UserID";
                ddlAgents.DataBind();
                ddlAgents.Visible = true;
                btnRevertGraphs.Visible = true;
                btnChangeGraphs.Visible = true;

                sSQL = "select CallDate, (sum(InboundCt))/(COUNT(*)) as count from VeritasPhone.dbo.AgentDailyCallSummary " +
                    "where UserID in (select UserID from UserSecurityInfo where TeamID = " + hfTeamID.Value + " and TeamLead != 1) " +
                    "and CallDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
                    "and CallDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "' " +
                    "group by CallDate";
                htmlInbound.DataSource = clR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                sSQL = "select CallDate, (sum(OutboundCt))/(COUNT(*)) as count from VeritasPhone.dbo.AgentDailyCallSummary " +
                    "where UserID in (select UserID from UserSecurityInfo where TeamID = " + hfTeamID.Value + " and TeamLead != 1) " +
                    "and CallDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
                    "and CallDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "' " +
                    "group by CallDate";
                htmlOutBound.DataSource = clR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                sSQL = "select date, (sum(TotalDuration/60000))/(COUNT(*)) as count from VeritasPhone.dbo.AgentDnD " +
                    "where UserID in (select UserID from UserSecurityInfo where TeamID = " + hfTeamID.Value + " and TeamLead != 1) " +
                    "and date <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
                    "and date > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "' " +
                    "group by date";
                htmlDnD.DataSource = clR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                sSQL = "select CreDate, count(*) as numOfClaimsOpened,0 as goal " +
                    "from Claim where CreDate < '" + DateTime.Today + "' " +
                    "and CreDate > '" + DateTime.Today.AddDays(-8) + "'" +
                    "and CreBy in (select UserID from UserSecurityInfo where TeamID = " + hfTeamID.Value + " and TeamLead != 1) " +
                    "group by CreDate";
                htmlClaimsOpened.DataSource = clR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                sSQL = "select CloseDate, count(*) as numOfClaimsClosed,0 as goal " +
                    "from Claim where CloseDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
                    "and CloseDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "'" +
                    "and CloseBy in (select UserID from UserSecurityInfo where TeamID = " + hfTeamID.Value + " and TeamLead != 1) " +
                    "group by CloseDate";
                htmlNumOfClaimsMovedFromOpen.DataSource = clR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                htmlNumOfClaimsMovedFromOpen.DataBind();



                htmlInbound.ChartTitle.Text = "Inbound Average";
                htmlOutBound.ChartTitle.Text = "Outbound Average";
                htmlDnD.ChartTitle.Text = "DnD Average";
                htmlClaimsOpened.ChartTitle.Text = "Number of Opened Claims";

                htmlNumOfClaimsMovedFromOpen.ChartTitle.Text = "Number of Claims Moved From Open";
                sSQL = "select claimid, ClaimNo, CreDate,ModDate, DATEDIFF(DAY,CreDate,GETDATE()) as ClaimAge, ca.ActivityDesc, ui.fname + ' ' + ui.lname as Adjuster from Claim " +
                    "cl left join ClaimActivity ca on cl.ClaimActivityID = ca.ClaimActivityID " +
                    "left join userinfo ui on ui.userid = cl.CreBy " +
                    "where Status = 'open' " +
                    "and CreBy in (select userID from UserSecurityInfo where TeamID = " + hfTeamID.Value + ") order by ClaimAge desc ";
                rgClaimOpen.Columns[6].Visible = true;
                rgClaimOpen.DataSource = clR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                rgClaimOpen.DataBind();
                sSQL = "select claimid, ClaimNo, CreDate,ModDate, DATEDIFF(DAY,CreDate,GETDATE()) as ClaimAge, ca.ActivityDesc, ui.fname + ' ' + ui.lname as Adjuster " +
                       "from Claim cl left join ClaimActivity ca on cl.ClaimActivityID = ca.ClaimActivityID " +
                       "left join userinfo ui on ui.userid = cl.CreBy " +
                       "where cl.ClaimActivityID = 3 and status = 'Open' order by ClaimAge desc ";
                rgClaimManagment.DataSource = clR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                rgClaimManagment.DataBind();
                rgClaimManagment.Visible = true;
                htmlClaimsOpened.Visible = true;
                htmlNumOfClaimsMovedFromOpen.Visible = true;
                rgClaimManagment.MasterTableView.Caption = "Managers Claim Open";
            } 
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        protected void rgClaimOpen_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claim.aspx?sid=" + hfID.Value + "&ClaimID=" + Convert.ToString(rgClaimOpen.SelectedValue));
        }

        protected void rgClaimOpen_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            SetDashboard();
        }

        protected void rgClaimManagment_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claim.aspx?sid=" + hfID.Value + "&ClaimID=" + Convert.ToString(rgClaimManagment.SelectedValue));
        }

        protected void rgClaimManagment_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            SetDashboard();
        }

        protected void btnChangeGraphs_Click(object sender, EventArgs e)
        {
            string sSQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            string sStartDate;
            string sEndDate;
            sStartDate = DateTime.Today.ToString();
            sEndDate = DateTime.Today.AddDays(1).ToString();
            int iSelectedAgent;
            int iSelectedIndex;
            iSelectedAgent = Convert.ToInt32(ddlAgents.SelectedValue);
            iSelectedIndex = ddlAgents.SelectedIndex;
            string sSelectedAgent;
            sSelectedAgent = ddlAgents.SelectedItem.Text;
            sSQL = "select ui.FName + ' ' +  ui.LName as names, ui.UserID from UserInfo ui left join UserSecurityInfo usi on usi.UserID = ui.UserID where TeamID =" + hfTeamID.Value + " and Active = 1";
            ddlAgents.DataSource = clR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            ddlAgents.DataBind();
            ddlAgents.DataTextField = "names";
            ddlAgents.DataValueField = "UserID";
            ddlAgents.DataBind();
            ddlAgents.Visible = true;
            ddlAgents.SelectedIndex = iSelectedIndex;
            btnRevertGraphs.Visible = true;
            btnChangeGraphs.Visible = true;



            sSQL = "select UserID, CallDate,'Inbound Calls' as title ,InboundCt as count, " +
                    "(case when (select count(goal) from UserGoals where GoalsID = 1 and UserID = ia.UserID) = 0 then '0' else (select goal from UserGoals where GoalsID = 1 and UserID = ia.UserID) end )as goal " +
                    "from VeritasPhone.dbo.AgentDailyCallSummary ia where UserID = '" + iSelectedAgent + "' and CallDate < '" + DateTime.Today + "' and CallDate > '" + DateTime.Today.AddDays(-8) + "' and ((DATEPART(dw, CallDate) + @@DATEFIRST) % 7) NOT IN (0, 1)";
            htmlInbound.DataSource = clR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            sSQL = "select UserID, CallDate,'Outbound Calls' as title ,OutboundCt as count, " +
                    "(case when (select count(goal) from UserGoals where GoalsID = 2 and UserID = ia.UserID) = 0 then '0' else (select goal from UserGoals where GoalsID = 2 and UserID = ia.UserID) end )as goal " +
                    " from VeritasPhone.dbo.AgentDailyCallSummary ia where UserID = '" + iSelectedAgent + "' and CallDate < '" + DateTime.Today + "' and CallDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "' and ((DATEPART(dw, CallDate) + @@DATEFIRST) % 7) NOT IN (0, 1)";
            htmlOutBound.DataSource = clR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            sSQL = "select userID, date as CallDate, 'DnD Total' as title, (TotalDuration/60000) as count," +
                "(case when (select count(goal) from UserGoals where GoalsID = 3 and UserID = ia.UserID) = 0 then '0' else (select goal from UserGoals where GoalsID = 3 and UserID = ia.UserID) end )as goal " +
                " from VeritasPhone.dbo.AgentDnD ia where UserID = '" + iSelectedAgent + "' and date < '" + DateTime.Today + "' and date > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "' and ((DATEPART(dw, date) + @@DATEFIRST) % 7) NOT IN (0, 1)";
            htmlDnD.DataSource = clR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            sSQL = "select CreDate, count(*) as numOfClaimsOpened " +
                   "from Claim where CreDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
                    "and CreDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "'" +
                    "and CreBy in (" + iSelectedAgent + ") " +
                    "group by CreDate";
            htmlClaimsOpened.DataSource = clR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            sSQL = "select CloseDate, count(*) as numOfClaimsClosed " +
                "from Claim where CloseDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
                "and CloseDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "'" +
                "and CloseBy in (" + iSelectedAgent + ") " +
                "group by CloseDate";
            htmlNumOfClaimsMovedFromOpen.DataSource = clR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);

            htmlInbound.ChartTitle.Text = "Inbound Stats";
            htmlOutBound.ChartTitle.Text = "Outbound Average";
            htmlDnD.ChartTitle.Text = "DnD Average";
            htmlClaimsOpened.ChartTitle.Text = "Number of opened claims Stats";
            htmlNumOfClaimsMovedFromOpen.ChartTitle.Text = "Number of claims moved from open";
            sSQL = "select claimid, ClaimNo, CreDate,ModDate, DATEDIFF(DAY,CreDate,GETDATE()) as ClaimAge, ca.ActivityDesc, ui.fname + ' ' + ui.lname as Adjuster from Claim " +
               "cl left join ClaimActivity ca on cl.ClaimActivityID = ca.ClaimActivityID " +
               "left join userinfo ui on ui.userid = cl.CreBy " +
               "where Status = 'open' " +
               "and CreBy = " + iSelectedAgent + " order by ClaimAge desc ";
            htmlInbound.Visible = true;
            htmlOutBound.Visible = true;
            htmlDnD.Visible = true;
            rgClaimOpen.Visible = true;
            htmlClaimsOpened.Visible = true;
            htmlNumOfClaimsMovedFromOpen.Visible = true;
            rgClaimOpen.DataSource = clR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);

            sSQL = "select claimid, ClaimNo, CreDate,ModDate, DATEDIFF(DAY,CreDate,GETDATE()) as ClaimAge, ca.ActivityDesc, ui.fname + ' ' + ui.lname as Adjuster from Claim cl left join ClaimActivity ca on cl.ClaimActivityID = ca.ClaimActivityID left join userinfo ui on ui.userid = cl.CreBy where cl.ClaimActivityID = 3 order by ClaimAge desc ";
            rgClaimManagment.Visible = true;
            rgClaimManagment.DataSource = clR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            rgClaimManagment.MasterTableView.Caption = "Managers Claim Open";
            rgClaimOpen.MasterTableView.Caption = "Claims Open";
            try
            {
                //tables
                rgClaimOpen.DataBind();
                rgClaimManagment.DataBind();

                //graphs
                htmlInbound.DataBind();
                htmlOutBound.DataBind();
                htmlDnD.DataBind();
                htmlClaimsOpened.DataBind();
                htmlNumOfClaimsMovedFromOpen.DataBind();
            } 
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        protected void btnRevertGraphs_Click(object sender, EventArgs e)
        {
            SetDashboard();
        }
    }
}