﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DealerDocuments.ascx.cs" Inherits="VeritasGeneratorCS.Dealer.DealerDocuments" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Panel ID="pnlList" runat="server">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="Add Document" BackColor="#1eabe2" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <telerik:RadGrid ID="rgContractDocument" OnSelectedIndexChanged="rgContractDocument_SelectedIndexChanged" AutoGenerateColumns="false" runat="server" AllowSorting="true" AllowPaging="true" 
                    Width="1000" ShowFooter="true">
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                    <MasterTableView AutoGenerateColumns="false" DataKeyNames="DealerDocumentID" PageSize="10">
                        <Columns>
                            <telerik:GridBoundColumn DataField="DealerDocumentID"  ReadOnly="true" Visible="false" UniqueName="DealerDocumentID"></telerik:GridBoundColumn>
                            <telerik:GridHyperLinkColumn DataTextField="DocumentName" Target="_blank" DataNavigateUrlFields="DocumentLink" UniqueName="DocumentLink" HeaderText="Title"></telerik:GridHyperLinkColumn>
                            <telerik:GridBoundColumn DataField="DocumentDesc" UniqueName="DocumentDesc" HeaderText="Description"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>

<asp:Panel ID="pnlDetail" runat="server">
     <asp:Table runat="server">
         <asp:TableRow>
             <asp:TableCell>
                 <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Title:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtTitleDetail" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Description
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtDescDetail" Width="400" Height="100" TextMode="MultiLine" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                 </asp:Table>
             </asp:TableCell>
         </asp:TableRow>
         <asp:TableRow>
             <asp:TableCell>
                 <asp:Table runat="server">
                     <asp:TableRow>
                         <asp:TableCell Font-Bold="true">
                             Created By:
                         </asp:TableCell>
                         <asp:TableCell>
                             <asp:TextBox ID="txtCreBy" runat="server"></asp:TextBox>
                         </asp:TableCell>
                         <asp:TableCell Font-Bold="true">
                             Create Date:
                         </asp:TableCell>
                         <asp:TableCell>
                             <asp:TextBox ID="txtCreDate" runat="server"></asp:TextBox>
                         </asp:TableCell>
                         <asp:TableCell Font-Bold="true">
                             Modified By:
                         </asp:TableCell>
                         <asp:TableCell>
                             <asp:TextBox ID="txtModBy" runat="server"></asp:TextBox>
                         </asp:TableCell>
                         <asp:TableCell Font-Bold="true">
                             Modified Date:
                         </asp:TableCell>
                         <asp:TableCell>
                             <asp:TextBox ID="txtModDate" runat="server"></asp:TextBox>
                         </asp:TableCell>
                     </asp:TableRow>
                 </asp:Table>
             </asp:TableCell>
         </asp:TableRow>
         <asp:TableRow>
             <asp:TableCell HorizontalAlign="Right">
                 <asp:Table runat="server">
                     <asp:TableRow>
                         <asp:TableCell>
                               <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" Text="Close" BackColor="#1eabe2"/>
                         </asp:TableCell>
                         <asp:TableCell>
                               <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" Text="Delete" BackColor="#1eabe2"/>
                         </asp:TableCell>
                         <asp:TableCell>
                               <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" Text="Save" BackColor="#1eabe2"/>
                         </asp:TableCell>
                     </asp:TableRow>
                 </asp:Table>
             </asp:TableCell>
         </asp:TableRow>
     </asp:Table>
</asp:Panel>

<asp:Panel ID="pnlAdd" runat="server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="200px" Width="300px" PostBackControls="btnUpload">
        <asp:Table runat="server">
            <asp:TableRow>
                <asp:TableCell Font-Bold="true">
                    Title:
                </asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="txtDocName" runat="server"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Font-Bold="true">
                    Description
                </asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="txtDocDesc" Width="400" Height="100" TextMode="MultiLine" runat="server"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Font-Bold="true">
                    File Upload:
                </asp:TableCell>
                <asp:TableCell>
                    <asp:FileUpload runat="server" ID="FileUpload2" />
                    <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" Text="Upload" BackColor="#1eabe2"/>
                    <asp:Button ID="btnCloseAdd" runat="server" OnClick="btnCloseAdd_Click" Text="Close" BackColor="#1eabe2"/>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </telerik:RadAjaxPanel>
</asp:Panel>

<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfDocID" runat="server" />
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfDealerID" runat="server" />
<asp:HiddenField ID="hfDealerNo" runat="server" />

