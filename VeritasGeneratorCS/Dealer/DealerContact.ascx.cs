﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Dealer
{
    public partial class DealerContact : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsContactType.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsDealerContact.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            hfDealerID.Value = Session["DealerID"].ToString();
        }
    }
}