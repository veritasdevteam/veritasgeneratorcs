﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Fulfillment.aspx.cs" Inherits="VeritasGeneratorCS.FulFillment.Fulfillment" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="Veritas.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Fulfillment</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" BorderStyle="None" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSubAgent" OnClick="btnSubAgent_Click" runat="server" BorderStyle="None" Text="Sub Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnFulFillment" OnClick="btnFulFillment_Click" runat="server" BorderStyle="None" Text="Fulfillment"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agents.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnFulfillmentStock" OnClick="btnFulfillmentStock_Click" runat="server" BorderStyle="None" Text="Fulfillment Stock"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agents.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" BorderStyle="None" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" BorderStyle="None" Text="Dealerships"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" BorderStyle="None" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" BorderStyle="None" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" BorderStyle="None" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" BorderStyle="None" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" OnClick="btnSettings_Click" runat="server" BorderStyle="None" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" BorderStyle="None" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" BorderStyle="None" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:Table runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadTabStrip ID="TabStrip1" OnTabClick="TabStrip1_TabClick" runat="server" Skin="Glow">
                                                    <Tabs>
                                                        <telerik:RadTab Text="Pending Approval" Value="Approve"></telerik:RadTab>
                                                        <telerik:RadTab Text="Pending Shipment" Value="Pend"></telerik:RadTab>
                                                        <telerik:RadTab Text="Waiting on Supplies" Value="Wait"></telerik:RadTab>
                                                        <telerik:RadTab Text="Shipped" Value="Shipped"></telerik:RadTab>
                                                        <telerik:RadTab Text="Cancelled" Value="Cancel"></telerik:RadTab>
                                                    </Tabs>
                                                </telerik:RadTabStrip>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadGrid ID="rgOrder" AllowMultiRowSelection="true" AutoGenerateColumns="false" runat="server" AllowFilteringByColumn="true"
                                                    AllowSorting="true" AllowPaging="true"  Width="1200" ShowFooter="true" DataSourceID="dsOrder1">
                                                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="OrderID" PageSize="10" ShowFooter="true">
                                                        <Columns>
                                                            <telerik:GridBoundColumn DataField="OrderID" ReadOnly="true" Visible="false" UniqueName="OrderID"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="OrderNo" UniqueName="OrderNo" HeaderText="Order No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Name" UniqueName="Name" HeaderText="Ship To" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn> 
                                                            <telerik:GridBoundColumn DataField="OrderDate" UniqueName="OrderDate" HeaderText="Order Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="RequestByNo" UniqueName="RequestByNo" HeaderText="Request By No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="RequestBy" UniqueName="RequestBy" HeaderText="Request By" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ShipOrderStatus" UniqueName="ShipOrderStatus" HeaderText="Order Status" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ShipDate" UniqueName="ShipDate" HeaderText="Ship Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"></telerik:GridBoundColumn>
                                                            <telerik:GridClientSelectColumn UniqueName="ClientSelect"></telerik:GridClientSelectColumn>
                                                        </Columns>
                                                    </MasterTableView>
                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                        <Selecting AllowRowSelect="true" />
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                                <asp:SqlDataSource ID="dsOrder1"
                                                ProviderName="System.Data.SqlClient" runat="server" SelectCommand="Select orderid, orderNo, Name, orderdate, requestbyno, requestby, shiporderstatus, shipdate 
                                                    From shiporder so 
                                                    inner Join shiporderstatus sos on so.shiporderstatusid = sos.shiporderstatusid 
                                                    where so.test = 0 and so.shiporderstatusid = 1
                                                    order by orderdate desc"
                                                    ></asp:SqlDataSource>
                                                <asp:SqlDataSource ID="dsOrder2"
                                                ProviderName="System.Data.SqlClient" runat="server" SelectCommand="Select orderid, orderNo, Name, orderdate, requestbyno, requestby, shiporderstatus, shipdate 
                                                    From shiporder so 
                                                    inner Join shiporderstatus sos on so.shiporderstatusid = sos.shiporderstatusid 
                                                    where so.test = 0 and so.shiporderstatusid = 2
                                                    order by orderdate desc"
                                                    ></asp:SqlDataSource>
                                                <asp:SqlDataSource ID="dsOrder3"
                                                ProviderName="System.Data.SqlClient" runat="server" SelectCommand="Select orderid, orderNo, Name, orderdate, requestbyno, requestby, shiporderstatus, shipdate 
                                                    From shiporder so 
                                                    inner Join shiporderstatus sos on so.shiporderstatusid = sos.shiporderstatusid 
                                                    where so.test = 0 and so.shiporderstatusid = 3
                                                    order by orderdate desc"
                                                    ></asp:SqlDataSource>
                                                <asp:SqlDataSource ID="dsOrder4"
                                                ProviderName="System.Data.SqlClient" runat="server" SelectCommand="Select orderid, orderNo, Name, orderdate, requestbyno, requestby, shiporderstatus, shipdate 
                                                    From shiporder so 
                                                    inner Join shiporderstatus sos on so.shiporderstatusid = sos.shiporderstatusid 
                                                    where so.test = 0 and so.shiporderstatusid = 4
                                                    order by orderdate desc"
                                                    ></asp:SqlDataSource>
                                                <asp:SqlDataSource ID="dsOrder5"
                                                ProviderName="System.Data.SqlClient" runat="server" SelectCommand="Select orderid, orderNo, Name, orderdate, requestbyno, requestby, shiporderstatus, shipdate 
                                                    From shiporder so 
                                                    inner Join shiporderstatus sos on so.shiporderstatusid = sos.shiporderstatusid 
                                                    where so.test = 0 and so.shiporderstatusid = 5
                                                    order by orderdate desc"
                                                    ></asp:SqlDataSource>
                                                <asp:HiddenField runat="server" ID="hfSQL"/>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Right">
                                                <asp:Button ID="btnCancelOrder" OnClick="btnCancelOrder_Click" runat="server" BackColor="Red" Text="Cancel Orders" />&nbsp&nbsp<asp:Button ID="btnApproveOrders" OnClick="btnApproveOrders_Click" BackColor="Green" runat="server" Text="Approve Orders" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow runat="server">
                                            <asp:TableCell>
                                                <asp:Panel runat="server" ID="pnlOrder">
                                                    <asp:Table runat="server">
                                                        <asp:TableRow>
                                                            <asp:TableCell Font-Bold="true">
                                                                Authorized Date:
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:TextBox ID="txtAuthDate" ReadOnly="true" runat="server"></asp:TextBox>
                                                            </asp:TableCell>
                                                            <asp:TableCell Font-Bold="true">
                                                                Authorized By:
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:TextBox ID="txtAuthBy" ReadOnly="true" runat="server"></asp:TextBox>
                                                            </asp:TableCell>
                                                            <asp:TableCell Font-Bold="true">
                                                                Tracking No: 
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:TextBox ID="txtTrackingNo" ReadOnly="true" runat="server"></asp:TextBox>
                                                            </asp:TableCell>
                                                            <asp:TableCell Font-Bold="true">
                                                                Shipping Date:
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:TextBox ID="txtShipDate" ReadOnly="true" runat="server"></asp:TextBox>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell Font-Bold="true">
                                                                Ship Overnight?:
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:CheckBox ID="chkOvernight" OnCheckedChanged="chkOvernight_CheckedChanged" AutoPostBack="true" runat="server" />
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                <asp:Table runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell Font-Bold="true" VerticalAlign="Top">
                                                            Shipping Info:
                                                        </asp:TableCell>
                                                        <asp:TableCell VerticalAlign="Top">
                                                            <telerik:RadTextBox ID="txtShipInfo" ReadOnly="true" BorderStyle="None" TextMode="MultiLine" Width="240" Height="140" runat="server"></telerik:RadTextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell Font-Bold="true">
                                                            Confirmation E-Mail:
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtConfirmEmail" ReadOnly="true" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                                <asp:Table runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <telerik:RadGrid ID="rgItem" AutoGenerateColumns="false" runat="server"  AllowAutomaticUpdates="true"
                                                                AllowAutomaticDeletes="true" AllowSorting="true" AllowPaging="true" Height="300" Width="1200" ShowFooter="true" DataSourceID="dsItem">
                                                                <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="Bottom" EditMode="Batch" DataKeyNames="ItemID" ShowFooter="true"
                                                                    CommandItemSettings-ShowAddNewRecordButton="false" 
                                                                    CommandItemSettings-CancelChangesText="Cancel" 
                                                                    CommandItemSettings-SaveChangesText="Save">
                                                                    <Columns>
                                                                        <telerik:GridBoundColumn DataField="ItemID" ReadOnly="true" Visible="false" UniqueName="OrderID"></telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="ProductNo" UniqueName="ProductNo" HeaderText="Detail No"></telerik:GridBoundColumn> 
                                                                        <telerik:GridBoundColumn DataField="ProductName" UniqueName="ProductName" HeaderText="Product Name"></telerik:GridBoundColumn>
                                                                        <telerik:GridTemplateColumn ItemStyle-BackColor="LightPink" DefaultInsertValue="0" HeaderText="Qty" HeaderStyle-Width="60px" SortExpression="Qty" UniqueName="Qty" DataField="Qty">
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" ID="lblQty" Text='<%# Eval("Qty") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <EditItemTemplate>
                                                                                <telerik:RadNumericTextBox RenderMode="Lightweight" Width="55px" runat="server" ID="tbQty"></telerik:RadNumericTextBox>
                                                                                <asp:RequiredFieldValidator ID="rfvQty"
                                                                                    ControlToValidate="tbQty" ErrorMessage="*Required" runat="server" Display="Dynamic">
                                                                                </asp:RequiredFieldValidator>
                                                                            </EditItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                                                                            ConfirmTitle="Delete" HeaderText="Delete" HeaderStyle-Width="50px"
                                                                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                                                        </telerik:GridButtonColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                                <ClientSettings>
                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                </ClientSettings>
                                                            </telerik:RadGrid>
                                                            <asp:SqlDataSource ID="dsItem"
                                                            ProviderName="System.Data.SqlClient" runat="server"
                                                                deletecommand="delete shiporderitem where itemid = @ItemID"
                                                                updatecommand="update shiporderitem set qty = @qty where itemid = @itemid"
                                                                selectcommand="select soi.itemid, spd.productno, spd.productname, soi.qty from shiporderitem soi
                                                                    inner join shipproduct sp on sp.productid = soi.productid
                                                                    inner join shipproductdetail spd on spd.productdetailid = sp.productdetailid
                                                                    where orderid = @orderid">
                                                                <DeleteParameters>
                                                                    <asp:Parameter Name="ItemID" Type="Int32" />
                                                                </DeleteParameters>
                                                                <UpdateParameters>
                                                                    <asp:Parameter Name="ItemID" Type="Int32" />
                                                                    <asp:Parameter Name="Qty" Type="Int32" />
                                                                </UpdateParameters>
                                                                <SelectParameters>
                                                                    <asp:Parameter Name="OrderID" Type="Int32" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                                <asp:Table runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell Font-Bold="true">
                                                            Comments:
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtComments" TextMode="MultiLine" Width="1200" Height="100" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell HorizontalAlign="Right">
                                                            <asp:Button ID="btnSaveComment" OnClick="btnSaveComment_Click" BorderColor="Red" BackColor="Red" ForeColor="White" runat="server" Text="Save Comments" />
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </asp:Panel>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfError" runat="server" />
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
        <asp:HiddenField ID="hfUserEMail" runat="server" />
    </form>
</body>
</html>
