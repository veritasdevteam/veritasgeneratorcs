﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace VeritasGeneratorCS
{
    static public class Functions
    {
        static public string GetUserInfo(long xUserID)
        {
            if (xUserID == 0)
            {
                return "";
            }
            string SQL;
            clsDBO.clsDBO clUI = new clsDBO.clsDBO();
            SQL = "select * from userinfo where userid = " + xUserID;
            clUI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clUI.RowCount() > 0)
            {
                clUI.GetRow();
                return clUI.GetFields("fname") + " " + clUI.GetFields("lname");
            }
            return "";
        }
        static public string GetUserEmail(long xUserID)
        {
            if (xUserID == 0)
            {
                return "";
            }
            string SQL;
            clsDBO.clsDBO clUI = new clsDBO.clsDBO();
            SQL = "select * from userinfo where userid = " + xUserID;
            clUI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clUI.RowCount() > 0)
            {
                clUI.GetRow();
                return clUI.GetFields("email");
            }
            return "";
        }
        static public string GetSubAgentInfo(long xID)
        {
            if (xID == 0)
            {
                return "";
            }
            string SQL;
            clsDBO.clsDBO clSA = new clsDBO.clsDBO();
            SQL = "select * from subagents where subagentid = " + xID;
            clSA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSA.RowCount() > 0)
            {
                clSA.GetRow();
                return clSA.GetFields("subagentno") + " / " + clSA.GetFields("subagentname");
            }
            return "";
        }
        static public string GetAgentInfo(long xID)
        {
            if (xID == 0)
            {
                return "";
            }
            string SQL;
            clsDBO.clsDBO clA = new clsDBO.clsDBO();
            SQL = "select * from agents where agentid = " + xID;
            clA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                return clA.GetFields("agentno") + " / " + clA.GetFields("agentname");
            }
            return "";
        }
        static public string GetRateType(long xID)
        {
            if (xID == 0)
            {
                return "";
            }
            string SQL;
            clsDBO.clsDBO clRT = new clsDBO.clsDBO();
            SQL = "select * from ratetype " + "where ratetypeid = " + xID;
            clRT.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clRT.RowCount() > 0)
            {
                clRT.GetRow();
                return clRT.GetFields("ratetypename");
            }
            return "";
        }
        static public bool GetAllow45(long xUserID)
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string SQL;
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + xUserID + " " +
                  "and allow45days <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                return true;
            }
            return false;
        }
        static public string GetServiceCenterState(long xClaimID)
        {
            //GetServiceCenterState = ""
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select sc.State from claim c " +
                  "inner join ServiceCenter sc on c.ServiceCenterID = sc.ServiceCenterID " +
                  "where claimid = " + xClaimID;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("state");
            }
            return "";
        }
        static public long GetDaysSale(long xClaimID)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claim cl " +
                  "left join contract c on cl.contractid = c.contractid " +
                  "where claimid = " + xClaimID;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return (DateTime.Parse(clR.GetFields("lossdate")) - DateTime.Parse(clR.GetFields("saledate"))).Days;
            }
            return 0;
        }
    }
}