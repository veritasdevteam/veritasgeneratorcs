﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace VeritasGeneratorCS.Contract
{
    public partial class ContractCustomerModification : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsStates.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            if (!IsPostBack)
            {
                hfContractID.Value = Request.QueryString["contractid"];
                GetServerInfo();
                FillCustomer();
                ReadOnlyButtons();
            }
        }
        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly").ToLower() == "true")
                {
                    btnSave.Visible = false;
                }
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private void FillCustomer()
        {
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            string SQL;
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                txtAddr1.Text = clC.GetFields("addr1");
                txtAddr2.Text = clC.GetFields("addr2");
                txtCity.Text = clC.GetFields("city");
                txtFName.Text = clC.GetFields("fname");
                txtLName.Text = clC.GetFields("lname");
                txtPhone.Text = clC.GetFields("phone");
                txtZip.Text = clC.GetFields("zip");
                cboState.SelectedValue = clC.GetFields("state");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            FillCustomer();
            Response.Redirect("contract.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            string SQL;
            VeritasGlobalToolsV2.clsMoxyAddressChange clMAC = new VeritasGlobalToolsV2.clsMoxyAddressChange();


            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                clC.SetFields("addr1", txtAddr1.Text);
                clC.SetFields("addr2", txtAddr2.Text);
                clC.SetFields("city", txtCity.Text);
                clC.SetFields("fname", txtFName.Text);
                clC.SetFields("lname", txtLName.Text);
                clC.SetFields("phone", txtPhone.Text);
                clC.SetFields("zip", txtZip.Text);
                clC.SetFields("state", cboState.SelectedValue);
                clC.SaveDB();
                clMAC.UpdateMoxy(long.Parse(hfContractID.Value));
            }
            Response.Redirect("contract.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);
        }

    }
}