﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Contract
{
    public partial class ContractCommission : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsAgent.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsRateType.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            if (!IsPostBack)
            {
                hfContractID.Value = Request.QueryString["contractid"];
                GetServerInfo();
                FillTable();
                ReadOnlyButtons();
                pnlSearchRateType.Visible = false;
                pnlChange.Visible = false;
                pnlSearchAgent.Visible = false;
            }
        }
        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly").ToLower() == "true")
                {
                    btnAdd.Enabled = false;
                    btnSave.Enabled = false;
                    btnRateTypeSearch.Enabled = false;
                }
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private void FillTable()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            SQL = "select contractcommissionid, rt.ratetypename, AgentName, amt, finaldate, cancelamt, cancelfinaldate from contractcommissions cc " +
                  "inner join ratetype rt on rt.ratetypeid = cc.ratetypeid " +
                  "inner join agents a on a.agentid = cc.agentid " +
                  "where contractid = " + hfContractID.Value;
            rgCommission.DataSource = clC.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            rgCommission.DataBind();
        }

        protected void rgCommission_SelectedIndexChanged(object sender, EventArgs e)
        {
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            string SQL;
            pnlCommission.Visible = false;
            pnlChange.Visible = true;
            SQL = "select * from contractcommissions where contractcommissionid = " + rgCommission.SelectedValue;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfContractCommissionID.Value = rgCommission.SelectedValue.ToString();
                hfRateTypeID.Value = clC.GetFields("ratetypeid");
                if (clC.GetFields("finaldate").Length > 0)
                {
                    rdpCycleDate.SelectedDate = DateTime.Parse(clC.GetFields("finaldate"));
                }
                else
                {
                    rdpCycleDate.Clear();
                }
                txtAmt.Text = Convert.ToDouble(clC.GetFields("amt")).ToString("#,##0.00");
                if (clC.GetFields("cancelfinaldate").Length > 0)
                {
                    rdpCancelCycleDate.SelectedDate = DateTime.Parse(clC.GetFields("cancelfinaldate"));
                }
                else
                {
                    rdpCancelCycleDate.Clear();
                }
                txtCancelAmt.Text = clC.GetFields("cancelamt");
                GetRateType();
            }
        }
        private void GetRateType()
        {
            string SQL;
            clsDBO.clsDBO clRT = new clsDBO.clsDBO();
            SQL = "select * from ratetype where ratetypeid = " + hfRateTypeID.Value;
            clRT.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clRT.RowCount() > 0)
            {
                clRT.GetRow();
                txtRateType.Text = clRT.GetFields("ratetypename");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlChange.Visible = false;
            pnlCommission.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clCC = new clsDBO.clsDBO();
            SQL = "select * from contractcommissions where contractcommissionid = " + hfContractCommissionID.Value;
            clCC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCC.RowCount() == 0)
            {
                clCC.NewRow();
            }
            else
            {
                clCC.GetRow();
            }
            clCC.SetFields("contractid", hfContractID.Value);
            clCC.SetFields("ratetypeid", hfRateTypeID.Value);
            GetAgentID();
            clCC.SetFields("agentid", hfAgentID.Value);
            if (txtAmt.Text.Length > 0)
            {
                clCC.SetFields("amt", txtAmt.Text);
            }
            else
            {
                clCC.SetFields("amt", 0.ToString());
            }
            if (rdpCycleDate.SelectedDate != null) 
            {
                clCC.SetFields("finaldate", rdpCycleDate.SelectedDate.ToString());
            }
            if (txtCancelAmt.Text.Length > 0)
            {
                clCC.SetFields("cancelamt", txtCancelAmt.Text);
            }
            else
            {
                clCC.SetFields("cancelamt", 0.ToString());
            }
            if (rdpCancelCycleDate.SelectedDate != null)
            {
                clCC.SetFields("cancelfinaldate", txtCancelAmt.Text);
            }
            if (clCC.RowCount() == 0)
            {
                clCC.AddRow();
            }
            clCC.SaveDB();
            pnlChange.Visible = false;
            pnlCommission.Visible = true;
            FillTable();
        }
        public void GetAgentID()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfAgentID.Value = clC.GetFields("agentsid");
            }
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            pnlCommission.Visible = false;
            pnlChange.Visible = true;
            hfContractCommissionID.Value = 0.ToString();
            hfAgentID.Value = 0.ToString();
            hfRateTypeID.Value = 0.ToString();
            txtAgentID.Text = "";
            txtRateType.Text = "";
            txtAmt.Text = "";
            rdpCycleDate.Clear();
            txtCancelAmt.Text = "";
            rdpCancelCycleDate.Clear();
        }

        protected void btnRateTypeSearch_Click(object sender, EventArgs e)
        {
            rgRateType.Rebind();
            pnlChange.Visible = false;
            pnlSearchRateType.Visible = true;
        }

        protected void btnCancelRateType_Click(object sender, EventArgs e)
        {
            pnlChange.Visible = true;
            pnlSearchRateType.Visible = false;
        }

        protected void rgRateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfRateTypeID.Value = rgRateType.SelectedValue.ToString();
            pnlChange.Visible = true;
            pnlSearchRateType.Visible = false;
            GetRateType();
        }

        protected void btnAgentSearch_Click(object sender, EventArgs e)
        {
            pnlChange.Visible = false;
            pnlSearchAgent.Visible = true;
        }

        protected void btnClearAgentInfo_Click(object sender, EventArgs e)
        {
            hfAgentID.Value = 0.ToString();
            txtAgentID.Text = "";
            pnlChange.Visible = true;
            pnlSearchAgent.Visible = false;
        }

        protected void rgAgent_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfAgentID.Value = rgAgent.SelectedValue.ToString();
            txtAgentID.Text = Functions.GetAgentInfo(long.Parse(hfAgentID.Value));
            pnlChange.Visible = true;
            pnlSearchAgent.Visible = false;
        }
    }
}