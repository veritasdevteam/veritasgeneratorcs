﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Contract
{
    public partial class Contract : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            GetServerInfo();
            CheckToDo();
            if (!IsPostBack)
            {
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                hfContractID.Value = Request.QueryString["contractid"];
                FillContract();
                pvVehicleInfo.Selected = true;
                tsContract.Tabs[0].Selected = true;
                pnlContractConfirm.Visible = false;
            }
            if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
            {
                pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
            }
            else
            {
                pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
            }
        }

        private void FillContract()
        {
            string SQL;
            string sTemp;
            SQL = "select p.programname as PName, * from contract c " +
                  "left join program p on c.programid = p.programid " +
                  "left join plantype pt on pt.plantypeid = c.plantypeid " +
                  "where contractid = " + hfContractID.Value;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                lblContractNo.Text = clC.GetFields("contractno");
                lblStatus.Text = clC.GetFields("status");
                if (clC.GetFields("saledate").Length > 0)
                {
                    lblSaleDate.Text = DateTime.Parse(clC.GetFields("saledate")).ToString("M/d/yyyy");
                }
                if (clC.GetFields("effdate").Length > 0)
                {
                    lblEffDate.Text = DateTime.Parse(clC.GetFields("effdate")).ToString("M/d/yyyy");
                }
                if (clC.GetFields("expdate").Length > 0)
                {
                    lblExpDate.Text = DateTime.Parse(clC.GetFields("expdate")).ToString("M/d/yyyy");
                }
                if (clC.GetFields("salemile").Length > 0)
                {
                    //lblSaleMiles.Text = Format(CLng(clC.Fields("salemile")), "#,##0");
                    lblSaleMiles.Text = long.Parse(clC.GetFields("salemile")).ToString("#,##0");
                }
                if (clC.GetFields("effmile").Length > 0)
                {
                    lblEffMile.Text = long.Parse(clC.GetFields("effmile")).ToString("#,##0");
                }
                if (clC.GetFields("expmile").Length > 0)
                {
                    lblExpMile.Text = long.Parse(clC.GetFields("expmile")).ToString("#,##0");
                }
                if (Convert.ToInt32(clC.GetFields("programid")) == 93 || Convert.ToInt32(clC.GetFields("programid")) == 94 || Convert.ToInt32(clC.GetFields("programid")) == 95 || Convert.ToInt32(clC.GetFields("programid")) == 96 || Convert.ToInt32(clC.GetFields("programid")) == 97)
                {
                    lblSaleMilesLabel.Text = "Sale KM:";
                    lblEffMileLabel.Text = "Effective KM:";
                    lblExpireMileLabel.Text = "Expire KM:";
                }
                lblLienholder.Text = clC.GetFields("lienholder");
                hfProgramID.Value = clC.GetFields("programid");
                hfPlanTypeID.Value = clC.GetFields("plantypeid");
                lblProgram.Text = clC.GetFields("pname");
                lblPlanType.Text = clC.GetFields("plantype");
                lblTerm.Text = clC.GetFields("termmonth") + "/" + long.Parse(clC.GetFields("termmile")).ToString("#,##0");
                sTemp = clC.GetFields("fname") + " " +
                        clC.GetFields("lname") + "\r\n" +
                        clC.GetFields("addr1") + "\r\n";
                if (clC.GetFields("addr2").Length > 0)
                {
                    sTemp = sTemp + clC.GetFields("addr2") + "\r\n";
                }
                sTemp = sTemp + clC.GetFields("city") + " ";
                sTemp = sTemp + clC.GetFields("state") + " ";
                sTemp = sTemp + clC.GetFields("zip") + "\r\n";
                sTemp = sTemp + clC.GetFields("phone");
                txtCustomerInfo.Text = sTemp;
                if (clC.GetFields("datepaid").Length > 0)
                {
                    lblPaidDate.Text = DateTime.Parse(clC.GetFields("datepaid")).ToString("M/d/yyyy");
                }
                if (clC.GetFields("decpage").Length == 0)
                {
                    tcDec.Visible = false;
                    tcDesc2.Visible = false;
                }
                else
                {
                    tcDec.Visible = true;
                    tcDesc2.Visible = true;
                    hlDec.NavigateUrl = clC.GetFields("decpage");
                    hlDec.Text = clC.GetFields("decpage");
                }
                if (clC.GetFields("tcpage").Length == 0)
                {
                    tcTC.Visible = false;
                    tcTC2.Visible = false;
                }
                else
                {
                    tcTC.Visible = true;
                    tcTC2.Visible = true;
                    hlTC.NavigateUrl = clC.GetFields("tcpage");
                    hlTC.Text = clC.GetFields("tcpage");
                }
                GetDealerInfo(long.Parse(clC.GetFields("dealerid")));
                GetAgentInfo(long.Parse(clC.GetFields("agentsid")));
                FillReinstatement();
            }
        }
        private void GetDealerInfo(long xDealerID)
        {
            clsDBO.clsDBO clD = new clsDBO.clsDBO();
            string SQL;
            string sTemp;
            SQL = "select * from dealer where dealerid = " + xDealerID;
            clD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
                sTemp = clD.GetFields("dealerno") + "\r\n" +
                        clD.GetFields("dealername") + "\r\n" +
                        clD.GetFields("Addr1") + "\r\n";
                if (clD.GetFields("Addr2").Length > 0)
                {
                    sTemp = sTemp + clD.GetFields("Addr2") + "\r\n";
                }
                sTemp = sTemp + clD.GetFields("City") + " " + 
                        clD.GetFields("State") + " " + 
                        clD.GetFields("Zip") + "\r\n" + 
                        clD.GetFields("Phone");
                txtDealer.Text = sTemp;
                GetANCB(long.Parse(clD.GetFields("dealerid")));
            }
        }
        private void GetANCB(long xDealerID)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                SQL = "select * from CommissionHeader " +
                      "where (ratetypeid = 29 or ratetypeid = 26) " +
                      "and dealerid = " + xDealerID + " " +
                      "and programid = " + clC.GetFields("programid") + " ";
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() > 0)
                {
                    chkANCB.Checked = true;
                }
                else
                {
                    chkANCB.Checked = false;
                }
            }
        }
        private void GetAgentInfo(long xAgentID)
        {
            clsDBO.clsDBO clA = new clsDBO.clsDBO();
            string SQL;
            string sTemp;
            SQL = "select * from agents where agentid = " + xAgentID;
            clA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                sTemp = clA.GetFields("agentno") + "\r\n" +
                        clA.GetFields("agentname") + "\r\n" +
                        clA.GetFields("addr1") + "\r\n";
                if (clA.GetFields("addr2").Length > 0)
                {
                    sTemp = sTemp + clA.GetFields("addr2") + "\r\n";
                }
                sTemp = sTemp + clA.GetFields("city") + " " + clA.GetFields("state") + " " + clA.GetFields("zip") + "\r\n";
                sTemp = sTemp + clA.GetFields("phone");
                txtAgent.Text = sTemp;
            }
        }
        private void FillReinstatement()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from contractcancel " +
                  "where contractid = " + hfContractID.Value +
                  "and not reinstatedate is null ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                lblReinstatementDate.Text = DateTime.Parse(clR.GetFields("reinstatedate")).ToString("M/d/yyyy");
            }
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO.clsDBO clTD = new clsDBO.clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
                ReadOnlyButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfReadOnly.Value = clSI.GetFields("readonly");
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("viewbreakdown")))
                {
                    tsContract.Tabs[1].Visible = true;
                }
                else
                {
                    tsContract.Tabs[1].Visible = false;
                }
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true) {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true) {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true) {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true) {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true) {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true) {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true) {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true) {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true) {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true) {
                    btnReports.Enabled = true;
                }
                if (clSI.GetFields("agentid").Length > 0)
                {
                    if (clSI.GetFields("agentid") != "0")
                    {
                        tsContract.Tabs[6].Visible = false;
                        tsContract.Tabs[8].Visible = false;
                    }
                }
            }
        }
        private void ReadOnlyButtons()
        {
            if (hfReadOnly.Value.ToLower() == "true")
            {
                btnAddClaim.Enabled = false;
                tsContract.Tabs[8].Visible = false;
            }
        }
        private void ShowError(string xMessage)
        {
            hfError.Value = "Visible";
            lblError.Text = xMessage;
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }
        private void ShowDupeClaim()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwDupeClaim.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void tsContract_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            if (tsContract.SelectedTab.Value == "Vehicle") {
                pvVehicleInfo.Selected = true;
            }
            if (tsContract.SelectedTab.Value == "Breakdown") {
                pvBreakdown.Selected = true;
            }
            if (tsContract.SelectedTab.Value == "Costs") {
                pvCost.Selected = true;
            }
            if (tsContract.SelectedTab.Value == "Commissions") {
                pvCommissions.Selected = true;
            }
            if (tsContract.SelectedTab.Value == "Payment") {
                pvPayment.Selected = true;
            }
            if (tsContract.SelectedTab.Value == "Document") {
                pvContractDocument.Selected = true;
            }
            if (tsContract.SelectedTab.Value == "Cancellation") {
                pvCancel.Selected = true;
                //ucCancel.FillCancellation();
            }
            if (tsContract.SelectedTab.Value == "Transfer") {
                pvTransfer.Selected = true;
            }
            if (tsContract.SelectedTab.Value == "Notes") {
                pvNotes.Selected = true;
            }
            if (tsContract.SelectedTab.Value == "CustModify") {
                pvCustomerMod.Selected = true;
            }
            if (tsContract.SelectedTab.Value == "ContractModify") {
                pvContractMod.Selected = true;
            }
        }

        protected void btnAddClaim_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            if (!CheckClaimDuplicate()) 
            {
                string SQL;
                clsDBO.clsDBO clC = new clsDBO.clsDBO();
                if (hfReadOnly.Value.ToLower() == "true") 
                {
                    return;
                }
                pnlContractConfirm.Visible = false;
                tsContract.Visible = true;
                SQL = "select * from contract where contractid = " + hfContractID.Value;
                clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clC.RowCount() > 0) {
                    clC.GetRow();
                    if (clC.GetFields("status") == "Paid") 
                    {
                        if (Convert.ToInt32(clC.GetFields("plantypeid")) != 118 && Convert.ToInt32(clC.GetFields("programid")) != 103)
                        {
                            GetClaimNo();
                            AddClaimDetail();
                            Response.Redirect("~\\claim\\claim.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value);
                        }
                        else
                        {
                            GetClaimNoGAP();
                            Response.Redirect("~\\claim\\claimgap.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value);
                        }
                        return;
                    }
                    else
                    {
                        if (!CheckCancelExpire()) 
                        {
                            ShowError("Contract is Expired or Cancelled. Can not start claim.");
                            return;
                        }
                        tsContract.Visible = false;
                        txtContractStatus2.Text = clC.GetFields("status");
                        if (Convert.ToInt32(clC.GetFields("plantypeid")) != 118 && Convert.ToInt32(clC.GetFields("programid")) != 103) 
                        {
                            GetClaimNo();
                            AddClaimDetail();
                        }
                        else 
                        {
                            GetClaimNoGAP();
                            hfPlanTypeID.Value = clC.GetFields("plantypeid");
                            hfProgramID.Value = clC.GetFields("programid");
                        }
                        pnlContractConfirm.Visible = true;
                        return;
                    }
                }
            }
        }
        private bool CheckClaimDuplicate()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (!CheckWithInDays())
            {
                if (hfContractID.Value.Length > 0)
                {
                    SQL = "select * from claim " + 
                          "where contractid = " + hfContractID.Value + " " + 
                          "and credate > '" + DateTime.Today.AddDays(-3) + "' ";
                    clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clR.RowCount() > 0)
                    {
                        ShowDupeClaim();
                        return true;
                    }
                }
            }
            return false;
        }
        private bool CheckWithInDays()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("within3days")))
                {
                    return true;
                }
            }
            return false;
        }
        private void GetClaimNo()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            string sClaimNo;
            long lClaimNo;
        MoveHere:
            sClaimNo = "";
            SQL = "select max(claimno) as claimno from claim where claimno like 'C1%' ";
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                sClaimNo = clC.GetFields("claimno");
                sClaimNo = sClaimNo.Replace("C1", "");
                lClaimNo = long.Parse(sClaimNo);
                lClaimNo = lClaimNo + 1;
                sClaimNo = lClaimNo.ToString("100000000");
                sClaimNo = "C" + sClaimNo;
            }
            if (sClaimNo.Length > 0)
            {
                SQL = "select * from claim where claimno = '" + sClaimNo + "' ";
                clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clC.RowCount() > 0)
                {
                    goto MoveHere;
                }
                else
                {
                    clC.NewRow();
                    clC.SetFields("claimno", sClaimNo);
                    clC.SetFields("status", "Open");
                    if (hfContractID.Value != "0")
                    {
                        clC.SetFields("contractid", hfContractID.Value);
                        clC.SetFields("ClaimCntNo", (GetClaimCnt() + 1).ToString());
                    }
                    clC.SetFields("lossdate", DateTime.Today.ToString());
                    clC.SetFields("claimactivityid", 63.ToString());
                    clC.SetFields("assignedto", hfUserID.Value);
                    clC.SetFields("creby", hfUserID.Value);
                    clC.SetFields("credate", DateTime.Today.ToString());
                    clC.SetFields("moddate", DateTime.Today.ToString());
                    clC.SetFields("modby", hfUserID.Value);
                    clC.AddRow();
                    clC.SaveDB();
                }
            }
            SQL = "select * from claim where claimno = '" + sClaimNo + "' ";
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfClaimID.Value = clC.GetFields("claimid");
                SQL = "insert into claimopenhistory " +
                      "(claimid, opendate, openby) " +
                      "values (" + hfClaimID.Value + ",'" +
                      DateTime.Today + "'," +
                      hfUserID.Value + ")";
                clC.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            }
        }
        private long GetClaimCnt()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string SQL;
            SQL = "select * from claim where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            return clR.RowCount();
        }
        private void AddClaimDetail()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            long lDeductID = 6;
            long dDeduct = 0;
            SQL = "select deductid from contract where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                lDeductID = long.Parse(clR.GetFields("deductid"));
            }
            SQL = "select * from deductible where deductid = " + lDeductID.ToString();
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                dDeduct = long.Parse(clR.GetFields("deductamt"));
            }
            SQL = "select * from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and jobno = 'A01' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
            }
            else
            {
                clR.GetRow();
            }
            clR.SetFields("ratetypeid", 1.ToString());
            clR.SetFields("claimid", hfClaimID.Value);
            clR.SetFields("claimdetailtype", "Deductible");
            clR.SetFields("jobno", "A01");
            clR.SetFields("reqqty", "1");
            clR.SetFields("reqcost", (dDeduct * -1).ToString());
            clR.SetFields("claimdetailstatus", "Requested");
            clR.SetFields("reqamt", (dDeduct * -1).ToString());
            clR.SetFields("creby", hfUserID.Value);
            clR.SetFields("credate", DateTime.Today.ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.SetFields("moddate", DateTime.Today.ToString());
            if (clR.RowCount() == 0)
            {
                clR.AddRow();
            }
            clR.SaveDB();
        }
        private void GetClaimNoGAP()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            string sClaimNo;
            long lClaimNo = 0;
        MoveHere:
            sClaimNo = "";
            SQL = "select max(claimno) as claimno from claimgap where claimno like 'G0%' ";
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                if (clC.GetFields("claimno").Length > 0)
                {
                    sClaimNo = clC.GetFields("claimno");
                    sClaimNo = sClaimNo.Replace("G0", "");
                    lClaimNo = long.Parse(sClaimNo);
                    lClaimNo = lClaimNo + 1;
                    sClaimNo = lClaimNo.ToString("000000000");
                    sClaimNo = "G" + sClaimNo;
                }
                else
                {
                    sClaimNo = "G000000000";
                }
            }
            if (sClaimNo.Length > 0)
            {
                SQL = "select * from claimgap where claimno = '" + sClaimNo + "' ";
                clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clC.RowCount() > 0)
                {
                    goto MoveHere;
                }
                else
                {
                    clC.NewRow();
                    clC.SetFields("claimno", sClaimNo);
                    clC.SetFields("status", 1.ToString());
                    if (hfContractID.Value != "0")
                    {
                        clC.SetFields("contractid", hfContractID.Value);
                    }
                    clC.SetFields("claimdate", DateTime.Today.ToString());
                    clC.SetFields("creby", hfUserID.Value);
                    clC.SetFields("credate", DateTime.Today.ToString());
                    clC.SetFields("moddate", DateTime.Today.ToString());
                    clC.SetFields("modby", hfUserID.Value);
                    clC.AddRow();
                    clC.SaveDB();
                }
            }
            SQL = "select * from claimgap where claimno = '" + sClaimNo + "' ";
            clC.OpenDB(SQL, System.Configuration.ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfClaimID.Value = clC.GetFields("claimgapid");
            }
        }
        private bool CheckCancelExpire()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clU = new clsDBO.clsDBO();
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("status").ToLower() == "expired" || clR.GetFields("status").ToLower() == "pending expired" || clR.GetFields("status").ToLower() == "cancelled" || clR.GetFields("status").ToLower() == "cancelled before paid")
                {
                    SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
                    clU.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clU.RowCount() > 0)
                    {
                        clU.GetRow();
                        if (Convert.ToBoolean(clU.GetFields("allowcancelexpire")))
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        protected void btnCancelClaim_Click(object sender, EventArgs e)
        {
            UpdateContractClaimCancel();
            UpdateClaimNote();
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }
        private void UpdateContractClaimCancel()
        {
            clsDBO.clsDBO clCL = new clsDBO.clsDBO();
            string SQL;
            if (hfReadOnly.Value.ToLower() == "true") {
                return;
            }
            SQL = "select * from claim where claimid = " + hfClaimID.Value;
            clCL.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCL.RowCount() > 0)
            {
                clCL.GetRow();
                clCL.SetFields("status", "Void");
                clCL.SetFields("contractid", hfContractID.Value);
                clCL.SaveDB();
            }
        }
        private void UpdateClaimNote()
        {
            if (txtNote.Text.Length == 0)
            {
                return;
            }
            clsDBO.clsDBO clCN = new clsDBO.clsDBO();
            string SQL;
            SQL = "select * from claimnote " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimnoteid = 0 ";
            clCN.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            clCN.NewRow();
            clCN.SetFields("claimid", hfClaimID.Value);
            clCN.SetFields("claimnotetypeid", 5.ToString());
            clCN.SetFields("note", txtNote.Text);
            clCN.SetFields("credate", DateTime.Today.ToString());
            clCN.SetFields("creby", hfUserID.Value);
            clCN.AddRow();
            clCN.SaveDB();
        }
        protected void btnProceedClaim_Click(object sender, EventArgs e)
        {
            if (hfPlanTypeID.Value != "118" && hfProgramID.Value != "103") 
            {
                UpdateClaimNote();
                pnlContractConfirm.Visible = false;
                tsContract.Visible = true;
                Response.Redirect("~/claim/claim.aspx?sid=" + hfID.Value + "&ClaimID=" + hfClaimID.Value);
            }
            else 
            {
                UpdateClaimNote2();
                pnlContractConfirm.Visible = false;
                tsContract.Visible = true;
                Response.Redirect("~/claim/claimgap.aspx?sid=" + hfID.Value + "&ClaimID=" + hfClaimID.Value);
            }
        }
        private void UpdateClaimNote2()
        {
            if (txtNote.Text.Length == 0)
            {
                return;
            }
            clsDBO.clsDBO clCN = new clsDBO.clsDBO();
            string SQL;
            SQL = "select * from claimgapnote " +
                  "where claimgapid = " + hfClaimID.Value + " " +
                  "and claimnoteid = 0 ";
            clCN.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            clCN.NewRow();
            clCN.SetFields("claimgapid", hfClaimID.Value);
            clCN.SetFields("claimgapnotetypeid", 3.ToString());
            clCN.SetFields("note", txtNote.Text);
            clCN.SetFields("credate", DateTime.Today.ToString());
            clCN.SetFields("creby", hfUserID.Value);
            clCN.AddRow();
            clCN.SaveDB();
        }
    }
}