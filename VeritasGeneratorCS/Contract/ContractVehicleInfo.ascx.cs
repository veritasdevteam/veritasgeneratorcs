﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasGeneratorCS.Contract
{
    public partial class ContractVehicleInfo : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pnlModify.Visible = false;
                pnlViewer.Visible = true;
                hfContractID.Value = Request.QueryString["contractid"];
                GetServerInfo();
                FillVehicleInfo();
                ReadOnlyButtons();
            }
        }
        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (!Convert.ToBoolean(clR.GetFields("contractmodification")))
                {
                    btnEdit.Enabled = false;
                }
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private void FillVehicleInfo()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                lblVIN.Text = clC.GetFields("vin");
                lblClass.Text = clC.GetFields("class");
                lblYear.Text = clC.GetFields("year");
                lblMake.Text = clC.GetFields("make");
                lblModel.Text = clC.GetFields("model");
                lblTrim.Text = clC.GetFields("Trim");
                chkAWD.Checked = Convert.ToBoolean(clC.GetFields("AWD"));
                chkTurbo.Checked = Convert.ToBoolean(clC.GetFields("Turbo"));
                chkDiesel.Checked = Convert.ToBoolean(clC.GetFields("Diesel"));
                chkHybrid.Checked = Convert.ToBoolean(clC.GetFields("Hybrid"));
                chkCommercial.Checked = Convert.ToBoolean(clC.GetFields("Commercial"));
                chkHydraulic.Checked = Convert.ToBoolean(clC.GetFields("Hydraulic"));
                chkAirBladder.Checked = Convert.ToBoolean(clC.GetFields("AirBladder"));
                chkLiftKit.Checked = Convert.ToBoolean(clC.GetFields("LiftKit"));
                chkLuxury.Checked = Convert.ToBoolean(clC.GetFields("Luxury"));
                chkSeals.Checked = Convert.ToBoolean(clC.GetFields("Seals"));
                chkSnowPlow.Checked = Convert.ToBoolean(clC.GetFields("SnowPlow"));
                chkLargerLiftKit.Checked = Convert.ToBoolean(clC.GetFields("LargerLiftKit"));
                chkRideShare.Checked = Convert.ToBoolean(clC.GetFields("rideshare"));

                txtVINMod.Text = clC.GetFields("vin");
                txtClassMod.Text = clC.GetFields("class");
                txtYearMod.Text = clC.GetFields("year");
                txtMakeMod.Text = clC.GetFields("make");
                txtModelMod.Text = clC.GetFields("model");
                txtTrimMod.Text = clC.GetFields("Trim");
                chkAwdMod.Checked = Convert.ToBoolean(clC.GetFields("AWD"));
                chkTurboMod.Checked = Convert.ToBoolean(clC.GetFields("Turbo"));
                chkDieselMod.Checked = Convert.ToBoolean(clC.GetFields("Diesel"));
                chkHybridMod.Checked = Convert.ToBoolean(clC.GetFields("Hybrid"));
                chkCommercialMod.Checked = Convert.ToBoolean(clC.GetFields("Commercial"));
                chkHydraulicMod.Checked = Convert.ToBoolean(clC.GetFields("Hydraulic"));
                chkAirBladderMod.Checked = Convert.ToBoolean(clC.GetFields("AirBladder"));
                chkLiftKitMod.Checked = Convert.ToBoolean(clC.GetFields("LiftKit"));
                chkLuxuryMod.Checked = Convert.ToBoolean(clC.GetFields("Luxury"));
                chkSealsMod.Checked = Convert.ToBoolean(clC.GetFields("Seals"));
                chkSnowPlowMod.Checked = Convert.ToBoolean(clC.GetFields("SnowPlow"));
                chkLargerLiftKitMod.Checked = Convert.ToBoolean(clC.GetFields("LargerLiftKit"));
                chkRideShareMod.Checked = Convert.ToBoolean(clC.GetFields("rideshare"));
                chkSalvageTitle.Checked = Convert.ToBoolean(clC.GetFields("SalvageTitle"));
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            pnlModify.Visible = true;
            pnlViewer.Visible = false;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlModify.Visible = false;
            pnlViewer.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            CheckChange();
            SaveContract();
            FillVehicleInfo();
            pnlModify.Visible = false;
            pnlViewer.Visible = true;
        }
        private void SaveContract()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                clC.SetFields("vin", txtVINMod.Text);
                clC.SetFields("class", txtClassMod.Text);
                clC.SetFields("year", txtYearMod.Text);
                clC.SetFields("make", txtMakeMod.Text);
                clC.SetFields("model", txtModelMod.Text);
                clC.SetFields("Trim", txtTrimMod.Text);
                clC.SetFields("AWD", chkAwdMod.Checked.ToString());
                clC.SetFields("Turbo", chkTurboMod.Checked.ToString());
                clC.SetFields("Diesel", chkDieselMod.Checked.ToString());
                clC.SetFields("Hybrid", chkHybridMod.Checked.ToString());
                clC.SetFields("Commercial", chkCommercialMod.Checked.ToString());
                clC.SetFields("Hydraulic", chkHydraulicMod.Checked.ToString());
                clC.SetFields("AirBladder", chkAirBladderMod.Checked.ToString());
                clC.SetFields("LiftKit", chkLiftKitMod.Checked.ToString());
                clC.SetFields("Luxury", chkLuxuryMod.Checked.ToString());
                clC.SetFields("Seals", chkSealsMod.Checked.ToString());
                clC.SetFields("SnowPlow", chkSnowPlowMod.Checked.ToString());
                clC.SetFields("LargerLiftKit", chkLargerLiftKitMod.Checked.ToString());
                clC.SetFields("modby", hfUserID.Value);
                clC.SetFields("moddate", DateTime.Today.ToString());
                clC.SetFields("rideshare", chkRideShareMod.Checked.ToString());
                clC.SetFields("SalvageTitle", chkSalvageTitle.Checked.ToString());
                clC.SaveDB();
            }
        }
        private void CheckChange()
        {
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            string SQL;
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                if (txtVINMod.Text != clC.GetFields("vin")) {
                    UpdateHistory("VIN", clC.GetFields("VIN"), txtVINMod.Text);
                }
                if (txtClassMod.Text != clC.GetFields("class")) {
                    UpdateHistory("Class", clC.GetFields("class"), txtClassMod.Text);
                }
                if (txtYearMod.Text != clC.GetFields("year")) {
                    UpdateHistory("Year", clC.GetFields("year"), txtYearMod.Text);
                }
                if (txtMakeMod.Text != clC.GetFields("Make")) {
                    UpdateHistory("Make", clC.GetFields("make"), txtMakeMod.Text);
                }
                if (txtModelMod.Text != clC.GetFields("model")) {
                    UpdateHistory("Model", clC.GetFields("model"), txtModelMod.Text);
                }
                if (txtTrimMod.Text != clC.GetFields("trim")) {
                    UpdateHistory("Trim", clC.GetFields("trim"), txtTrimMod.Text);
                }
                if (chkAwdMod.Checked != Convert.ToBoolean(clC.GetFields("awd"))) {
                    UpdateHistory("AWD", clC.GetFields("awd"), chkAwdMod.Checked.ToString());
                }
                if (chkTurboMod.Checked != Convert.ToBoolean(clC.GetFields("turbo"))) {
                    UpdateHistory("Turbo", clC.GetFields("turbo"), chkTurbo.Checked.ToString());
                }
                if (chkDieselMod.Checked != Convert.ToBoolean(clC.GetFields("diesel"))) {
                    UpdateHistory("Diesel", clC.GetFields("diesel"), chkDieselMod.Checked.ToString());
                }
                if (chkHybridMod.Checked != Convert.ToBoolean(clC.GetFields("hybrid"))) {
                    UpdateHistory("Hybrid", clC.GetFields("hybrid"), chkHybridMod.Checked.ToString());
                }
                if (chkCommercialMod.Checked != Convert.ToBoolean(clC.GetFields("commercial"))) {
                    UpdateHistory("Commercial", clC.GetFields("commercial"), chkCommercial.Checked.ToString());
                }
                if (chkHydraulicMod.Checked != Convert.ToBoolean(clC.GetFields("hydraulic"))) {
                    UpdateHistory("Hydraulic", clC.GetFields("hydraulic"), chkHydraulicMod.Checked.ToString());
                }
                if (chkAirBladderMod.Checked != Convert.ToBoolean(clC.GetFields("airbladder"))) {
                    UpdateHistory("Air Bladder", clC.GetFields("airbaldder"), chkAirBladderMod.Checked.ToString());
                }
                if (chkLiftKitMod.Checked != Convert.ToBoolean(clC.GetFields("LiftKit"))) {
                    UpdateHistory("Lift Kit", clC.GetFields("liftkit"), chkLiftKitMod.Checked.ToString());
                }
                if (chkLuxuryMod.Checked != Convert.ToBoolean(clC.GetFields("luxury"))) {
                    UpdateHistory("Luxury", clC.GetFields("luxury"), chkLuxuryMod.Checked.ToString());
                }
                if (chkSealsMod.Checked != Convert.ToBoolean(clC.GetFields("seals"))) {
                    UpdateHistory("Seals", clC.GetFields("seals"), chkSealsMod.Checked.ToString());
                }
                if (chkSnowPlowMod.Checked != Convert.ToBoolean(clC.GetFields("snowplow"))) {
                    UpdateHistory("Snow Plow", clC.GetFields("snowplow"), chkSnowPlowMod.Checked.ToString());
                }
                if (chkLargerLiftKitMod.Checked != Convert.ToBoolean(clC.GetFields("largerliftkit"))) {
                    UpdateHistory("Larger Lift Kit", clC.GetFields("largerliftkit"), chkLargerLiftKitMod.Checked.ToString());
                }
            }
        }
        private void UpdateHistory(string xFieldName, string xOldValue, string xNewValue)
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            SQL = "select * from contracthistory where contracthistoryid =  0 ";
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            clC.NewRow();
            clC.SetFields("contractid", hfContractID.Value);
            clC.SetFields("fieldname", xFieldName);
            clC.SetFields("OldValue", xOldValue);
            clC.SetFields("newvalue", xNewValue);
            clC.SetFields("creby", hfUserID.Value);
            clC.SetFields("credate", DateTime.Today.ToString());
            clC.AddRow();
            clC.SaveDB();
        }
    }
}