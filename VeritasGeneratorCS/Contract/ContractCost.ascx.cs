﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Contract
{
    public partial class ContractCost : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hfContractID.Value = Request.QueryString["contractid"];
                GetServerInfo();
                FillScreen();
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private void FillScreen()
        {
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            string SQL;
            SQL = "select moxydealercost, discountamt, customercost, markup " +
                  "from contract " +
                  "where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                if (clC.GetFields("customercost").Length == 0)
                {
                    clC.SetFields("customercost", 0.ToString());
                }
                if (clC.GetFields("markup").Length == 0)
                {
                    clC.SetFields("markup", 0.ToString());
                }
                if (clC.GetFields("moxydealercost").Length == 0)
                {
                    clC.SetFields("moxydealercost", 0.ToString());
                }
                if (clC.GetFields("discountamt").Length == 0)
                {
                    clC.SetFields("discountamt", 0.ToString());
                }
                txtCustomerCost.Text = Convert.ToDouble(clC.GetFields("customercost")).ToString("#,##0.00");
                txtMarkup.Text = Convert.ToDouble(clC.GetFields("markup")).ToString("#,##0.00");
                txtDealerCost.Text = Convert.ToDouble(clC.GetFields("moxydealercost")).ToString("#,##0.00");
                txtDiscountAmt.Text = Convert.ToDouble(clC.GetFields("discountamt")).ToString("#,##0.00");
                txtAddMarkup.Text = (Convert.ToDouble(clC.GetFields("customercost")) - Convert.ToDouble(clC.GetFields("markup")) - Convert.ToDouble(clC.GetFields("moxydealercost")) + Convert.ToDouble(clC.GetFields("discountamt"))).ToString("#,##0.00");
            }
        }
    }
}