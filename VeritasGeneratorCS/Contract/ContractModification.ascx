﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractModification.ascx.cs" Inherits="VeritasGeneratorCS.Contract.ContractModification" %>
<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell VerticalAlign="Top">
            <asp:Table runat="server">
                <asp:TableRow>
                    <asp:TableCell Font-Bold="true">
                        Sale Mile:
                    </asp:TableCell>
                    <asp:TableCell>
                        <telerik:RadNumericTextBox ID="txtSaleMile" NumberFormat-DecimalDigits="0" runat="server"></telerik:RadNumericTextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Font-Bold="true">
                        Sale Date:
                    </asp:TableCell>
                    <asp:TableCell>
                        <telerik:RadDateTimePicker ID="txtSaleDate" runat="server"></telerik:RadDateTimePicker>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow Font-Bold="true">
                    <asp:TableCell>
                        Lienholder:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="txtLienholder" runat="server"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Font-Bold="true">
                        Dealer Cost
                    </asp:TableCell>
                    <asp:TableCell>
                        <telerik:RadNumericTextBox ID="txtDealerCost" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Font-Bold="true">
                        Customer Cost
                    </asp:TableCell>
                    <asp:TableCell>
                        <telerik:RadNumericTextBox ID="txtCustomerCost" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        &nbsp
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Button ID="btnSave" OnClick="btnSave_Click" BackColor="#1eabe2" runat="server" Text="Save" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell VerticalAlign="Top">
            <asp:Table runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgHistory">
                            <telerik:RadGrid ID="rgHistory" runat="server" AutoGenerateColumns="false" AllowSorting="true">
                                <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true"
                                    CommandItemDisplay="TopAndBottom">
                                    <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false" />
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="ContractHistoryID" UniqueName="ContractHistoryID" Visible="false"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="FieldName" UniqueName="FieldName" HeaderText="Field Name"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="OldValue" UniqueName="OldValue" HeaderText="Old Value"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="NewValue" UniqueName="NewValue" HeaderText="New Value"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CreDate" UniqueName="Credate" HeaderText="Mod Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="UserName" UniqueName="UserName" HeaderText="Mod By"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                        </telerik:RadAjaxPanel>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

<asp:HiddenField ID="hfContractID" runat="server" />
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />
