﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Contract
{
    public partial class ContractBreakdown : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsRateCategory.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsRateType.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            if (!IsPostBack)
            {
                GetServerInfo();
                hfContractID.Value = Request.QueryString["contractid"];
                pnlAddRateType.Visible = false;
                pnlModify.Visible = false;
                pnlSearchRateType.Visible = false;
                FillRateTypeGrid();
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private void FillRateTypeGrid()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string SQL;
            SQL = "select contractamtid, ratetypename, categoryname, amt, cancelamt from contractamt ca " +
                  "left join ratetype rt on rt.ratetypeid = ca.ratetypeid " +
                  "left join ratecategory rc on rt.ratecategoryid = rc.ratecategoryid " +
                  "where contractid = " + hfContractID.Value + " " +
                  "order by orderid, ratetypename ";

            rgAmount.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            rgAmount.DataBind();
        }

        protected void rgAmount_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfContractAmtID.Value = rgAmount.SelectedValue.ToString();
            if (CheckCommission())
            {
                return;
            }
            pnlAmounts.Visible = false;
            pnlModify.Visible = true;
            FillModify();
        }
        private bool CheckCommission()
        {
            clsDBO.clsDBO clCA = new clsDBO.clsDBO();
            string SQL;
            SQL = "select * from contractamt ca " +
                  "left join ratetype rt on rt.ratetypeid = ca.ratetypeid " +
                  "where contractamtid  = " + hfContractAmtID.Value;
            clCA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCA.RowCount() > 0)
            {
                clCA.GetRow();
                if (Convert.ToInt32(clCA.GetFields("ratecategoryid")) == 2)
                {
                    return true;
                }
            }
            return false;
        }
        private void FillModify()
        {
            string SQL;
            clsDBO.clsDBO clCA = new clsDBO.clsDBO();
            SQL = "select * from contractamt ca " +
                  "left join ratetype rt on rt.ratetypeid = ca.ratetypeid " +
                  "where contractamtid = " + hfContractAmtID.Value;
            clCA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCA.RowCount() > 0)
            {
                clCA.GetRow();
                hfRateTypeID.Value = clCA.GetFields("ratetypeid");
                txtRateType.Text = clCA.GetFields("ratetypename");
                txtAmount.Text = clCA.GetFields("amt");
                txtCancelAmt.Text = clCA.GetFields("cancelamt");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlModify.Visible = false;
            pnlAmounts.Visible = true;
        }
    }
}