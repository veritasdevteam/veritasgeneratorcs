﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Contract
{
    public partial class ContractPayment : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            SqlDataSource2.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            GetServerInfo();

            if (!IsPostBack)
            {
                ReadOnlyButtons();
                hfContractID.Value = Request.QueryString["contractid"];
                pnlPaymentList.Visible = true;
                pnlPayment.Visible = false;
                pnlPayeeSearch.Visible = false;
                pnlPayeeModify.Visible = false;
            }
        }
        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly").ToLower() == "true")
                {
                    btnAddPayee.Enabled = false;
                    btnAddPayment.Enabled = false;
                    btnPayeeSearch.Enabled = false;
                    btnSavePayment.Enabled = false;
                }
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

    }
}