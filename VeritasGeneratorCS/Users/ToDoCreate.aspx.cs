﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Users
{
    public partial class ToDoCreate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsMessage.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsUserInfo.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            GetServerInfo();
            if (!IsPostBack)
            {
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
                pnlDetail.Visible = false;
                pnlList.Visible = true;
                pnlSeekUser.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnReports.Enabled = false;
            btnUserSettings.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true)
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true)
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true)
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true)
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true)
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true)
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("usersettings")) == true)
                {
                    btnUserSettings.Enabled = true;
                }
            }
        }
        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnToDoReader_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/todoreader.aspx?sid=" + hfID.Value);
        }

        protected void btnToDoCreate_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/todocreate.aspx?sid=" + hfID.Value);
        }

        protected void btnUserSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/usersettings.aspx?sid=" + hfID.Value);
        }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/ChangePassword.aspx?sid=" + hfID.Value);
        }

        protected void btnAddMessage_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            ClearDetail();
        }
        private void ClearDetail()
        {
            hfIDX.Value = "0";
            hfToID.Value = "0";
            txtFrom.Text = GetUserName();
            txtTo.Text = "";
            txtHeader.Text = "";
            txtMessage.Text = "";
            txtMessageDate.Text = "";
            btnSendMessage.Visible = true;
        }
        protected void rgMessage_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            txtFrom.Text = GetUserName();
            hfIDX.Value = rgMessage.SelectedValue.ToString();
            btnSendMessage.Visible = false;
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usermessage where idx = " + hfIDX.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfToID.Value = clR.GetFields("toid");
                txtTo.Text = GetToName();
                txtHeader.Text = clR.GetFields("header");
                txtMessage.Text = clR.GetFields("message");
                txtMessageDate.Text = clR.GetFields("messagedate");
            }
        }

        protected void btnSeekUser_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlSeekUser.Visible = true;
        }

        protected void rgUserInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfToID.Value = rgUserInfo.SelectedValue.ToString();
            txtTo.Text = GetToName();
            pnlDetail.Visible = true;
            pnlSeekUser.Visible = false;
        }

        protected void btnCancelMessage_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgMessage.SelectedIndexes.Clear();
        }

        protected void btnSendMessage_Click(object sender, EventArgs e)
        {
            if (hfIDX.Value != "0")
            {
                return;
            }
            if (hfToID.Value == "0")
            {
                return;
            }
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usermessage where idx = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("toid", hfToID.Value);
                clR.SetFields("fromid", hfUserID.Value);
                clR.SetFields("header", txtHeader.Text);
                clR.SetFields("message", txtMessage.Text);
                clR.SetFields("messagedate", DateTime.Now.ToString());
                clR.AddRow();
                clR.SaveDB();
            }
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgMessage.Rebind();
        }
        private string GetToName()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from userinfo where userid = " + hfToID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("fname") + " " + clR.GetFields("lname");
            }
            return "";
        }
        private string GetUserName()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from userinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("fname") + " " + clR.GetFields("lname");
            }
            return "";
        }

        protected void btnBackToMessage_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = true;
            pnlSeekUser.Visible = false;
        }
    }
}