﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ToDoCreate.aspx.cs" Inherits="VeritasGeneratorCS.Users.ToDoCreate" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>To Do Creation</title>
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" BorderStyle="None" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnToDoReader" OnClick="btnToDoReader_Click" runat="server" BorderStyle="None" Text="To Do Reader"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnToDoCreate" OnClick="btnToDoCreate_Click" runat="server" BorderStyle="None" Text="To Do Creation"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUserSettings" OnClick="btnUserSettings_Click" runat="server" BorderStyle="None" Text="User Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnChangePassword" OnClick="btnChangePassword_Click" runat="server" BorderStyle="None" Text="Change Password"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" BorderStyle="None" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" BorderStyle="None" Text="Dealerships"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" BorderStyle="None" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" BorderStyle="None" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" BorderStyle="None" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" BorderStyle="None" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" OnClick="btnSettings_Click" runat="server" BorderStyle="None" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" BorderStyle="None" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" BorderStyle="None" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel runat="server" ID="pnlList">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="btnAddMessage" OnClick="btnAddMessage_Click" runat="server" Text="Create Message" BackColor="#1eabe2" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadGrid ID="rgMessage" OnSelectedIndexChanged="rgMessage_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                                AllowSorting="true" Height="500"  Width="1000" ShowFooter="true" DataSourceID="dsMessage">
                                                <GroupingSettings CaseSensitive="false" />
                                                <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="IDX" ShowFooter="true">
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="idx" ReadOnly="true" Visible="false" UniqueName="idx"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="FName" UniqueName="FName" HeaderText="First Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="LName" UniqueName="LName" HeaderText="Last Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Header" UniqueName="Header" HeaderText="Header" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="MessageDate" UniqueName="MessageDaste" HeaderText="Message Date" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings EnablePostBackOnRowClick="true" Scrolling-AllowScroll="true">
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="dsMessage"
                                            ProviderName="System.Data.SqlClient" 
                                            SelectCommand="select um.idx, ui.FName, ui.LName, um.Header, um.MessageDate  from userinfo ui 
                                            inner join usermessage um on ui.UserID = um.toID where um.fromid = @UserID order by um.messagedate desc" runat="server">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="hfUserID" Name="UserID" PropertyName="Value" Type="Int32" />
                                            </SelectParameters>
                                            </asp:SqlDataSource>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>

                            <asp:Panel runat="server" ID="pnlDetail">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            From
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtFrom" ReadOnly="true" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            To:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtTo" ReadOnly="true" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnSeekUser" OnClick="btnSeekUser_Click" runat="server" Text="Seek User" BackColor="#1eabe2" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Header:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtHeader" Width="500" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Message:
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell ColumnSpan="2">
                                            <asp:TextBox ID="txtMessage" Width="1000" Height="500" TextMode="MultiLine" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Message Date:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtMessageDate" ReadOnly="true" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell ColumnSpan="2" HorizontalAlign="Right">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Button ID="btnCancelMessage" OnClick="btnCancelMessage_Click" runat="server" Text="Close Message" BackColor="#1eabe2" />
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Button ID="btnSendMessage" OnClick="btnSendMessage_Click" runat="server" Text="Send Message" BackColor="#1eabe2" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlSeekUser">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="btnBackToMessage" OnClick="btnBackToMessage_Click" runat="server" Text="Back To Message" BackColor="#1eabe2" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadGrid ID="rgUserInfo" OnSelectedIndexChanged="rgUserInfo_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                                AllowSorting="true" Width="1000" Height="500" ShowFooter="true" DataSourceID="dsUserInfo">
                                                <GroupingSettings CaseSensitive="false" />
                                                <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="UserID" ShowFooter="true">
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="UserID" ReadOnly="true" Visible="false" UniqueName="UserID"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="FName" UniqueName="FName" HeaderText="First Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="LName" UniqueName="LName" HeaderText="Last Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="email" UniqueName="Email" HeaderText="E-Mail" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings EnablePostBackOnRowClick="true"  Scrolling-AllowScroll="true">
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="dsUserInfo"
                                            ProviderName="System.Data.SqlClient" 
                                            SelectCommand="select ui.userid, ui.FName, ui.LName, ui.email  from userinfo ui where active <> 0 order by ui.lname, ui.fname" runat="server">
                                            </asp:SqlDataSource>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfIDX" runat="server" />
                <asp:HiddenField ID="hfToID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
    </form>
</body>
</html>
