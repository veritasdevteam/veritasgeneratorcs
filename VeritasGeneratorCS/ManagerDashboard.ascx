﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManagerDashboard.ascx.cs" Inherits="VeritasGeneratorCS.ManagerDashboard" %>
<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell>
            <telerik:RadHtmlChart ID="htmlInbound" runat="server" Visible="false" Width="400" Height="400" Transitions="false">
                <PlotArea>
                <Series>
                    <telerik:VerticalBulletSeries DataCurrentField="count" DataTargetField="goal">
                    </telerik:VerticalBulletSeries>
                </Series>
                <XAxis DataLabelsField="CallDate">
                    <MinorGridLines Visible="false"></MinorGridLines>
                    <MajorGridLines Visible="false"></MajorGridLines>
                </XAxis>
                <YAxis MaxValue="40">
                    <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                    <MinorGridLines Visible="false"></MinorGridLines>
                </YAxis>
            </PlotArea>
            <Legend>
                <Appearance Visible="false"></Appearance>
            </Legend>
            <ChartTitle Text="Stats"></ChartTitle>
            </telerik:RadHtmlChart>
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadHtmlChart ID="htmlOutBound" runat="server" Visible="false" Width="400" Height="400" Transitions="false">
                <PlotArea>
                    <Series>
                        <telerik:VerticalBulletSeries DataCurrentField="count" DataTargetField="goal">
                        </telerik:VerticalBulletSeries>
                    </Series>                        
                    <XAxis DataLabelsField="CallDate">
                        <MinorGridLines Visible="false"></MinorGridLines>
                        <MajorGridLines Visible="false"></MajorGridLines>
                    </XAxis>
                    <YAxis MaxValue="40">
                        <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                        <MinorGridLines Visible="false"></MinorGridLines>
                    </YAxis>
                </PlotArea>
                <Legend>
                    <Appearance Visible="false"></Appearance>
                </Legend>
                <ChartTitle Text="Stats"></ChartTitle>
            </telerik:RadHtmlChart>
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadHtmlChart ID="htmlDnD" runat="server" Visible="false" Width="400" Height="400" Transitions="false">
                <PlotArea>
                    <Series>
                        <telerik:VerticalBulletSeries DataCurrentField="count" DataTargetField="goal">
                        </telerik:VerticalBulletSeries>
                    </Series>                        
                    <XAxis DataLabelsField="CallDate">
                        <MinorGridLines Visible="false"></MinorGridLines>
                        <MajorGridLines Visible="false"></MajorGridLines>
                    </XAxis>
                    <YAxis MaxValue="400">
                        <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                        <MinorGridLines Visible="false"></MinorGridLines>
                    </YAxis>
                </PlotArea>
                <Legend>
                    <Appearance Visible="false"></Appearance>
                </Legend>
                <ChartTitle Text="Stats"></ChartTitle>
            </telerik:RadHtmlChart>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <telerik:RadHtmlChart ID="htmlClaimsOpened" runat="server" Visible="false" Width="400" Height="400" Transitions="false">
                <PlotArea>
                    <Series>
                        <telerik:VerticalBulletSeries DataCurrentField="numOfClaimsOpened" DataTargetField="goal">
                        </telerik:VerticalBulletSeries>
                    </Series>
                    <XAxis DataLabelsField="CreDate">
                        <MinorGridLines Visible="false"></MinorGridLines>
                        <MajorGridLines Visible="false"></MajorGridLines>
                    </XAxis>
                    <YAxis MaxValue="50">
                        <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                        <MinorGridLines Visible="false"></MinorGridLines>
                    </YAxis>
                </PlotArea>
                <Legend>
                    <Appearance Visible="false"></Appearance>
                </Legend>                    
                <ChartTitle Text="Stats"></ChartTitle>
            </telerik:RadHtmlChart>
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadHtmlChart ID="htmlNumOfClaimsMovedFromOpen" runat="server" Visible="false" Width="400" Height="400" Transitions="false">
                <PlotArea>
                    <Series>
                        <telerik:VerticalBulletSeries DataCurrentField="numOfClaimsClosed" DataTargetField="goal">
                        </telerik:VerticalBulletSeries>
                    </Series>
                    <XAxis DataLabelsField="CloseDate">
                        <MinorGridLines Visible="false"></MinorGridLines>
                        <MajorGridLines Visible="false"></MajorGridLines>
                    </XAxis>
                    <YAxis MaxValue="50">
                        <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                        <MinorGridLines Visible="false"></MinorGridLines>
                    </YAxis>
                </PlotArea>
                <Legend>
                    <Appearance Visible="false"></Appearance>
                </Legend>                    
                <ChartTitle Text="Stats"></ChartTitle>
            </telerik:RadHtmlChart>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="2" VerticalAlign="Top">
            <telerik:RadGrid ID="rgClaimOpen" OnSelectedIndexChanged="rgClaimOpen_SelectedIndexChanged" OnNeedDataSource="rgClaimOpen_NeedDataSource" runat="server" AutoGenerateColumns ="true" AllowFilteringByColumn="false" AllowSorting="true" ShowFooter ="true" ShowHeader="true" AllowPaging="true" PagerStyle-HorizontalAlign="Left" PagerStyle-Position="bottom">
                <PagerStyle EnableAllOptionInPagerComboBox="true" />
                    <MasterTableView AutoGenerateColumns ="false" PageSize="15" ShowFooter="true" DataKeyNames="ClaimID">
                        <Columns>
                            <telerik:GridBoundColumn DataField="ClaimID" ReadOnly="true" Visible="false" UniqueName="ClaimID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ClaimNo" UniqueName="ClaimNo" HeaderText="Claim No"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CreDate" UniqueName="CreDate" HeaderText="Loss date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ModDate" UniqueName="ModDate" HeaderText="Last Activity" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ClaimAge" UniqueName="ClaimAge" HeaderText="Claim Age"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ActivityDesc" UniqueName="ActivityDesc" HeaderText="Activity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Adjuster" UniqueName="Adjuster" HeaderText="Adjuster" Visible="false"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
            </telerik:RadGrid>
        </asp:TableCell>
        <asp:TableCell ColumnSpan="2" VerticalAlign="Top">
            <telerik:RadGrid ID="rgClaimManagment" OnSelectedIndexChanged="rgClaimManagment_SelectedIndexChanged" OnNeedDataSource="rgClaimManagment_NeedDataSource" runat="server" AutoGenerateColumns ="true" AllowFilteringByColumn="false" AllowSorting="true" ShowFooter ="true" ShowHeader="true" AllowPaging="true" PagerStyle-HorizontalAlign="Left" PagerStyle-Position="bottom">
                <PagerStyle EnableAllOptionInPagerComboBox="true" />
                    <MasterTableView AutoGenerateColumns ="false" PageSize="15" ShowFooter="true" DataKeyNames="ClaimID">
                        <Columns>
                            <telerik:GridBoundColumn DataField="ClaimID" ReadOnly="true" Visible="false" UniqueName="ClaimID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ClaimNo" UniqueName="ClaimNo" HeaderText="Claim No"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CreDate" UniqueName="CreDate" HeaderText="Loss date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ModDate" UniqueName="ModDate" HeaderText="Last Activity" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ClaimAge" UniqueName="ClaimAge" HeaderText="Claim Age"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ActivityDesc" UniqueName="ActivityDesc" HeaderText="Activity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Adjuster" UniqueName="Adjuster" HeaderText="Adjuster"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
            </telerik:RadGrid>
        </asp:TableCell>
        <asp:TableCell>
            <asp:DropDownList ID="ddlAgents" runat="server" Visible="false" ></asp:DropDownList>
            <asp:Button ID="btnChangeGraphs" OnClick="btnChangeGraphs_Click" Text="Select" runat="server" Visible="false" BackColor="#1a4688" ForeColor="White" BorderColor="#1a4688" Width="75"/>
            <asp:Button ID="btnRevertGraphs" OnClick="btnRevertGraphs_Click" Text="Undo" runat="server" Visible="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Visible ="false" ID ="tblecSetGoals">
            <asp:Table runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label runat="server" Text ="Inbound goal"></asp:Label> <asp:TextBox runat="server" ID="txbSetGoalInbound"  ></asp:TextBox>
                    </asp:TableCell>                                                    
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label runat="server" Text ="Outbound goal"></asp:Label><asp:TextBox runat="server" ID="txbSetGoalOutbound" ></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label runat="server" Text ="DnD goal"></asp:Label><asp:TextBox runat="server" ID="txbSetGoalDnD" ></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfTeamID" runat="server" />
