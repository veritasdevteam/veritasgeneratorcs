﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CommissionCancel.aspx.cs" Inherits="VeritasGeneratorCS.Accounting.CommissionCancel" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Commission Cancel</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" BorderStyle="None" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCommission" OnClick="btnCommission_Click" runat="server" BorderStyle="None" Text="Commission"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnPaylink" OnClick="btnPaylink_Click" runat="server" BorderStyle="None" Text="Paylink/Mepco"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnWex" OnClick="btnWex_Click" runat="server" BorderStyle="None" Text="Wex"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccountAdj" OnClick="btnAccountAdj_Click" runat="server" BorderStyle="None" Text="Account Adjustment"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCommissionCancel" OnClick="btnCommissionCancel_Click" runat="server" BorderStyle="None" Text="Commission Cancel"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" BorderStyle="None" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" BorderStyle="None" Text="Dealerships"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" BorderStyle="None" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" BorderStyle="None" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" BorderStyle="None" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" BorderStyle="None" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" OnClick="btnSettings_Click" runat="server" BorderStyle="None" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" BorderStyle="None" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" BorderStyle="None" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Table runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell Font-Bold="true">
                                                    Commission Cancel Type:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="cboCommissionCancelType" OnSelectedIndexChanged="cboCommissionCancelType_SelectedIndexChanged" AutoPostBack="true" DataSourceID="dsCommissionCancelType" DataTextField="CommissionCancelType" DataValueField="CommissionCancelTypeID" runat="server"></asp:DropDownList>
                                                    <asp:SqlDataSource ID="dsCommissionCancelType" 
                                                    ProviderName="System.Data.SqlClient" SelectCommand="select CommissionCancelTypeID, CommissionCancelType from CommissionCancelType order by CommissionCancelTypeID" runat="server"></asp:SqlDataSource>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" PostBackControls="rgCommission">
                                            <telerik:RadGrid ID="rgCommission" OnPageIndexChanged="rgCommission_PageIndexChanged" runat="server" DataSourceID="SqlDataSource1" AutoGenerateColumns="false"
                                                AllowSorting="true" AllowPaging="true"  Width="1500" ShowFooter="true" AllowMultiRowSelection="true">
                                                <GroupingSettings CaseSensitive="false" />
                                                <MasterTableView AutoGenerateColumns="false" DataKeyNames="ContractID" PageSize="10" ShowFooter="true" CommandItemDisplay="TopAndBottom">
                                                <CommandItemSettings ShowExportToExcelButton="false" ShowExportToCsvButton="true" ShowAddNewRecordButton="false" />
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="ContractID"  ReadOnly="true" Visible="false" UniqueName="ContractID"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ContractNo" UniqueName="ContractNo" HeaderText="Contract No" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DealerNo" UniqueName="DealerNo" HeaderText="Dealer No" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DealerName" UniqueName="Dealer" HeaderText="Dealer Name" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="300" HeaderStyle-Width="300"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="CustomerName" UniqueName="CustomerName" HeaderText="Customer Name" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="SaleDate" UniqueName="SaleDate" HeaderText="Sale Date" ItemStyle-Width="100" HeaderStyle-Width ="100" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DatePaid" UniqueName="DatePaid" HeaderText="Paid Date" ItemStyle-Width="100" HeaderStyle-Width ="100" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="CancelEffDate" UniqueName="CancelEffDate" HeaderText="Cancel Eff Date" ItemStyle-Width="100" HeaderStyle-Width ="100" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="CancelDate" UniqueName="CancelDate" HeaderText="Cancel Date" ItemStyle-Width="100" HeaderStyle-Width ="100" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="MoxyDealerCost" UniqueName="MoxyDealerCost" HeaderText="Dealer Cost" ItemStyle-Width="75" HeaderStyle-Width="75" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Amt" UniqueName="Amt" HeaderText="Amt" ItemStyle-Width="75" HeaderStyle-Width="75" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="AgentName" UniqueName="AgentName" HeaderText="Agent" ItemStyle-Width="125" HeaderStyle-Width="125"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Addr1" UniqueName="Addr1" HeaderText="Addr 1" ItemStyle-Width="125" HeaderStyle-Width="125"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Addr2" UniqueName="Addr2" HeaderText="Addr 2" ItemStyle-Width="125" HeaderStyle-Width="125"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Addr3" UniqueName="Addr3" HeaderText="Addr 3" ItemStyle-Width="125" HeaderStyle-Width="125"></telerik:GridBoundColumn>
                                                        <telerik:GridClientSelectColumn UniqueName="ClaimSelect"></telerik:GridClientSelectColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings >
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="SqlDataSource1"
                                            ProviderName="System.Data.SqlClient" SelectCommand=" " runat="server"></asp:SqlDataSource>
                                        </telerik:RadAjaxPanel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="1000" HorizontalAlign="Right">
                                        <asp:Button ID="btnMoveToPayment" OnClick="btnMoveToPayment_Click" runat="server" Text="Move To Payment"  BackColor="#1eabe2" BorderColor="#1eabe2" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgPayment">
                                            <telerik:RadGrid ID="rgPayment" OnItemCommand="rgPayment_ItemCommand" runat="server" AutoGenerateColumns="false" DataSourceID="SQLDataSource2"
                                                AllowSorting="true" AllowPaging="true" ShowFooter="true"  MasterTableView-CommandItemDisplay="TopAndBottom"
                                                    Font-Names="Calibri" Font-Size="Small" AllowMultiRowSelection="true">
                                                <GroupingSettings CaseSensitive="false" />
                                                <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true" Width="1500" DataKeyNames="ContractID">
                                                    <CommandItemSettings ShowExportToExcelButton="false" ShowExportToCsvButton="true" ShowAddNewRecordButton="false" />
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="ContractID"  ReadOnly="true" Visible="false" UniqueName="ContractID"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ContractNo" UniqueName="ContractNo" HeaderText="Contract No" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DealerNo" UniqueName="DealerNo" HeaderText="Dealer No" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DealerName" UniqueName="Dealer" HeaderText="Dealer Name" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="300" HeaderStyle-Width="300"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="CustomerName" UniqueName="CustomerName" HeaderText="Customer Name" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="SaleDate" UniqueName="SaleDate" HeaderText="Sale Date" ItemStyle-Width="100" HeaderStyle-Width ="100" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DatePaid" UniqueName="DatePaid" HeaderText="Paid Date" ItemStyle-Width="100" HeaderStyle-Width ="100" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="CancelEffDate" UniqueName="CancelEffDate" HeaderText="Cancel Eff Date" ItemStyle-Width="100" HeaderStyle-Width ="100" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="CancelDate" UniqueName="CancelDate" HeaderText="Cancel Date" ItemStyle-Width="100" HeaderStyle-Width ="100" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="MoxyDealerCost" UniqueName="MoxyDealerCost" HeaderText="Dealer Cost" ItemStyle-Width="75" HeaderStyle-Width="75" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Amt" UniqueName="Amt" HeaderText="Amt" ItemStyle-Width="75" HeaderStyle-Width="75" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="AgentName" UniqueName="AgentName" HeaderText="Agent" ItemStyle-Width="125" HeaderStyle-Width="125"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Addr1" UniqueName="Addr1" HeaderText="Addr 1" ItemStyle-Width="125" HeaderStyle-Width="125"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Addr2" UniqueName="Addr2" HeaderText="Addr 2" ItemStyle-Width="125" HeaderStyle-Width="125"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Addr3" UniqueName="Addr3" HeaderText="Addr 3" ItemStyle-Width="125" HeaderStyle-Width="125"></telerik:GridBoundColumn>
                                                        <telerik:GridButtonColumn CommandName="DeleteRow" CommandArgument="ClaimID" ButtonType="ImageButton" ImageUrl="~/images/delete.png"></telerik:GridButtonColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings>
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="SqlDataSource2"
                                            ProviderName="System.Data.SqlClient" SelectCommand=" " runat="server"></asp:SqlDataSource>
                                        </telerik:RadAjaxPanel>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
    </form>
</body>
</html>
