﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Accounting
{
    public partial class Wex : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsWexAcct.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsWexTransfer.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsPaid.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsPending.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsVoid.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            tbTodo.Width = pnlHeader.Width;
            if (!IsPostBack)
            {
                GetServerInfo();
                CheckToDo();
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
                FillTotals();
                pnlAdd.Visible = false;
                pnlPaid.Visible = false;
                rdpTransferDate.SelectedDate = DateTime.Today;
                txtTransferAmt.Text = "0";
                cboWexAcct.SelectedValue = "1";
                hfWexAPIID.Value = "1";
                pnlTransferList.Visible = false;
                pnlPending.Visible = false;
                pnlVoid.Visible = false;
            }
        }
        private void FillTotals()
        {
            GetTotalTransfer();
            GetTotalPaid();
            GetTotalPending();
            GetTotalVoid();
            txtBalance.Text = (Convert.ToDouble(txtDeposit.Text) - Convert.ToDouble(txtPaid.Text) - Convert.ToDouble(txtPending.Text)).ToString("#,##0.00");
        }
        private void GetTotalVoid()
        {
            string SQL;
            clsDBO.clsDBO clWA = new clsDBO.clsDBO();
            SQL = "select sum(cast(paymentamt as money)) as Amt  from ClaimWexPayment cwp " +
                  "inner join wexapi wa on wa.CompanyNo = cwp.CompanyNo " +
                  "where reasoncode = 'success' " +
                  "and not VoidDate is null " +
                  "and paiddate is null ";
            if (cboWexAcct.SelectedValue.Length > 0)
            {
                SQL = SQL + "and wexapiid = " + cboWexAcct.SelectedValue + " ";
            }
            else
            {
                SQL = SQL + "and wexapiid = 1 ";
            }
            clWA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clWA.RowCount() > 0)
            {
                clWA.GetRow();
                if (clWA.GetFields("amt").Length > 0)
                {
                    txtVoid.Text = Convert.ToDouble(clWA.GetFields("amt")).ToString("#,##0.00");
                }
                else
                {
                    txtVoid.Text = 0.ToString("#,##0.00");
                }
            }
        }
        private void GetTotalPending()
        {
            string SQL;
            clsDBO.clsDBO clWA = new clsDBO.clsDBO();
            SQL = "select sum(cast(paymentamt as money)) as Amt  from ClaimWexPayment cwp " +
                  "inner join wexapi wa on wa.CompanyNo = cwp.CompanyNo " +
                  "where reasoncode = 'success' " +
                  "and paiddate is null " +
                  "and VoidDate is null ";
            if (cboWexAcct.SelectedValue.Length > 0)
            {
                SQL = SQL + "and wexapiid = " + cboWexAcct.SelectedValue + " ";
            } 
            else 
            {
                SQL = SQL + "and wexapiid = 1 ";
            }
            clWA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clWA.RowCount() > 0)
            {
                clWA.GetRow();
                if (clWA.GetFields("amt").Length > 0)
                {
                    txtPending.Text = Convert.ToDouble(clWA.GetFields("amt")).ToString("#,##0.00");
                }
                else
                {
                    txtPending.Text = 0.ToString("#,##0.00");
                }
            }
        }
        private void GetTotalPaid()
        {
            string SQL;
            clsDBO.clsDBO clWA = new clsDBO.clsDBO();
            SQL = "select sum(cast(paymentamt as money)) as Amt  from ClaimWexPayment cwp " +
                  "inner join wexapi wa on wa.CompanyNo = cwp.CompanyNo " +
                  "where reasoncode = 'success' " +
                  "and not paiddate is null ";
            if (cboWexAcct.SelectedValue.Length > 0)
            {
                SQL = SQL + "and wexapiid = " + cboWexAcct.SelectedValue;
            } 
            else 
            {
                SQL = SQL + "and wexapiid = 1 ";
            }
            clWA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clWA.RowCount() > 0)
            {
                clWA.GetRow();
                if (clWA.GetFields("amt").Length > 0)
                {
                    txtPaid.Text = Convert.ToDouble(clWA.GetFields("amt")).ToString("#,##0.00");
                }
                else
                {
                    txtPaid.Text = 0.ToString("#,##0.00");
                }
            }
        }
        private void GetTotalTransfer()
        {
            string SQL;
            clsDBO.clsDBO clWT = new clsDBO.clsDBO();
            SQL = "select sum(amt) as Amt from wextransfer ";
            if (cboWexAcct.SelectedValue.Length > 0)
            {
                SQL = SQL + "where wexapiid = " + cboWexAcct.SelectedValue + " ";
            }
            else
            {
                SQL = SQL + "where wexapiid = 1 ";
            }
            clWT.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clWT.RowCount() > 0)
            {
                clWT.GetRow();
                if (clWT.GetFields("amt").Length > 0)
                {
                    txtDeposit.Text = Convert.ToDouble(clWT.GetFields("amt")).ToString("#,##0.00");
                } 
                else 
                {
                    txtDeposit.Text = 0.ToString("#,##0.00");
                }
            }
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO.clsDBO clTD = new clsDBO.clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnReports.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true)
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true)
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true)
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true)
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true)
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true)
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true)
                {
                    btnReports.Enabled = true;
                }
            }
        }
        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }
        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnCommission_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/Commission.aspx?sid=" + hfID.Value);
        }

        protected void btnPaylink_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/Paylink.aspx?sid=" + hfID.Value);
        }

        protected void btnAutoNationACH_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/autonationach.aspx?sid=" + hfID.Value);
        }

        protected void btnWex_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/wex.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountAdj_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/AccountAdjustments.aspx?sid=" + hfID.Value);
        }

        protected void btnCommissionCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/CommissionCancel.aspx?sid=" + hfID.Value);
        }

        protected void cboWexAcct_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfWexAPIID.Value = cboWexAcct.SelectedValue;
            FillTotals();
            rgWexTransfer.Rebind();
            rgPaid.Rebind();
            rgPending.Rebind();
            rgVoid.Rebind();
        }

        protected void btnAddDeposit_Click(object sender, EventArgs e)
        {
            txtTransferAmt.Text = "";
            pnlAdd.Visible = true;
            pnlTotal.Visible = false;
            hfAdd.Value = "true";
            hfWexTransferID.Value = "0";
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            if (hfAdd.Value.ToLower() == "true")
            {
                pnlTotal.Visible = true;
            } 
            else 
            {
                pnlTransferList.Visible = true;
            }
            pnlAdd.Visible = false;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clWT = new clsDBO.clsDBO();
            SQL = "select * from wextransfer where wextransferid = " + hfWexTransferID.Value;
            clWT.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clWT.RowCount() > 0)
            {
                clWT.GetRow();
            } 
            else 
            {
                clWT.NewRow();
            }
            clWT.SetFields("wexapiid", cboWexAcct.SelectedValue);
            if (rdpTransferDate.SelectedDate != null)
            {
                clWT.SetFields("transferdate", rdpTransferDate.SelectedDate.ToString());
            }
            if (txtTransferAmt.Text.Length > 0)
            {
                clWT.SetFields("amt", txtTransferAmt.Text);
            }
            if (clWT.RowCount() == 0)
            {
                clWT.AddRow();
            }
            clWT.SaveDB();

            if (hfAdd.Value.ToLower() == "true")
            {
                pnlTotal.Visible = true;
            }
            else
            {
                pnlTransferList.Visible = true;
            }
            pnlAdd.Visible = false;
        }

        protected void btnViewDeposit_Click(object sender, EventArgs e)
        {
            pnlTotal.Visible = false;
            pnlTransferList.Visible = true;
        }

        protected void btnTransferClose_Click(object sender, EventArgs e)
        {
            pnlTotal.Visible = true;
            pnlTransferList.Visible = false;
        }

        protected void rdpTransferDate_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            hfAdd.Value = "false";
            pnlTransferList.Visible = false;
            pnlAdd.Visible = true;
            hfWexTransferID.Value = rgWexTransfer.SelectedValue.ToString();
            FillTransferDetail();
        }
        private void FillTransferDetail()
        {
            string SQL;
            clsDBO.clsDBO clWT = new clsDBO.clsDBO();
            SQL = "select * from wextransfer where wextransferid = " + hfWexTransferID.Value;
            clWT.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clWT.RowCount() > 0)
            {
                clWT.GetRow();
                rdpTransferDate.SelectedDate = DateTime.Parse(clWT.GetFields("transferdate"));
                txtTransferAmt.Text = Convert.ToDouble(clWT.GetFields("amt")).ToString("#,##.00");
            }
        }
        protected void btnViewPaid_Click(object sender, EventArgs e)
        {
            pnlPaid.Visible = true;
        }

        protected void btnPaidClose_Click(object sender, EventArgs e)
        {
            pnlPaid.Visible = false;
        }

        protected void btnViewPending_Click(object sender, EventArgs e)
        {
            pnlPending.Visible = true;
        }

        protected void btnPendingClose_Click(object sender, EventArgs e)
        {
            pnlPending.Visible = false;
        }

        protected void btnViewVoid_Click(object sender, EventArgs e)
        {
            pnlVoid.Visible = true;
        }

        protected void btnVoidClose_Click(object sender, EventArgs e)
        {
            pnlVoid.Visible = false;
        }
    }
}