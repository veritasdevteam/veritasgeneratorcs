﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace VeritasGeneratorCS.Accounting
{
    public partial class CommissionCancel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            dsCommissionCancelType.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            SqlDataSource2.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            GetServerInfo();
            CheckToDo();
            if (!IsPostBack)
            {
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
            }
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO.clsDBO clTD = new clsDBO.clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnReports.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true)
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true)
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true)
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true)
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true)
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true)
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true)
                {
                    btnReports.Enabled = true;
                }
            }
        }
        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }
        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnCommission_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/Commission.aspx?sid=" + hfID.Value);
        }

        protected void btnPaylink_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/Paylink.aspx?sid=" + hfID.Value);
        }

        protected void btnAutoNationACH_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/autonationach.aspx?sid=" + hfID.Value);
        }

        protected void btnWex_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/wex.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountAdj_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/AccountAdjustments.aspx?sid=" + hfID.Value);
        }

        protected void btnCommissionCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/CommissionCancel.aspx?sid=" + hfID.Value);
        }

        protected void cboCommissionCancelType_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillCommission();
            FillPayment();
        }
        private void FillCommission()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (Convert.ToInt32(cboCommissionCancelType.SelectedValue) == 1)
            {
                SQL = "";
                SqlDataSource1.SelectCommand = SQL;
                rgCommission.Rebind();
            }
            if (Convert.ToInt32(cboCommissionCancelType.SelectedValue) == 2)
            {
                SQL = "select distinct c.contractid, c.ContractNo,d.DealerNo,d.DealerName,c.FName + ' '+c.LName as customerName, c.SaleDate,c.DatePaid, cc.CancelEffDate, cc.CancelDate, c.MoxyDealerCost,ca.CancelAmt * -1 as Amt,a.AgentName,a.Addr1,a.Addr2,a.City + ',' + a.State + ',' +a.Zip as addr3 " +
                      "from Veritas.dbo.Contract c " +
                      "left join Veritas.dbo.Dealer d on d.DealerID = c.DealerID " +
                      "left join Veritas.dbo.Agents a on a.agentID = c.agentsID " +
                      "left join veritas.dbo.contractamt ca on ca.ContractID = c.ContractID " +
                      "left join veritas.dbo.ContractCancel cc on cc.ContractID = c.ContractID " +
                      "where ca.rateTypeId = 3047 " +
                      "and c.contractno like '%an%' " +
                      "and c.status = 'Cancelled' " +
                      "and cc.CancelStatus = 'Cancelled' " +
                      "and not c.contractno in (select contractno from CommissionSheet where IsCancelled <> 0) " +
                      "and not c.ContractID in (select contractid from commissioncancel) " +
                      "and ca.CancelAmt <> 0 " +
                      "order by cc.CancelDate desc ";
                SqlDataSource1.SelectCommand = SQL;
                rgCommission.Rebind();
            }
            if (Convert.ToInt32(cboCommissionCancelType.SelectedValue) == 3)
            {
                SQL = "select distinct c.contractid, c.ContractNo,d.DealerNo,d.DealerName,c.FName + ' '+c.LName as customerName, c.SaleDate,c.DatePaid, cc.CancelEffDate, cc.CancelDate, c.MoxyDealerCost,ca.CancelAmt * -1 as Amt,a.AgentName,a.Addr1,a.Addr2,a.City + ',' + a.State + ',' +a.Zip as addr3 " +
                      "from Veritas.dbo.Contract c " +
                      "left join Veritas.dbo.Dealer d on d.DealerID = c.DealerID " +
                      "left join Veritas.dbo.Agents a on a.agentID = c.agentsID " +
                      "left join veritas.dbo.contractamt ca on ca.ContractID = c.ContractID " +
                      "left join veritas.dbo.ContractCancel cc on cc.ContractID = c.ContractID " +
                      "where ca.rateTypeId = 3047 " +
                      "and not c.contractno like '%an%' and not c.contractno like '%EP%' " +
                      "and c.status = 'Cancelled' " +
                      "and cc.CancelStatus = 'Cancelled' " +
                      "and not c.contractno in (select contractno from CommissionSheet where IsCancelled <> 0) " +
                      "and not c.ContractID in (select contractid from commissioncancel) " +
                      "and ca.CancelAmt <> 0 " +
                      "order by cc.CancelDate desc ";
                SqlDataSource1.SelectCommand = SQL;
                rgCommission.Rebind();
            }
            if (Convert.ToInt32(cboCommissionCancelType.SelectedValue) == 4)
            {
                SQL = "select distinct c.contractid, c.ContractNo,d.DealerNo,d.DealerName,c.FName + ' '+c.LName as customerName, c.SaleDate,c.DatePaid, cc.CancelEffDate, cc.CancelDate, c.MoxyDealerCost,ca.CancelAmt * -1 as Amt,a.AgentName,a.Addr1,a.Addr2,a.City + ',' + a.State + ',' +a.Zip as addr3 " +
                      "from Veritas.dbo.Contract c " +
                      "left join Veritas.dbo.Dealer d on d.DealerID = c.DealerID " +
                      "left join Veritas.dbo.Agents a on a.agentID = c.agentsID " +
                      "left join veritas.dbo.contractamt ca on ca.ContractID = c.ContractID " +
                      "left join veritas.dbo.ContractCancel cc on cc.ContractID = c.ContractID " +
                      "where c.AgentsID = 36 " +
                      "and rateTypeId = 13 " +
                      "and c.contractno like '%EP%' " +
                      "and c.status = 'Cancelled' " +
                      "and not c.contractno in (select contractno from CommissionSheet where IsCancelled <> 0) " +
                      "and not c.ContractID in (select contractid from commissioncancel) " +
                      "order by cc.CancelDate desc ";
                SqlDataSource1.SelectCommand = SQL;
                rgCommission.Rebind();
            }
            if (Convert.ToInt32(cboCommissionCancelType.SelectedValue) == 5) 
            {
                SQL = "select distinct c.contractid, c.ContractNo,d.DealerNo,d.DealerName,c.FName + ' '+c.LName as customerName, c.SaleDate,c.DatePaid, cc.CancelEffDate, cc.CancelDate, c.MoxyDealerCost,ca.CancelAmt * -1 as Amt,a.AgentName,a.Addr1,a.Addr2,a.City + ',' + a.State + ',' +a.Zip as addr3 " +
                      "from Veritas.dbo.Contract c " +
                      "left join Veritas.dbo.Dealer d on d.DealerID = c.DealerID " +
                      "left join Veritas.dbo.Agents a on a.agentID = c.agentsID " +
                      "left join veritas.dbo.contractamt ca on ca.ContractID = c.ContractID " +
                      "left join veritas.dbo.ContractCancel cc on cc.ContractID = c.ContractID " +
                      "where c.AgentsID = 36 " +
                      "and rateTypeId = 13 " +
                      "and not c.contractno like '%an%' and not c.contractno like '%EP%' " +
                      "and c.status = 'Cancelled' " +
                      "and not c.contractno in (select contractno from Veritas.dbo.CommissionSheet where IsCancelled <> 0) " +
                      "and not c.ContractID in (select contractid from Veritas.dbo.commissioncancel) " +
                      "order by cc.CancelDate desc ";
                SqlDataSource1.SelectCommand = SQL;
                rgCommission.Rebind();
            }
            if (Convert.ToInt32(cboCommissionCancelType.SelectedValue) == 6) 
            { 
                SQL = "select distinct c.contractid, c.ContractNo,d.DealerNo,d.DealerName,c.FName + ' '+c.LName as customerName, c.SaleDate,c.DatePaid, cc.CancelEffDate, cc.CancelDate, c.MoxyDealerCost,ca.CancelAmt * -1 as Amt,a.AgentName,a.Addr1,a.Addr2,a.City + ',' + a.State + ',' +a.Zip as addr3 " +
                      "from Veritas.dbo.Contract c " +
                      "left join Veritas.dbo.Dealer d on d.DealerID = c.DealerID " +
                      "left join Veritas.dbo.Agents a on a.agentID = c.agentsID " +
                      "left join veritas.dbo.contractamt ca on ca.ContractID = c.ContractID " +
                      "left join veritas.dbo.ContractCancel cc on cc.ContractID = c.ContractID " +
                      "where c.contractno like '%an%' " +
                      "and rateTypeId = 2907 " +
                      "and c.status = 'Cancelled' " +
                      "and not c.contractno in (select contractno from Veritas.dbo.CommissionSheet where IsCancelled <> 0) " +
                      "and not c.ContractID in (select contractid from Veritas.dbo.commissioncancel) " +
                      "order by cc.CancelDate desc ";
                SqlDataSource1.SelectCommand = SQL;
                rgCommission.Rebind();
            }
        }
        protected void rgCommission_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            FillCommission();
            FillPayment();
        }

        protected void btnMoveToPayment_Click(object sender, EventArgs e)
        {
            foreach (GridDataItem gdi in rgCommission.SelectedItems)
            {
                AddPayment(long.Parse(gdi.GetDataKeyValue("ContractID").ToString()));
            }
            FillCommission();
            FillPayment();
        }
        private void FillPayment()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select contractid, ContractNo,DealerNo,DealerName,customerName, SaleDate,DatePaid,CancelEffDate,CancelDate,MoxyDealerCost,Amt,AgentName,Addr1,Addr2,addr3 " +
                  "from Veritas.dbo.commissioncancel " +
                  "where commissioncanceltypeid = " + cboCommissionCancelType.SelectedValue;
            SqlDataSource2.SelectCommand = SQL;
            rgPayment.Rebind();
        }
        private void AddPayment(long xID)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clP = new clsDBO.clsDBO();
            SQL = "select distinct c.contractid, c.ContractNo,d.DealerNo,d.DealerName,c.FName + ' '+c.LName as customerName, c.SaleDate,c.DatePaid, cc.CancelEffDate, cc.CancelDate, c.MoxyDealerCost,ca.CancelAmt * -1 as Amt,a.AgentName,a.Addr1,a.Addr2,a.City + ',' + a.State + ',' +a.Zip as addr3 " +
                  "from Veritas.dbo.Contract c " +
                  "left join Veritas.dbo.Dealer d on d.DealerID = c.DealerID " +
                  "left join Veritas.dbo.Agents a on a.agentID = c.agentsID " +
                  "left join veritas.dbo.contractamt ca on ca.ContractID = c.ContractID " +
                  "left join veritas.dbo.ContractCancel cc on cc.ContractID = c.ContractID " + 
                  "where c.contractid = " + xID;
            if (Convert.ToInt32(cboCommissionCancelType.SelectedValue) == 2) 
            {
                SQL = SQL + "and ca.rateTypeId = 3047 ";
            }
            if (Convert.ToInt32(cboCommissionCancelType.SelectedValue) == 3) 
            {
                SQL = SQL + "and ca.rateTypeId = 3047 ";
            }
            if (Convert.ToInt32(cboCommissionCancelType.SelectedValue) == 4) 
            {
                SQL = SQL + "and rateTypeId = 13 ";
            }
            if (Convert.ToInt32(cboCommissionCancelType.SelectedValue) == 5) 
            {
                SQL = SQL + "and rateTypeId = 13 ";
            }
            if (Convert.ToInt32(cboCommissionCancelType.SelectedValue) == 6) 
            {
                SQL = SQL + "and rateTypeId = 2907 ";
            }
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0) 
            {
                clR.GetRow();
                SQL = "select * from commissioncancel " +
                      "where commissioncanceltypeid = " + cboCommissionCancelType.SelectedValue + " " +
                      "and contractid = " + xID;
                clP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clP.RowCount() > 0) 
                {
                    clP.GetRow();
                } 
                else 
                {
                    clP.NewRow();
                    clP.SetFields("commissioncanceltypeid", cboCommissionCancelType.SelectedValue);
                    clP.SetFields("contractid", xID.ToString());
                }
                clP.SetFields("contractno", clR.GetFields("contractno"));
                clP.SetFields("dealerno", clR.GetFields("dealerno"));
                clP.SetFields("dealername", clR.GetFields("dealername"));
                clP.SetFields("customername", clR.GetFields("customername"));
                clP.SetFields("saledate", clR.GetFields("saledate"));
                clP.SetFields("datepaid", clR.GetFields("datepaid"));
                clP.SetFields("canceleffdate", clR.GetFields("canceleffdate"));
                clP.SetFields("canceldate", clR.GetFields("canceldate"));
                clP.SetFields("moxydealercost", clR.GetFields("moxydealercost"));
                clP.SetFields("amt", clR.GetFields("amt"));
                clP.SetFields("agentname", clR.GetFields("agentname"));
                clP.SetFields("addr1", clR.GetFields("addr1"));
                clP.SetFields("addr2", clR.GetFields("addr2"));
                clP.SetFields("addr3", clR.GetFields("addr3"));
                if (clP.RowCount() == 0)
                {
                    clP.AddRow();
                }
                clP.SaveDB();
            }
        }
        protected void rgPayment_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToCsv")
            {
                rgPayment.ExportSettings.ExportOnlyData = false;
                rgPayment.ExportSettings.IgnorePaging = true;
                rgPayment.ExportSettings.OpenInNewWindow = true;
                rgPayment.ExportSettings.UseItemStyles = true;
                rgPayment.ExportSettings.FileName = "CommissionCancel";
                rgPayment.ExportSettings.Csv.FileExtension = "csv";
                rgPayment.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Comma;
                rgPayment.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
                rgPayment.MasterTableView.ExportToCSV();
            }
            if (e.CommandName == "DeleteRow")
            {
                DeletePayment(long.Parse(rgPayment.Items[e.Item.ItemIndex].GetDataKeyValue("ContractID").ToString()));
                FillCommission();
                FillPayment();
            }
        }
        private void DeletePayment(long xID)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "delete commissioncancel " +
                  "where contractid = " + xID + " " +
                  "and commissioncanceltypeid = " + cboCommissionCancelType.SelectedValue;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
    }
}