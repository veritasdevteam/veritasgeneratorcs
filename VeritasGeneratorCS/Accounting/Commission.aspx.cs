﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace VeritasGeneratorCS.Accounting
{
    public partial class Commission : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsAgent.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            tbTodo.Width = pnlHeader.Width;
            if (!IsPostBack)
            {
                GetServerInfo();
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
                CheckToDo();
                pnlAgentSearch.Visible = false;
                FillCycleMonth();
            }
        }
        private void FillCycleMonth()
        {
            string SQL;
            clsDBO.clsDBO clCM = new clsDBO.clsDBO();
            SQL = "select cast(month(finaldate) as nvarchar(2)) + '/' + cast(day(finaldate) as nvarchar(2)) + '/' + cast(year(finaldate) as nvarchar(4))as FinalDateout from contractcommissions " +
                  "group by finaldate " +
                  "order by finaldate desc ";
            cboCycleMonth.DataSource = clCM.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            cboCycleMonth.DataTextField = "FinalDateOut";
            cboCycleMonth.DataValueField = "FinalDateOut";
            cboCycleMonth.DataBind();
            cboCycleMonth.Items.Insert(0, "");
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO.clsDBO clTD = new clsDBO.clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnReports.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true)
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true)
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true)
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true)
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true)
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true)
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true)
                {
                    btnReports.Enabled = true;
                }
            }
        }
        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }
        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnCommission_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/Commission.aspx?sid=" + hfID.Value);
        }

        protected void btnPaylink_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/Paylink.aspx?sid=" + hfID.Value);
        }

        protected void btnAutoNationACH_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/autonationach.aspx?sid=" + hfID.Value);
        }

        protected void btnWex_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/wex.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountAdj_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/AccountAdjustments.aspx?sid=" + hfID.Value);
        }

        protected void btnCommissionCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/CommissionCancel.aspx?sid=" + hfID.Value);
        }

        protected void btnAgentSearch_Click(object sender, EventArgs e)
        {
            pnlAgentSearch.Visible = true;
            pnlCommission.Visible = false;
        }

        protected void btnAgentClose_Click(object sender, EventArgs e)
        {
            pnlAgentSearch.Visible = false;
            pnlCommission.Visible = true;
        }

        protected void rgAgent_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlAgentSearch.Visible = false;
            pnlCommission.Visible = true;
            hfAgentID.Value = rgAgent.SelectedValue.ToString();
            txtAgent.Text = Functions.GetAgentInfo(long.Parse(rgAgent.SelectedValue.ToString()));
        }

        protected void btnGetCommission_Click(object sender, EventArgs e)
        {
            FillGrid();
        }
        private void FillGrid()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();

            if (!chkCancel.Checked)
            {
                SQL = "select cc.prefinal, cc.contractcommissionid, c.contractno, a.agentno, a.agentname, rt.ratetypename, cc.amt  " +
                      "from contractcommissions cc " +
                      "inner join contract c on c.contractid = cc.contractid " +
                      "inner join agents a on a.agentid = cc.agentid " +
                      "inner join RateType rt on cc.RateTypeID = rt.RateTypeID " +
                      "where a.agentid = " + hfAgentID.Value + " " +
                      "and c.status = 'Paid' ";
                if (cboCycleMonth.Text.Length > 0)
                {
                    SQL = SQL + "and finaldate = '" + cboCycleMonth.Text + "' ";
                }
                else
                {
                    SQL = SQL + "and finaldate is null ";
                }
            }
            else
            {
                SQL = "select cc.prefinal, cc.contractcommissionid, c.contractno, a.agentno, a.agentname, rt.ratetypename, cc.cancelamt  " +
                      "from contractcommissions cc " +
                      "inner join contract c on c.contractid = cc.contractid " +
                      "inner join agents a on a.agentid = cc.agentid " +
                      "inner join RateType rt on cc.RateTypeID = rt.RateTypeID " +
                      "where a.agentid = " + hfAgentID.Value + " " +
                      "and not finaldate is null " +
                      "and cancelfinaldate is null " +
                      "and c.status = 'Cancelled' ";
                if (cboCycleMonth.Text.Length > 0)
                {
                    SQL = SQL + "and finaldate = '" + cboCycleMonth.Text + "' ";
                }
            }
            rgCommission.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            rgCommission.DataBind();
        }
        protected void rgCommission_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnAddCycle_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clCC = new clsDBO.clsDBO();
            clsDBO.clsDBO clCC2 = new clsDBO.clsDBO();
            long cnt;
            if (rdpCycleDate.SelectedDate == null)
            {
                return;
            }
            SQL = "select * from contractcommissions " +
                  "where agentid = " + hfAgentID.Value + " " +
                  "and prefinal = 1 " +
                  "and finaldate is null ";
            clCC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCC.RowCount() > 0)
            {
                for (cnt = 0; cnt <= clCC.RowCount() - 1; cnt++)
                {
                    clCC.GetRowNo((int)cnt);
                    SQL = "select * from contractcommissions where contractcommissionid = " + clCC.GetFields("contractcommissionid");
                    clCC2.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clCC2.RowCount() > 0)
                    {
                        clCC2.GetRow();
                        clCC2.SetFields("finaldate", rdpCycleDate.SelectedDate.ToString());
                        clCC2.SaveDB();
                    }
                }
            }
            SQL = "select * from contractcommissions " +
                  "where agentid = " + hfAgentID.Value + " " +
                  "and prefinalcancel = 1 " +
                  "and cancelfinaldate is null ";
            clCC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCC.RowCount() > 0)
            {
                for (cnt = 0; cnt <= clCC.RowCount() - 1; cnt++)
                {
                    clCC.GetRowNo((int)cnt);
                    SQL = "select * from contractcommissions where contractcommissionid = " + clCC.GetFields("contractcommissionid");
                    clCC2.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clCC2.RowCount() > 0)
                    {
                        clCC2.GetRow();
                        clCC2.SetFields("cancelfinaldate", rdpCycleDate.SelectedDate.ToString());
                        clCC2.SaveDB();
                    }
                }
            }
        }

        protected void rgCommission_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            GridEditableItem item = (GridEditableItem)e.Item;
            if (e.CommandName == "Remove")
            {
                SQL = "update contractcommissions ";
                if (chkCancel.Checked)
                {
                    SQL = SQL + "set cancelfinaldate = null ";
                }
                else
                {
                    SQL = SQL + "set finaldate = null ";
                }
                SQL = SQL + "where contractcommissionid = " + item["ContractCommissionID"].Text;
                clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                rgCommission.Rebind();
                FillGrid();
            }
        }
    }
}