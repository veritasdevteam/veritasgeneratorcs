﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Accounting
{
    public partial class AccountAdjustments : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            dsAdj.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsRateType.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            if (!IsPostBack)
            {
                GetServerInfo();
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
                CheckToDo();
                pnlDetail.Visible = false;
                pnlSearch.Visible = false;
                rgAdj.Rebind();
            }
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO.clsDBO clTD = new clsDBO.clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnReports.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true)
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true)
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true)
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true)
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true)
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true)
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true)
                {
                    btnReports.Enabled = true;
                }
            }
        }
        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnCommission_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/Commission.aspx?sid=" + hfID.Value);
        }

        protected void btnPaylink_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/Paylink.aspx?sid=" + hfID.Value);
        }

        protected void btnAutoNationACH_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/autonationach.aspx?sid=" + hfID.Value);
        }

        protected void btnWex_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/wex.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountAdj_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/AccountAdjustments.aspx?sid=" + hfID.Value);
        }

        protected void btnCommissionCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/CommissionCancel.aspx?sid=" + hfID.Value);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            pnlList.Visible = false;
            pnlDetail.Visible = true;
            txtAmt.Text = "0";
            rdpAdjDate.Clear();
            txtDesc.Text = "";
            txtRateTypeDesc.Text = "";
            hfAAID.Value = "0";
        }

        protected void rgAdj_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlList.Visible = false;
            pnlDetail.Visible = true;
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            hfAAID.Value = rgAdj.SelectedValue.ToString();
            SQL = "select * from accountadjustments where idx = " + rgAdj.SelectedValue;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfRateTypeID.Value = clR.GetFields("ratetypeid");
                rdpAdjDate.SelectedDate = DateTime.Parse(clR.GetFields("adjdate"));
                txtAmt.Text = clR.GetFields("amount");
                txtDesc.Text = clR.GetFields("description");
                GetRateDesc();
            }
        }
        private void GetRateDesc()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from ratetype where ratetypeid = " + hfRateTypeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtRateTypeDesc.Text = clR.GetFields("ratetypename");
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfAAID.Value.Length == 0)
            {
                hfAAID.Value = "0";
            }
            SQL = "select * from accountadjustments where idx = " + hfAAID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                clR.SetFields("moddate", DateTime.Today.ToString());
                clR.SetFields("modby", hfUserID.Value);
            }
            else
            {
                clR.NewRow();
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", hfUserID.Value);
            }
            if (rdpAdjDate.SelectedDate != null)
            {
                clR.SetFields("adjdate", rdpAdjDate.SelectedDate.ToString());
            }
            clR.SetFields("amount", txtAmt.Text);
            clR.SetFields("ratetypeid", hfRateTypeID.Value);
            clR.SetFields("description", txtDesc.Text);
            if (clR.RowCount() == 0)
            {
                clR.AddRow();
            }
            clR.SaveDB();
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgAdj.Rebind();
        }

        protected void btnSearchRateType_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlSearch.Visible = true;
            rgRateType.Rebind();
        }

        protected void rgRateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlSearch.Visible = false;
            pnlDetail.Visible = true;
            hfRateTypeID.Value = rgRateType.SelectedValue.ToString();
            GetRateDesc();
        }
    }
}