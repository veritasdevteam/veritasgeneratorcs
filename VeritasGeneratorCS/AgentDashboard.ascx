﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="AgentDashboard.ascx.cs" Inherits="VeritasGeneratorCS.AgentDashboard" %>
        <asp:Table runat="server">
            <asp:TableRow ID="trDashboard">
                <asp:TableCell>
                    <telerik:RadHtmlChart ID="htmlInbound" runat="server" Width="400" Height="400" Transitions="false">
                        <PlotArea>
                        <Series>
                            <telerik:VerticalBulletSeries DataCurrentField="count" DataTargetField="goal">
                            </telerik:VerticalBulletSeries>
                        </Series>
                        <XAxis DataLabelsField="CallDate">
                            <MinorGridLines Visible="false"></MinorGridLines>
                            <MajorGridLines Visible="false"></MajorGridLines>
                        </XAxis>
                        <YAxis>
                            <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                            <MinorGridLines Visible="false"></MinorGridLines>
                        </YAxis>
                    </PlotArea>
                    <Legend>
                        <Appearance Visible="false"></Appearance>
                    </Legend>
                    <ChartTitle Text="Stats"></ChartTitle>
                    </telerik:RadHtmlChart>
                </asp:TableCell>
                <asp:TableCell>
                    <telerik:RadHtmlChart ID="htmlOutBound" runat="server" Width="400" Height="400" Transitions="false">
                        <PlotArea>
                        <Series>
                            <telerik:VerticalBulletSeries DataCurrentField="count" DataTargetField="goal">
                            </telerik:VerticalBulletSeries>
                        </Series>                        <XAxis DataLabelsField="CallDate">
                            <MinorGridLines Visible="false"></MinorGridLines>
                            <MajorGridLines Visible="false"></MajorGridLines>
                        </XAxis>
                        <YAxis>
                            <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                            <MinorGridLines Visible="false"></MinorGridLines>
                        </YAxis>
                    </PlotArea>
                    <Legend>
                        <Appearance Visible="false"></Appearance>
                    </Legend>
                    <ChartTitle Text="Stats"></ChartTitle>
                    </telerik:RadHtmlChart>
                </asp:TableCell>
                <asp:TableCell>
                        <telerik:RadHtmlChart ID="htmlDnD" runat="server" Width="400" Height="400" Transitions="false">
                        <PlotArea>
                        <Series>
                            <telerik:VerticalBulletSeries DataCurrentField="count" DataTargetField="goal">
                            </telerik:VerticalBulletSeries>
                        </Series>                        <XAxis DataLabelsField="CallDate">
                            <MinorGridLines Visible="false"></MinorGridLines>
                            <MajorGridLines Visible="false"></MajorGridLines>
                        </XAxis>
                        <YAxis>
                            <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                            <MinorGridLines Visible="false"></MinorGridLines>
                        </YAxis>
                    </PlotArea>
                    <Legend>
                        <Appearance Visible="false"></Appearance>
                    </Legend>
                    <ChartTitle Text="Stats"></ChartTitle>
                    </telerik:RadHtmlChart>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="trOpen">
                <asp:TableCell>
                    <telerik:RadGrid ID="rgClaimOpen" OnSelectedIndexChanged="rgClaimOpen_SelectedIndexChanged" OnNeedDataSource="rgClaimOpen_NeedDataSource" runat="server" AutoGenerateColumns ="true" AllowFilteringByColumn="false" AllowSorting="true" ShowFooter ="true" ShowHeader="true" AllowPaging="true" PagerStyle-HorizontalAlign="Left" PagerStyle-Position="bottom">
                        <PagerStyle EnableAllOptionInPagerComboBox="true" />
                            <MasterTableView AutoGenerateColumns ="false" PageSize="6" ShowFooter="true" DataKeyNames="ClaimID">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="ClaimID" ReadOnly="true" Visible="false" UniqueName="ClaimID"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ClaimNo" UniqueName="ClaimNo" HeaderText="Claim No"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CreDate" UniqueName="CreDate" HeaderText="Loss date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ModDate" UniqueName="ModDate" HeaderText="Last Activity" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ClaimAge" UniqueName="ClaimAge" HeaderText="Claim Age"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ActivityDesc" UniqueName="ActivityDesc" HeaderText="Activity"></telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                        <ClientSettings EnablePostBackOnRowClick="true">
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="trBlank">
                <asp:TableCell>
                    &nbsp
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>

        <asp:HiddenField ID="hfUserID" runat="server" />
        <asp:HiddenField ID="hfID" runat="server" />
