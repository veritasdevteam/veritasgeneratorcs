﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimGAPNote : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsNote.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsNoteType.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            hfClaimID.Value = Request.QueryString["claimid"];
            if (!IsPostBack)
            {
                GetServerInfo();
                pnlDetail.Visible = false;
                pnlList.Visible = true;
                rgNote.Rebind();
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void btnAddNote_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            txtNote.Content = "";
            txtCreBy.Text = "";
            txtCreDate.Text = "";
            hfClaimNoteID.Value = "0";
        }

        protected void rgNote_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            hfClaimNoteID.Value = rgNote.SelectedValue.ToString();
            SQL = "select * from claimgapnote cn " +
                  "inner join userinfo ui on ui.userid = cn.creby " +
                  "where claimnoteid = " + hfClaimNoteID.Value + " ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                cboNoteType.SelectedValue = clR.GetFields("claimgapnotetypeid");
                txtNote.Content = clR.GetFields("note");
                txtCreBy.Text = clR.GetFields("username");
                txtCreDate.Text = clR.GetFields("credate");
                btnSave.Visible = true;
                if (hfUserID.Value != clR.GetFields("creby"))
                {
                    btnSave.Visible = false;
                }
                if ((DateTime.Now-DateTime.Parse(txtCreDate.Text)).TotalHours > 24)
                {
                    btnSave.Visible = false;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfClaimNoteID.Value.Length == 0) 
            {
                hfClaimNoteID.Value = "0";
            }
            SQL = "select * from claimgapnote cn where claimnoteid = " + hfClaimNoteID.Value + " ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0) 
            {
                clR.GetRow();
            } 
            else 
            {
                clR.NewRow();
            }
            clR.SetFields("claimgapid", hfClaimID.Value);
            clR.SetFields("claimgapnotetypeid", cboNoteType.SelectedValue);
            string sTemp;
            sTemp = txtNote.Text;
            clR.SetFields("note", txtNote.Content);
            clR.SetFields("notetext", sTemp);
            clR.SetFields("moddate", DateTime.Now.ToString());
            clR.SetFields("modby", hfUserID.Value);
            if (clR.RowCount() == 0) 
            {
                clR.SetFields("credate", DateTime.Now.ToString());
                clR.SetFields("creby", hfUserID.Value);
                clR.AddRow();
            }
            clR.SaveDB();
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgNote.Rebind();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgNote.SelectedIndexes.Clear();
        }

    }
}