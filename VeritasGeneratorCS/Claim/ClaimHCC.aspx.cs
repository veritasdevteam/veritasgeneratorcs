﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimHCC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["claimid"];
            if (!IsPostBack)
            {
                GetServerInfo();
                trCEOApp1.Visible = false;
                trManagerApp1.Visible = false;
                trManagerApp2.Visible = false;
                trCEOApp2.Visible = false;
                trInsurerApproval.Visible = false;
                FillHCC();
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private void FillHCC()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimhcc where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtAdjuster.Text = CalcUserName(long.Parse(clR.GetFields("adjusterid")));
                if (clR.GetFields("datesubmitted").Length > 0)
                {
                    txtDateSubmitted.Text = DateTime.Parse(clR.GetFields("datesubmitted")).ToString("M/d/yyyy");
                } 
                else
                {
                    txtDateSubmitted.Text = "";
                }
                txtDaysIntoContract.Text = clR.GetFields("DaysIntoContract");
                txtMilesIntoContract.Text = clR.GetFields("MilesIntoContract");
                txtLimitofLiability.Text = clR.GetFields("limitofliability");
                if (clR.GetFields("nadavalue").Length > 0)
                {
                    txtNADA.Text = Convert.ToDouble(clR.GetFields("nadavalue")).ToString("#,##0.00");
                } 
                else 
                {
                    txtNADA.Text = "";
                }
                txtDescFailure.Text = clR.GetFields("descfailure");
                if (clR.GetFields("claimcost").Length > 0) 
                {
                    txtClaimCost.Text = Convert.ToDouble(clR.GetFields("claimcost")).ToString("#,##0.00");
                } 
                else 
                {
                    txtClaimCost.Text = "";
                }
                if (clR.GetFields("laborcostperhour").Length > 0) 
                {
                    txtLaborCostPerHour.Text = Convert.ToDouble(clR.GetFields("laborcostperhour")).ToString("#,##0.00");
                } 
                else
                {
                    txtLaborCostPerHour.Text = "";
                }
                if (clR.GetFields("averagecost").Length > 0) 
                {
                    txtAverageCost.Text = Convert.ToDouble(clR.GetFields("averagecost")).ToString("#,##0.00");
                } 
                else 
                {
                    txtAverageCost.Text = "";
                }
                txtRecommendCorrect.Text = clR.GetFields("recommendcorrect");
                txtApprovedCorrect.Text = clR.GetFields("approvedcorrect");
                if (clR.GetFields("originalroamount").Length > 0)
                {
                    txtOriginalROAmount.Text = Convert.ToDouble(clR.GetFields("originalroamount")).ToString("#,##0.00");
                } 
                else 
                {
                    txtOriginalROAmount.Text = "";
                }
                if (clR.GetFields("mceapproveamount").Length > 0) 
                {
                    txtMCEApproveAmount.Text = Convert.ToDouble(clR.GetFields("MCEApproveAmount")).ToString("#,##0.00");
                } 
                else 
                {
                    txtMCEApproveAmount.Text = "";
                }
                chkInspectionDone.Checked = Convert.ToBoolean(clR.GetFields("inspectdone"));
                chkTeardownDone.Checked = Convert.ToBoolean(clR.GetFields("teardowndone"));
                chkRecordsRequestd.Checked = Convert.ToBoolean(clR.GetFields("recordsrequested"));
                chkCustomerStatementGiven.Checked = Convert.ToBoolean(clR.GetFields("customerstatementgiven"));
                chkPartsLaborMCE.Checked = Convert.ToBoolean(clR.GetFields("partslabormce"));
                chkInsurerApproval.Checked = Convert.ToBoolean(clR.GetFields("insurerapproval"));
                if (chkInsurerApproval.Checked) 
                {
                    trInsurerApproval.Visible = true;
                } 
                else 
                {
                    trInsurerApproval.Visible = false;
                }
                txtInsurerApproval.Content = clR.GetFields("emailsent");
                txtExplainClaim.Text = clR.GetFields("explainclaim");
                if (clR.GetFields("managerapprovalid").Length > 0) 
                {
                    txtManagerApprovalPrint.Text = CalcUserName(long.Parse(clR.GetFields("managerapprovalid")));
                    txtManagerSign.Text = CalcUserName(long.Parse(clR.GetFields("managerapprovalid")));
                    trManagerApp1.Visible = true;
                    trManagerApp2.Visible = true;
                    btnManagerApproval.Enabled = false;
                } 
                else 
                {
                    txtManagerSign.Text = "";
                    txtManagerApprovalPrint.Text = "";
                    btnManagerApproval.Enabled = true;
                    trManagerApp1.Visible = false;
                    trManagerApp2.Visible = false;
                }
                if (clR.GetFields("managerapprovaldate").Length > 0) 
                {
                    txtManagerApprovalDate.Text = DateTime.Parse(clR.GetFields("managerapprovaldate")).ToString("M/d/yyyy");
                }
                if (clR.GetFields("ceoapprovalid").Length > 0) 
                {
                    txtCEOApprovalPrint.Text = CalcUserName(long.Parse(clR.GetFields("ceoapprovalid")));
                    txtCEOApprovalSign.Text = CalcUserName(long.Parse(clR.GetFields("ceoapprovalid")));
                    trCEOApp1.Visible = true;
                    trCEOApp2.Visible = true;
                    btnCEOApproval.Enabled = false;
                } 
                else 
                {
                    txtCEOApprovalSign.Text = "";
                    txtCEOApprovalPrint.Text = "";
                    btnCEOApproval.Enabled = true;
                    trCEOApp1.Visible = false;
                    trCEOApp2.Visible = false;
                }
                if (clR.GetFields("ceoapprovaldate").Length > 0) 
                {
                    txtCEOApprovalDate.Text = DateTime.Parse(clR.GetFields("ceoapprovaldate")).ToString("M/d/yyyy");
                }
            }
            hlWorksheet.NavigateUrl = "hcc.aspx?claimid=" + hfClaimID.Value;
        }

        protected void chkInsurerApproval_CheckedChanged(object sender, EventArgs e)
        {
            if (chkInsurerApproval.Checked)
            {
                trInsurerApproval.Visible = true;
            } 
            else 
            {
                trInsurerApproval.Visible = false;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (txtClaimCost.Text.Length == 0)
            {
                lblError.Text = "Claim Cost needs to be entered.";
                lblError.Visible = true;
                return;
            } 
            else 
            {
                lblError.Visible = false;
            }
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimHCC where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                clR.SetFields("daysintocontract", txtDaysIntoContract.Text);
                clR.SetFields("milesintocontract", txtMilesIntoContract.Text);
                clR.SetFields("limitofliability", txtLimitofLiability.Text);
                clR.SetFields("descfailure", txtDescFailure.Text);
                clR.SetFields("claimcost", txtClaimCost.Text);
                clR.SetFields("laborcostperhour", txtLaborCostPerHour.Text);
                clR.SetFields("averagecost", txtAverageCost.Text);
                clR.SetFields("recommendcorrect", txtRecommendCorrect.Text);
                clR.SetFields("approvedcorrect", txtApprovedCorrect.Text);
                clR.SetFields("originalroamount", txtOriginalROAmount.Text);
                clR.SetFields("MCEApproveAmount", txtMCEApproveAmount.Text);
                clR.SetFields("InspectDone", chkInspectionDone.Checked.ToString());
                clR.SetFields("TearDownDone", chkTeardownDone.Checked.ToString());
                clR.SetFields("RecordsRequested", chkRecordsRequestd.Checked.ToString());
                clR.SetFields("CustomerStatementGiven", chkCustomerStatementGiven.Checked.ToString());
                clR.SetFields("PartsLaborMCE", chkPartsLaborMCE.Checked.ToString());
                clR.SetFields("InsurerApproval", chkInsurerApproval.Checked.ToString());
                clR.SetFields("ExplainClaim", txtExplainClaim.Text);
                clR.SetFields("EMailSent", txtInsurerApproval.Content);
                clR.SetFields("NADAValue", txtNADA.Text);
                clR.SaveDB();
            }
        }

        protected void btnManagerApproval_Click(object sender, EventArgs e)
        {
            if (txtClaimCost.Text.Length == 0)
            {
                lblError.Text = "Claim Cost needs to be entered before approval.";
                lblError.Visible = true;
                return;
            } else {
                lblError.Visible = false;
            }
            if (!CheckManagerApproval())
            {
                lblError.Text = "You are not authorized to approve claim";
                lblError.Visible = true;
                return;
            } 
            else 
            {
                lblError.Visible = false;
            }
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update claimhcc " +
                  "set managerapprovalid = " + hfUserID.Value + ", " +
                  "managerapprovaldate = '" + DateTime.Today + "' " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            SQL = "select * from claimhcc where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("managerapprovalid").Length > 0)
                {
                    txtManagerApprovalPrint.Text = CalcUserName(long.Parse(clR.GetFields("managerapprovalid")));
                    txtManagerSign.Text = CalcUserName(long.Parse(clR.GetFields("managerapprovalid")));
                    trManagerApp1.Visible = true;
                    trManagerApp2.Visible = true;
                    btnManagerApproval.Enabled = false;
                } else {
                    txtManagerSign.Text = "";
                    txtManagerApprovalPrint.Text = "";
                    btnManagerApproval.Enabled = true;
                    trManagerApp1.Visible = false;
                    trManagerApp2.Visible = false;
                }
                if (clR.GetFields("managerapprovaldate").Length > 0)
                {
                    txtManagerApprovalDate.Text = DateTime.Parse(clR.GetFields("managerapprovaldate")).ToString("M/d/yyyy");
                }
            }
        }

        protected void btnCEOApproval_Click(object sender, EventArgs e)
        {
            if (txtClaimCost.Text.Length == 0)
            {
                lblError.Text = "Claim Cost needs to be entered before approval.";
                lblError.Visible = true;
                return;
            }
            else
            {
                lblError.Visible = false;
            }
            if (!CheckCEOApproval())
            {
                lblError.Text = "You are not authorized to approve claim";
                lblError.Visible = true;
                return;
            } 
            else 
            {
                lblError.Visible = false;
            }
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update claimhcc " +
                  "set ceoapprovalid = " + hfUserID.Value + ", " +
                  "ceoapprovaldate = '" + DateTime.Today + "' " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            SQL = "select * from claimhcc where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("ceoapprovalid").Length > 0)
                {
                    txtCEOApprovalPrint.Text = CalcUserName(long.Parse(clR.GetFields("ceoapprovalid")));
                    txtCEOApprovalSign.Text = CalcUserName(long.Parse(clR.GetFields("ceoapprovalid")));
                    trCEOApp1.Visible = true;
                    trCEOApp2.Visible = true;
                    btnCEOApproval.Enabled = false;
                } 
                else 
                {
                    txtCEOApprovalSign.Text = "";
                    txtCEOApprovalPrint.Text = "";
                    btnCEOApproval.Enabled = true;
                    trCEOApp1.Visible = false;
                    trCEOApp2.Visible = false;
                }
                if (clR.GetFields("ceoapprovaldate").Length > 0)
                {
                    txtCEOApprovalDate.Text = DateTime.Parse(clR.GetFields("ceoapprovaldate")).ToString("M/d/yyyy");
                }
            }
        }
        private bool CheckCEOApproval()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + hfUserID.Value + " " +
                  "and hccapprovalceo <> 0 " +
                  "and claimauth > " + Convert.ToDouble(txtClaimCost.Text);
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                return true;
            }
            return false;
        }
        private bool CheckManagerApproval()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + hfUserID.Value + " " +
                  "and hccapprovalmanager <> 0 " +
                  "and claimauth > " + Convert.ToDouble(txtClaimCost.Text);
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                return true;
            }
            return false;
        }
        private string CalcUserName(long xUserID)
        {
            string SQL;
            clsDBO.clsDBO clU = new clsDBO.clsDBO();
            SQL = "select * from userinfo where userid = " + xUserID;
            clU.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clU.RowCount() > 0)
            {
                clU.GetRow();
                return clU.GetFields("fname") + " " + clU.GetFields("lname");
            }
            return "";
        }
    }
}