﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimTeamOpen : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetServerInfo();
                CheckToDo();
                FillScreen();
                btnRFClaimSubmit.Enabled = false;
                CheckRFClaimSubmit();
                CheckSecurity();

                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
            }
        }
        private void CheckSecurity()
        {
            string SQL;
            clsDBO.clsDBO clUSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clUSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clUSI.RowCount() > 0)
            {
                clUSI.GetRow();
                if (Convert.ToBoolean(clUSI.GetFields("AllowClaimAudit")))
                {
                    btnClaimAudit.Enabled = true;
                } 
                else 
                {
                    btnClaimAudit.Enabled = false;
                }
            }
        }
        private void CheckRFClaimSubmit()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + hfUserID.Value + " " +
                  "and teamlead <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                SQL = "select * from veritasclaims.dbo.claim " +
                      "where not sendclaim is null " +
                      "and processclaimdate is null ";
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() > 0)
                {
                    btnRFClaimSubmit.Enabled = true;
                }
            }
        }
        private void FillScreen()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clP = new clsDBO.clsDBO();
            string lTeamID = "0";
            SQL = "select * from userinfo ui inner join usersecurityinfo usi on ui.UserID = usi.Userid where ui.userid = " + hfUserID.Value;
            clP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clP.RowCount() > 0)
            {
                clP.GetRow();
                lTeamID = clP.GetFields("teamid");
            }
            SQL = "select cl.ClaimID, d.dealerno, DealerName, contractno, ui.username, c.FName, c.lname, vin, claimno, cl.CreDate as Reportdate, " +
                  "cl.Moddate as lastmaint, ca.ActivityDesc, vcdsta.totalamt as AmtDue, a.agentname, sa.subagentname, " +
                  "case when vcd.cnt is null then 0 else vcd.cnt end as ClaimAge, " +
                  "case when vna.cnt is null then 0 else vna.cnt end as noactivity, " +
                  "sc.ServiceCenterName, cl.OpenDate, " +
                  "case when DC.cnt is null then 0 else dc.cnt end as opendays, " +
                  "max(ci.RequestDate) as InspectionRequest, " +
                  "max(um.MessageDate) as InspectionComplete, " +
                  "max(case when ci.RequestDate is null then 0 " +
                  "else case when um.MessageDate is null then DATEDIFF(day, ci.requestdate, getdate()) else datediff(day, ci.requestdate, um.messagedate) end " +
                  "end) as InspectionDays " +
                  "from claim cl inner join contract c on c.contractid = cl.ContractID " +
                  "inner join dealer d on c.DealerID = d.DealerID " +
                  "left join agents a on d.agentsid = a.agentid " +
                  "left join SubAgents sa on sa.SubAgentID = d.SubAgentID " +
                  "left join vwClaimAge vcd on cl.ClaimID = vcd.claimid " +
                  "left join vwNoActivity vna on cl.claimid = vna.claimid " +
                  "left join vwclaimopen vco on cl.claimid = vco.claimid " +
                  "left join ClaimActivity ca on ca.ClaimActivityID = cl.ClaimActivityID " +
                  "left join vwClaimDetailSumTotalAmt as VCDSTA on vcdsta.claimid = cl.claimid " +
                  "left join ServiceCenter sc on cl.ServiceCenterID = sc.ServiceCenterID " +
                  "left join ClaimInspection ci on ci.ClaimID = cl.ClaimID and not InspectionID is null " +
                  "left join UserMessage um on ci.InspectionID = um.InspectionID " +
                  "left join userinfo ui on ui.userid = cl.assignedto " +
                  "left join(select claimid, sum(case when CloseDate is null then DATEDIFF(day, OpenDate, getdate()) else DATEDIFF(day, OpenDate, CloseDate) end) as cnt from ClaimOpenHistory " +
                  "group by claimid) DC on cl.claimid = dc.claimid " +
                  "where cl.status = 'Open' " +
                  "and AssignedTo in (select ui.userid from userinfo ui inner join usersecurityinfo usi on ui.UserID = usi.Userid where TeamID = "
                  + lTeamID + ") " +
                  "group by cl.ClaimID, d.dealerno, DealerName, contractno, ui.username, c.FName, c.lname, vin, claimno, cl.CreDate, " +
                  "cl.Moddate, ca.ActivityDesc, vcdsta.totalamt, a.agentname, sa.subagentname, " +
                  "case when DC.cnt is null then 0 else dc.cnt end, " +
                  "case when vna.cnt is null then 0 else vna.cnt end, " +
                  "sc.ServiceCenterName, cl.OpenDate, " +
                  "case when vco.cnt is null then 0 else vco.cnt end, " +
                  "case when vcd.cnt is null then 0 else vcd.cnt end " +
                  "order by ClaimAge desc ";
            rgClaimOpen.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            rgClaimOpen.Rebind();
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO.clsDBO clTD = new clsDBO.clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnUnlockClaim.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true)
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true)
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true)
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true)
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true)
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true)
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("unlockclaim")) == true)
                {
                    btnUnlockClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimpayment")) == false)
                {
                    btnInspectionWex.Enabled = false;
                    btnCarfaxPayment.Enabled = false;
                }
            }
        }
        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }
        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }
        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }
        protected void btnClaimSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnServiceCenters_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/servicecenters.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsopen.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimTeamOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimteamopen.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketMessage_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketResponse_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" + hfID.Value);
        }

        protected void btnUnlockClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/unlockclaim.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimAudit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnRFClaimSubmit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" + hfID.Value);
        }

        protected void btnLossCode_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimLossCode.aspx?sid=" + hfID.Value);
        }

        protected void btnAutonationACH_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/autonationach.aspx?sid=" + hfID.Value);
        }

        protected void btnInspectionWex_Click(object sender, EventArgs e)
        {

        }

        protected void btnCarfaxPayment_Click(object sender, EventArgs e)
        {

        }

        protected void rgClaimOpen_DataBound(object sender, EventArgs e)
        {
            int cnt;
            for (cnt = 0; cnt <= rgClaimOpen.Items.Count - 1; cnt++)
            {
                if (double.TryParse(rgClaimOpen.Items[cnt]["ClaimAge"].Text, out double ClaimAgeResult))
                {
                    if (ClaimAgeResult < 3)
                    {

                    }
                    else if (ClaimAgeResult == 3 || ClaimAgeResult == 4)
                    {
                        rgClaimOpen.Items[cnt]["ClaimAge"].BackColor = System.Drawing.Color.Yellow;
                    }
                    else if (ClaimAgeResult == 5 || ClaimAgeResult == 6)
                    {
                        rgClaimOpen.Items[cnt]["ClaimAge"].BackColor = System.Drawing.Color.Orange;
                    }
                    else if (ClaimAgeResult > 6)
                    {
                        rgClaimOpen.Items[cnt]["ClaimAge"].BackColor = System.Drawing.Color.Red;
                    }
                }
            }
            if (GetProcessDate(rgClaimOpen.Items[cnt-1]["ClaimNo"].Text) == GetMaxModDate(rgClaimOpen.Items[cnt-1]["ClaimNo"].Text))
            {
                rgClaimOpen.Items[cnt]["ClaimNo"].BackColor = System.Drawing.Color.Pink;
            }
        }
        private string GetProcessDate(string xClaimNo)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select processclaimdate from veritasclaims.dbo.claim where claimno = '" + xClaimNo + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("processclaimdate");
            }
            return "";
        }
        private string GetMaxModDate(string xClaimNo)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select Max(cd.ModDate) as MDate from claim cl " +
                  "inner join claimdetail cd on cd.claimid = cl.claimid " +
                  "where cl.claimno = '" + xClaimNo + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("mdate");
            }
            return "";
        }
    }
}
