﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimSearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsStates.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsStatus.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsUserInfo.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            
            if (!IsPostBack)
            {
                hfWebClaim.Value = "";
                GetServerInfo();
                trUserInfo.Visible = false;
                trUserInfoClose.Visible = false;
                CheckToDo();
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                trClaimResult.Visible = false;
                trServiceCenter.Visible = false;
                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
                if (hfUserID.Value == "1")
                {
                    btnLossCode.Visible = true;
                }
                else
                {
                    btnLossCode.Visible = false;
                }
                ReadOnlyButtons();
                btnRFClaimSubmit.Enabled = false;
                CheckRFClaimSubmit();
                PostMessage();
                CheckWebClaim();
            }
        }
        private void CheckWebClaim()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select claimid, claimno from claim " +
                  "where webclaim <> 0 " +
                  "and webview = 0 " +
                  "and assignedto = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0) 
            { 
                clR.GetRow();
                lblWebClaim.Text = "You have a Web Claim assigned to you. Claim No: " + clR.GetFields("claimno");
                hfWebClaimID.Value = clR.GetFields("claimid");
                ShowWebClaim();
            }
        }
        private void ShowWebClaim()
        {
            hfWebClaim.Value = "Visible";
            string script = "function f(){$find(\"" + rwWebClaim.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwWebClaim", script, true);
        }
        private void PostMessage()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clUI = new clsDBO.clsDBO();
            lblMessage.Text = "";
            SQL = "select * from userinfo ui " +
                  "inner join UserSecurityInfo usi on ui.UserID = usi.UserID " +
                  "where ui.userid = " + hfUserID.Value + " " +
                  "and teamid = 2 ";
            clUI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clUI.RowCount() > 0)
            {
                clUI.GetRow();
                if (Convert.ToInt32(clUI.GetFields("teamid")) == 2)
                {
                    SQL = "select * from veritasclaimticket.dbo.ticket where statusid = 3 ";
                    clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clR.RowCount() > 0)
                    {
                        lblMessage.Text = "You have received a message for a Ticket.";
                    }
                    SQL = "select * from veritasclaimticket.dbo.ticket where statusid = 2 ";
                    clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clR.RowCount() > 0)
                    {
                        lblMessage.Text = "You have received a response from ticket.";
                    }
                }
            }
        }
        private void CheckRFClaimSubmit()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            lblMessage.Text = "";
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + hfUserID.Value + " " +
                  "and teamlead <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                SQL = "select * from veritasclaims.dbo.claim " +
                      "where not sendclaim is null " +
                      "and processclaimdate is null ";
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() > 0)
                {
                    btnRFClaimSubmit.Enabled = true;
                    lblMessage.Text = "There is a claim submitted by a Repairshop Facility.";
                }
            }
        }
        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly").ToLower() == "true")
                {
                    btnServiceCenters.Enabled = false;
                    btnClaimsOpen.Enabled = false;
                    btnAutonationACH.Enabled = false;
                }
                if (Convert.ToBoolean(clR.GetFields("AllowClaimAudit")))
                {
                    btnClaimAudit.Enabled = true;
                }
                else
                {
                    btnClaimAudit.Enabled = false;
                }
                if (Convert.ToBoolean(clR.GetFields("teamlead")))
                {
                    btnClaimTeamOpen.Enabled = true;
                }
                else
                {
                    btnClaimTeamOpen.Enabled = false;
                }
                hfVeroOnly.Value = Convert.ToBoolean(clR.GetFields("veroonly")).ToString();
                hfAgentID.Value = clR.GetFields("agentid");
                hfSubAgentID.Value = clR.GetFields("subagentid");
                if (CheckANOnly())
                {
                    chkAutoNation.Visible = false;
                }
                if (hfVeroOnly.Value.ToLower() == "true") 
                {
                    chkAutoNation.Visible = false;
                }
                if (hfAgentID.Value.Length > 0)
                {
                    if (hfAgentID.Value != "0")
                    {
                        chkAutoNation.Visible = false;
                    }
                }
                if (hfSubAgentID.Value.Length > 0)
                {
                    if (hfSubAgentID.Value != "0")
                    {
                        chkAutoNation.Visible = false;
                    }
                }
            }
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO.clsDBO clTD = new clsDBO.clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnUnlockClaim.Enabled = false;
            btnTicketMessage.Enabled = false;
            btnTicketResponse.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true) 
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true) 
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true) 
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true) 
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true) 
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true) 
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true) 
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true) 
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true) 
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true) 
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("unlockclaim")) == true) 
                {
                    btnUnlockClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimpayment")) == false) 
                {
                    btnInspectionWex.Enabled = false;
                    btnCarfaxPayment.Enabled = false;
                }
            }
        }
        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnServiceCenters_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/servicecenters.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsopen.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimTeamOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimteamopen.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketMessage_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketResponse_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" + hfID.Value);
        }

        protected void btnUnlockClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/unlockclaim.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimAudit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnRFClaimSubmit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" + hfID.Value);
        }

        protected void btnLossCode_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimLossCode.aspx?sid=" + hfID.Value);
        }

        protected void btnAutonationACH_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/autonationach.aspx?sid=" + hfID.Value);
        }

        protected void btnInspectionWex_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimWexInspection.aspx?sid=" + hfID.Value);
        }

        protected void btnCarfaxPayment_Click(object sender, EventArgs e)
        {

        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }
        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            RunQuery();
        }
        private void RunQuery()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            SQL = "select claimid, c.contractid, contractno, claimno, cl.status, servicecenterno, servicecentername, sc.city, sc.state, vin, make, model, lossdate  from claim cl " +
                  "left join contract c on cl.contractid = c.contractid " +
                  "left join servicecenter sc on cl.servicecenterid = sc.servicecenterid " +
                  "left join dealer d on c.dealerid = d.dealerid " +
                  "left join agents a on a.agentid = d.agentsid " +
                  "where claimid > 0 ";
            if (txtContractNo.Text.Trim().Length > 0) {
                SQL = SQL + "and contractno like '%" + txtContractNo.Text.Trim() + "%' ";
            }
            if (txtRONumber.Text.Trim().Length > 0) {
                SQL = SQL + "and ronumber like '%" + txtRONumber.Text.Trim() + "%' ";
            }
            if (txtClaimNo.Text.Trim().Length > 0) {
                SQL = SQL + "and claimno like '%" + txtClaimNo.Text.Trim() + "%' ";
            }
            if (txtFName.Text.Trim().Length > 0) {
                SQL = SQL + "and fname like '%" + txtFName.Text.Trim() + "%' ";
            }
            if (txtLName.Text.Trim().Length > 0) {
                SQL = SQL + "and lname like '%" + txtLName.Text.Trim() + "%' ";
            }
            if (txtServiceCenterNo.Text.Trim().Length > 0) {
                SQL = SQL + "and cl.servicecenterid = " + hfServiceCenterID.Value;
            }
            if (txtCity.Text.Trim().Length > 0) {
                SQL = SQL + "and sc.city like '%" + txtCity.Text.Trim() + "%' ";
            }
            if (cboState.SelectedValue.Length > 0) {
                SQL = SQL + "and sc.state = '" + cboState.Text + "' ";
            }
            if (txtVIN.Text.Trim().Length > 0) {
                SQL = SQL + "and vin like '%" + txtVIN.Text.Trim() + "%' ";
            }
            if (cboStatus.Text.Length > 0) {
                SQL = SQL + "and cl.status = '" + cboStatus.SelectedValue + "' ";
            }
            if (hfAssignedTo.Value.Length > 0) {
                SQL = SQL + "and cl.assignedto = " + hfAssignedTo.Value + " ";
            }
            if (chkAutoNation.Checked) {
                SQL = SQL + "and sc.dealerno like '2%' ";
            }
            try
            {
                if (Convert.ToInt32(hfInsCarrierID.Value) > 0)
                {
                    SQL = SQL + "and c.inscarrierid = " + hfInsCarrierID.Value + " ";
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            if (CheckANOnly()) {
                SQL = SQL + "and (sc.dealerno like '2%' " + 
                            "or d.dealerno like '2%') ";
            }
            if (hfVeroOnly.Value.ToLower() == "true") {
                SQL = SQL + "and a.agentid = 54 ";
            }
            if (hfAgentID.Value.Length > 0) {
                if (hfAgentID.Value != "0") 
                {
                    SQL = SQL + "and d.agentsid = " + hfAgentID.Value + " ";
                }
            }
            if (hfSubAgentID.Value.Length > 0) {
                if (hfSubAgentID.Value != "0") {
                    SQL = SQL + "and d.subagentid = " + hfSubAgentID.Value + " ";
                }
            }
            SQL = SQL + "Union " +
                        "select claimgapid, c.contractid, contractno, claimno, s.ClaimGAPStatusdesc as status, '' as servicecenterno,  '' as servicecentername, c.city, c.state, vin, make, model, cl.ClaimDate as lossdate " +
                        "from claimgap cl " +
                        "left join contract c on cl.contractid = c.contractid " +
                        "left join dealer d on c.dealerid = d.dealerid " +
                        "left join agents a on a.agentid = d.agentsid " +
                        "left join ClaimGapStatus s on cl.Status = s.ClaimGAPStatusID " +
                        "where cl.claimgapid > 0 ";
            if (txtContractNo.Text.Trim().Length > 0) {
                SQL = SQL + "and contractno like '%" + txtContractNo.Text.Trim() + "%' ";
            }
            if (txtClaimNo.Text.Trim().Length > 0) {
                SQL = SQL + "and claimno like '%" + txtClaimNo.Text.Trim() + "%' ";
            }
            if (txtFName.Text.Trim().Length > 0) {
                SQL = SQL + "and c.fname like '%" + txtFName.Text.Trim() + "%' ";
            }
            if (txtLName.Text.Trim().Length > 0) {
                SQL = SQL + "and c.lname like '%" + txtLName.Text.Trim() + "%' ";
            }
            if (txtCity.Text.Trim().Length > 0) {
                SQL = SQL + "and c.city like '%" + txtCity.Text.Trim() + "%' ";
            }
            if (cboState.SelectedValue.Length > 0) {
                SQL = SQL + "and c.state = '" + cboState.Text + "%' ";
            }
            if (txtVIN.Text.Trim().Length > 0) {
                SQL = SQL + "and vin like '%" + txtVIN.Text.Trim() + "%' ";
            }
            if (cboStatus.Text.Length > 0) {
                SQL = SQL + "and s.claimgapstatusdesc = '" + cboStatus.SelectedValue + "' ";
            }
            if (hfAssignedTo.Value.Length > 0) {
                SQL = SQL + "and cl.CreBy = " + hfAssignedTo.Value + " ";
            }
            if (chkAutoNation.Checked) {
                SQL = SQL + "and c.dealerno like '2%' ";
            }
            try
            {
                if (Convert.ToInt32(hfInsCarrierID.Value) > 0)
                {
                    SQL = SQL + "and c.inscarrierid = " + hfInsCarrierID.Value + " ";
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            if (CheckANOnly()) {
                SQL = SQL + "or d.dealerno like '2%' ";
            }
            if (hfVeroOnly.Value.ToLower() == "true") {
                SQL = SQL + "and a.agentid = 54 ";
            }
            if (hfAgentID.Value.Length > 0) {
                if (hfAgentID.Value != "0") {
                    SQL = SQL + "and d.agentsid = " + hfAgentID.Value + " ";
                }
            }
            if (hfSubAgentID.Value.Length > 0) {
                if (hfSubAgentID.Value != "0") {
                    SQL = SQL + "and d.subagentid = " + hfSubAgentID.Value + " ";
                }
            }
            SQL = SQL + "order by LossDate desc ";
            rgClaim.DataSource = clC.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString).Tables[0];
            rgClaim.Rebind();
            trClaimResult.Visible = true;
            trServiceCenter.Visible = false;
            trUserInfo.Visible = false;
            trUserInfoClose.Visible = false;
        }
        private bool CheckANOnly()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + hfUserID.Value + " " +
                  "and autonationonly <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                return true;
            }
            return false;
        }
        protected void btnSCSeek_Click(object sender, EventArgs e)
        {
            trServiceCenter.Visible = true;
            rgServiceCenter.DataSourceID = "SQLDataSource1";
            rgServiceCenter.Rebind();
        }

        protected void rgServiceCenter_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtServiceCenterNo.Text = rgServiceCenter.SelectedValue.ToString();
            GetServiceCenterID();
            trServiceCenter.Visible = false;
        }
        private void GetServiceCenterID()
        {
            hfServiceCenterID.Value = 0.ToString();
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string SQL;
            SQL = "select * from servicecenter where servicecenterno = '" + txtServiceCenterNo.Text + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfServiceCenterID.Value = clR.GetFields("servicecenterid");
            }
        }
        protected void rgClaim_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            try
            {
                SQL = "select PlanTypeID, ProgramID from contract c where contractid = " + rgClaim.SelectedValue;
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() > 0)
                {
                    clR.GetRow();
                    if (Convert.ToInt32(clR.GetFields("programid")) != 103)
                    {
                        Response.Redirect("~/claim/claim.aspx?sid=" + hfID.Value + "&ClaimID=" + rgClaim.SelectedValue);
                    }
                    else
                    {
                        Response.Redirect("~/claim/claimgap.aspx?sid=" + hfID.Value + "&ClaimID=" + rgClaim.SelectedValue);
                    }
                }
            }
            catch (Exception ex)
            {
                //Response.Redirect("~/claim/claim.aspx?sid=" + hfID.Value + "&ClaimID=" + rgClaim.SelectedValues("ClaimID"));
                Console.WriteLine(ex.ToString());
            }
        }

        protected void btnSeekUser_Click(object sender, EventArgs e)
        {
            //trClaimResult.Visible = false;
            trUserInfo.Visible = true;
            trUserInfoClose.Visible = true;
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            //trClaimResult.Visible = true;
            trUserInfo.Visible = false;
            trUserInfoClose.Visible = false;
            hfAssignedTo.Value = "";
            txtAssignedTo.Text = "";
        }

        protected void rgUserInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            trClaimResult.Visible = true;
            trUserInfo.Visible = false;
            trUserInfoClose.Visible = false;
            hfAssignedTo.Value = rgUserInfo.SelectedValue.ToString();
            GetUserInfo();
        }
        private void GetUserInfo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from userinfo where userid = " + hfAssignedTo.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtAssignedTo.Text = clR.GetFields("fname") + " " + clR.GetFields("lname");
            }
        }
        protected void btnWebClaimOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwWebClaim.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
            AddWebNotify();
        }
        private void AddWebNotify()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string SQL;
            SQL = "update claim " +
                  "set webnotify = webnotify + 1 " +
                  "where claimid = " + hfWebClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        protected void rgClaim_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToCsv")
            {
                rgClaim.ExportSettings.ExportOnlyData = false;
                rgClaim.ExportSettings.IgnorePaging = true;
                rgClaim.ExportSettings.OpenInNewWindow = true;
                rgClaim.ExportSettings.UseItemStyles = true;
                rgClaim.ExportSettings.FileName = "";
                rgClaim.ExportSettings.Csv.FileExtension = "csv";
                rgClaim.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Comma;
                rgClaim.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
                rgClaim.MasterTableView.ExportToCSV();
            }
            if (e.CommandName == "ExportToExcel")
            {
                rgClaim.ExportSettings.ExportOnlyData = false;
                rgClaim.ExportSettings.IgnorePaging = true;
                rgClaim.ExportSettings.OpenInNewWindow = true;
                rgClaim.ExportSettings.UseItemStyles = true;
                rgClaim.ExportSettings.FileName = "";
                rgClaim.ExportSettings.Excel.FileExtension = "xlsx";
                rgClaim.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), GridExcelExportFormat.Xlsx.ToString());
                rgClaim.MasterTableView.ExportToExcel();
            }
            if (e.CommandName == "RebindGrid" || e.CommandName == "Sort" || e.CommandName == "Page")
            {
                RunQuery();
            }
        }

        protected void rgServiceCenter_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToCsv")
            {
                rgServiceCenter.ExportSettings.ExportOnlyData = false;
                rgServiceCenter.ExportSettings.IgnorePaging = true;
                rgServiceCenter.ExportSettings.OpenInNewWindow = true;
                rgServiceCenter.ExportSettings.UseItemStyles = true;
                rgServiceCenter.ExportSettings.FileName = "";
                rgServiceCenter.ExportSettings.Csv.FileExtension = "csv";
                rgServiceCenter.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Comma;
                rgServiceCenter.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
                rgServiceCenter.MasterTableView.ExportToCSV();
            }
            if (e.CommandName == "ExportToExcel")
            {
                rgServiceCenter.ExportSettings.ExportOnlyData = false;
                rgServiceCenter.ExportSettings.IgnorePaging = true;
                rgServiceCenter.ExportSettings.OpenInNewWindow = true;
                rgServiceCenter.ExportSettings.UseItemStyles = true;
                rgServiceCenter.ExportSettings.FileName = "";
                rgServiceCenter.ExportSettings.Excel.FileExtension = "xlsx";
                rgServiceCenter.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), GridExcelExportFormat.Xlsx.ToString());
                rgServiceCenter.MasterTableView.ExportToExcel();
            }
            if (e.CommandName == "RebindGrid" || e.CommandName == "Sort" || e.CommandName == "Page")
            {
                rgServiceCenter.Rebind();
            }
        }

        protected void rgUserInfo_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToCsv")
            {
                rgUserInfo.ExportSettings.ExportOnlyData = false;
                rgUserInfo.ExportSettings.IgnorePaging = true;
                rgUserInfo.ExportSettings.OpenInNewWindow = true;
                rgUserInfo.ExportSettings.UseItemStyles = true;
                rgUserInfo.ExportSettings.FileName = "";
                rgUserInfo.ExportSettings.Csv.FileExtension = "csv";
                rgUserInfo.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Comma;
                rgUserInfo.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
                rgUserInfo.MasterTableView.ExportToCSV();
            }
            if (e.CommandName == "ExportToExcel")
            {
                rgUserInfo.ExportSettings.ExportOnlyData = false;
                rgUserInfo.ExportSettings.IgnorePaging = true;
                rgUserInfo.ExportSettings.OpenInNewWindow = true;
                rgUserInfo.ExportSettings.UseItemStyles = true;
                rgUserInfo.ExportSettings.FileName = "";
                rgUserInfo.ExportSettings.Excel.FileExtension = "xlsx";
                rgUserInfo.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), GridExcelExportFormat.Xlsx.ToString());
                rgUserInfo.MasterTableView.ExportToExcel();
            }
            if (e.CommandName == "RebindGrid" || e.CommandName == "Sort" || e.CommandName == "Page")
            {
                rgUserInfo.Rebind();
            }
        }
    }
}