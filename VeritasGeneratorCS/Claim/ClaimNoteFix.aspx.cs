﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimNoteFix : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        protected void btnClean_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimnote " +
                  "where claimnotetypeid = 5 " +
                  "and (notetext like '%<p%>' " +
                  "or notetext like '%<div%') ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    ProcessNote(long.Parse(clR.GetFields("claimnoteid")));
                }
            }
        }
        private void ProcessNote(long xClaimNoteID)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimnote where claimnoteid = " + xClaimNoteID;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtNote.Content = clR.GetFields("note");
                clR.SetFields("notetext", txtNote.Text);
                clR.SaveDB();
            }
        }
    }
}