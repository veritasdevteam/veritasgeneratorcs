﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimVINWarranty.aspx.cs" Inherits="VeritasGeneratorCS.Claim.ClaimVINWarranty" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <telerik:RadGrid ID="rgWarranty" runat="server" AutoGenerateColumns="false" AllowSorting="true" Width="1000" ShowFooter="true">
                    <MasterTableView AutoGenerateColumns="false" DataKeyNames="WarrantyID" ShowFooter="true">
                        <Columns>
                            <telerik:GridBoundColumn DataField="WarrantyID" ReadOnly="true" Visible="false" UniqueName="WarrantyID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Name" UniqueName="Name" HeaderText="Name"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="WarrantyType" UniqueName="WarrantyType" HeaderText="Type"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Months" UniqueName="Months" HeaderText="Months"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Miles" UniqueName="Miles" HeaderText="Miles"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>


            <asp:HiddenField ID="hfContractID" runat="server" />
            <asp:HiddenField ID="hfClaimID" runat="server" />
            <asp:HiddenField ID="hfVIN" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
</body>
</html>
