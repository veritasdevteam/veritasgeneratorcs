﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimVINTransmission.aspx.cs" Inherits="VeritasGeneratorCS.Claim.ClaimVINTransmission" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Name:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtName" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Availability:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtAvailability" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Tansmission Type:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtTransmissionType" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Detail Type:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtDetail" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Gears:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtGears" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Order Code:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtOrderCode" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Fleet:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkFleet" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

                <asp:HiddenField ID="hfContractID" runat="server" />
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfVIN" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
