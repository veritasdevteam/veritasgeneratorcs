﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimCKParts.aspx.cs" Inherits="VeritasGeneratorCS.Claim.ClaimCKParts" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Claim CK Parts</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" BorderStyle="None" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimSearch" OnClick="btnClaimSearch_Click" runat="server" BorderStyle="None" Text="Claim Search"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnServiceCenters" OnClick="btnServiceCenters_Click" runat="server" BorderStyle="None" Text="Service Centers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLossCode" OnClick="btnLossCode_Click" runat="server" BorderStyle="None" Text="Loss Code"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnTicketMessage" OnClick="btnTicketMessage_Click" runat="server" BorderStyle="None" Text="Ticket Message"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnTicketResponse" OnClick="btnTicketResponse_Click" runat="server" BorderStyle="None" Text="Ticket Response"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUnlockClaim" OnClick="btnUnlockClaim_Click" runat="server" BorderStyle="None" Text="Unlock Claim"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAutonationACH" OnClick="btnAutonationACH_Click" runat="server" BorderStyle="None" Text="AutoNation ACH"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnInspectionWex" OnClick="btnInspectionWex_Click" runat="server" BorderStyle="None" Text="Wex Inspection"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCarfaxPayment" OnClick="btnCarfaxPayment_Click" runat="server" BorderStyle="None" Text="CarFax Payment"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" BorderStyle="None" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" BorderStyle="None" Text="Dealerships"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" BorderStyle="None" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" BorderStyle="None" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" BorderStyle="None" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" BorderStyle="None" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" OnClick="btnSettings_Click" runat="server" BorderStyle="None" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" BorderStyle="None" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" BorderStyle="None" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow HorizontalAlign="Left">
                                    <asp:TableCell Font-Bold="true">
                                        Quote ID:
                                    </asp:TableCell>
                                    <asp:TableCell >
                                        <asp:TextBox ID="txtQuoteID" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Order ID:
                                    </asp:TableCell>
                                    <asp:TableCell >
                                        <asp:TextBox ID="txtOrderID" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true" >
                                        Sent Date:
                                    </asp:TableCell>
                                    <asp:TableCell >
                                        <asp:TextBox ID="txtSentDate" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Button ID="btnRebuild" runat="server" Text="Rebuild Quote" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgPayment">
                                            <telerik:RadGrid ID="rgPayment" OnItemCommand="rgPayment_ItemCommand" runat="server" AutoGenerateColumns="false"
                                                AllowSorting="true" AllowPaging="false" ShowFooter="true"  MasterTableView-CommandItemDisplay="TopAndBottom"
                                                    Font-Names="Calibri" Font-Size="Small" DataSourceID="dsPart">
                                                <GroupingSettings CaseSensitive="false" />
                                                <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true" Width="1500" DataKeyNames="ClaimDetailID" >
                                                    <CommandItemTemplate>
                                                        <telerik:RadButton ID="updateBtn" runat="server" Text="Update Edited" CommandName="UpdateEdited"></telerik:RadButton>
                                                        <telerik:RadButton ID="cancelBtn" runat="server" Text="Cancel All" CommandName="CancelAll"></telerik:RadButton>
                                                    </CommandItemTemplate>
                                                    <CommandItemSettings ShowExportToExcelButton="false" ShowExportToCsvButton="false" ShowAddNewRecordButton="false" />
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="ClaimDetailID"  ReadOnly="true" Visible="false" UniqueName="ClaimDetailID"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="JobNo" AutoPostBackOnFilter="true" UniqueName="DateApproved" HeaderText="Job No"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PartNo" UniqueName="PartNo" HeaderText="Part No"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PartName" UniqueName="PartName" HeaderText="Part Name"></telerik:GridBoundColumn>
                                                        <telerik:GridTemplateColumn DataField="Qty" DataType="System.Decimal" ReadOnly="True" />
                                                        <telerik:GridBoundColumn DataField="ReqAmt" UniqueName="ReqAmt" HeaderText="ReqAmt" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="YourCost" UniqueName="YourCost" HeaderText="YourCost" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="TotalCost" UniqueName="TotalCost" HeaderText="TotalCost" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                                                        <telerik:GridButtonColumn CommandName="DeleteRow" CommandArgument="ClaimID" ButtonType="ImageButton" ImageUrl="~/images/delete.png"></telerik:GridButtonColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="dsPart" runat="server" SelectCommand="select cd.claimdetailid, cd.jobno, cd.partno, cd.partname, 
                                                cd.qty, cd.reqamt, cd.yourcost, cd.totalcost from ckpart cd inner join ckquote cq on cd.ckid = cq.ckid 
                                                where claimid = @claimid">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </telerik:RadAjaxPanel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right">
                                        <asp:Table runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    Total Part Cost:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadNumericTextBox ID="txtTotalPartCost" runat="server"></telerik:RadNumericTextBox>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    Shipping Cost:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadNumericTextBox ID="txtShippingCost" runat="server"></telerik:RadNumericTextBox>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    Total Cost:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadNumericTextBox ID="txtTotalCost" runat="server"></telerik:RadNumericTextBox>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow HorizontalAlign="Right">
                                    <asp:TableCell>
                                        <asp:Button ID="btnPurchase" runat="server" Text="Purchase" BackColor="#1eabe2" />
                                    </asp:TableCell>
                                </asp:TableRow>
                        </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
