﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimTicketMessage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsNote.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            if (!IsPostBack)
            {
                GetServerInfo();
                CheckToDo();
                pnlList.Visible = true;
                pnlDetail.Visible = false;
                FillClaimCombo();
            }
            if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
            {
                pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
            }
            else
            {
                pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
            }
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO.clsDBO clTD = new clsDBO.clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);

            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
                CheckRFClaimSubmit();
            }
        }
        private void CheckRFClaimSubmit()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + hfUserID.Value + " " +
                  "and teamlead <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                SQL = "select * from veritasclaims.dbo.claim " +
                      "where not sendclaim is null " +
                      "and processclaimdate is null ";
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() > 0)
                {
                    btnRFClaimSubmit.Enabled = true;
                }
            }
        }
        private void FillClaimCombo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select claimid, claimno from claim " +
                  "where assignedto = " + hfUserID.Value + " " +
                  "and claimid in (select claimid from veritasclaimticket.dbo.note) ";
            cboClaim.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            cboClaim.DataBind();
            hfClaimID.Value = cboClaim.SelectedValue;
            rgNote.Rebind();
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnReports.Enabled = false;
            btnUnlockClaim.Enabled = false;
            btnRFClaimSubmit.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true)
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true)
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true)
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true)
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true)
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true)
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("unlockclaim")) == true)
                {
                    btnUnlockClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimpayment")) == false)
                {
                    btnInspectionWex.Enabled = false;
                    btnCarfaxPayment.Enabled = false;
                }
            }
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnClaimSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnServiceCenters_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/servicecenters.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsopen.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimTeamOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimteamopen.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketMessage_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketResponse_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" + hfID.Value);
        }

        protected void btnUnlockClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/unlockclaim.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimAudit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnRFClaimSubmit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" + hfID.Value);
        }

        protected void btnLossCode_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimLossCode.aspx?sid=" + hfID.Value);
        }

        protected void btnAutonationACH_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/autonationach.aspx?sid=" + hfID.Value);
        }

        protected void btnInspectionWex_Click(object sender, EventArgs e)
        {

        }

        protected void btnCarfaxPayment_Click(object sender, EventArgs e)
        {

        }

        protected void rgNote_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            btnSave.Visible = false;
            txtNote.Enabled = false;
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            SQL = "select * from veritasclaimticket.dbo.note where noteid = " + rgNote.SelectedValue;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtNote.Content = clR.GetFields("note");
                txtCreBy.Text = clR.GetFields("creby");
                txtCreDate.Text = clR.GetFields("credate");
                txtResponseBy.Text = clR.GetFields("responseby");
                txtResponseDate.Text = clR.GetFields("responsedate");
            }
        }

        protected void btnAddNote_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            btnSave.Visible = true;
            txtNote.Enabled = true;
            txtNote.Content = "";
            txtCreBy.Text = "";
            txtCreDate.Text = "";
            txtResponseBy.Text = "";
            txtResponseDate.Text = "";
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from veritasclaimticket.dbo.note where noteid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("claimid", hfClaimID.Value);
                clR.SetFields("notetype", "Response");
                clR.SetFields("note", txtNote.Content);
                clR.SetFields("notetext", txtNote.Text);
                clR.SetFields("responseby", CalcUserName());
                clR.SetFields("responsedate", DateTime.Today.ToString());
                clR.AddRow();
                clR.SaveDB();
            }
            SQL = "update veritasclaimticket.dbo.ticket " +
                  "set statusid = 1 " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and statusid <> 4 ";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            SQL = "update veritasclaimticket.dbo.ticket " +
                  "set statusid = 2 " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and responseid <> 1 " +
                  "and statusid <> 4 ";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgNote.Rebind();
        }

        protected void cboClaim_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfClaimID.Value = cboClaim.SelectedValue;
            rgNote.Rebind();
        }
        private string CalcUserName()
        {
            string SQL;
            clsDBO.clsDBO clU = new clsDBO.clsDBO();
            SQL = "select * from userinfo where userid = " + hfUserID.Value;
            clU.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clU.RowCount() > 0)
            {
                clU.GetRow();
                return clU.GetFields("username");
            }
            return "";
        }

    }
}