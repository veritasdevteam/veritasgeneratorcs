﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimTicketResponse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsCDT.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsClaimDetailStatus.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsClaimReason.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsJobs.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            if (!IsPostBack)
            {
                GetServerInfo();
                CheckToDo();
                pnlDetail.Visible = false;
                pnlList.Visible = true;
                FillClaimCombo();
            }
            if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
            {
                pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
            }
            else
            {
                pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
            }
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO.clsDBO clTD = new clsDBO.clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);

            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
                CheckRFClaimSubmit();
            }
        }
        private void CheckRFClaimSubmit()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + hfUserID.Value + " " +
                  "and teamlead <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                SQL = "select * from veritasclaims.dbo.claim " +
                      "where not sendclaim is null " +
                      "and processclaimdate is null ";
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() > 0)
                {
                    btnRFClaimSubmit.Enabled = true;
                }
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnReports.Enabled = false;
            btnUnlockClaim.Enabled = false;
            btnRFClaimSubmit.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true)
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true)
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true)
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true)
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true)
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true)
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("unlockclaim")) == true)
                {
                    btnUnlockClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimpayment")) == false)
                {
                    btnInspectionWex.Enabled = false;
                    btnCarfaxPayment.Enabled = false;
                }
            }
        }
        private void FillClaimCombo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select claimid, claimno from claim " +
                  "where claimid in (select claimid from veritasclaimticket.dbo.ticket where responseid <> 1 and statusid = 2) ";
            cboClaim.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            cboClaim.DataBind();
            if (cboClaim.SelectedValue != "")
            {
                hfClaimID.Value = cboClaim.SelectedValue;
                FillList();
            }
            else 
            {
                hfClaimID.Value = "";
            }
            //FillList();
        }
        private void FillList()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select cd.claimdetailid, claimdetailtype,cd.partno, cd.claimdesc, claimdetailstatus, jobno, cd.losscode, lc.losscodedesc, " +
                  "payeename, reqamt, authamt, taxamt, paidamt, totalamt, r.response From claimdetail cd " +
                  "inner join veritasclaimticket.dbo.ticket T on t.claimdetailid = cd.claimdetailid " +
                  "inner join veritasclaimticket.dbo.response R on t.responseid = r.responseid " +
                  "left join claimpayee cp on cp.claimpayeeid = cd.claimpayeeid " +
                  "Left join claimlosscode lc on lc.losscode = cd.losscode " +
                  "where t.claimid = " + cboClaim.SelectedValue + " " +
                  "and (t.responseid = 2 or t.responseid = 3) and statusid = 2 " +
                  "order by jobno ";
            rgJobs.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            rgJobs.Rebind();
        }

        protected void cboClaim_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfClaimID.Value = cboClaim.SelectedValue;
        }

        protected void rgJobs_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfClaimDetailID.Value = rgJobs.SelectedValue.ToString();
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clT = new clsDBO.clsDBO();
            SQL = "select * from claimdetail where claimdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                SQL = "select * from veritasclaimticket.dbo.ticket t " +
                      "inner join veritasclaimticket.dbo.response r on r.responseid = t.responseid " +
                      "where claimdetailid = " + hfClaimDetailID.Value + " ";
                clT.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clT.RowCount() > 0)
                {
                    clT.GetRow();
                    cboClaimDetailType.SelectedValue = clR.GetFields("claimdetailtype");
                    txtJobNo.Text = clR.GetFields("jobno");
                    cboClaimDetailStatus.SelectedValue = clR.GetFields("claimdetailstatus");
                    txtPartNo.Text = clR.GetFields("partno");
                    txtClaimDesc.Text = clR.GetFields("claimdesc");
                    txtLossCode.Text = clR.GetFields("losscode");
                    cboReason.SelectedValue = clR.GetFields("claimreasonid");
                    txtLossCodeDesc.Text = GetLossCode(clR.GetFields("losscode"));
                    CalcPayee(long.Parse(clR.GetFields("claimpayeeid")));
                    txtReqQty.Text = clR.GetFields("reqqty");
                    txtReqCost.Text = clR.GetFields("reqcost");
                    txtReqAmt.Text = clR.GetFields("reqamt");
                    txtAuthQty.Text = clR.GetFields("authqty");
                    txtAuthAmt.Text = clR.GetFields("authcost");
                    txtTaxRate.Text = clR.GetFields("taxper");
                    txtTaxAmt.Text = clR.GetFields("taxamt");
                    txtTotalAmt.Text = clR.GetFields("totalamt");
                    txtAuthAmt.Text = clR.GetFields("authamt");
                    if (clR.GetFields("dateauth").Length > 0)
                    {
                        rdpAuthorizedDate.SelectedDate = DateTime.Parse(clR.GetFields("dateauth"));
                    }
                    else
                    {
                        rdpAuthorizedDate.Clear();
                    }
                    if (clR.GetFields("dateapprove").Length > 0)
                    {
                        rdpDateApprove.SelectedDate = DateTime.Parse(clR.GetFields("dateapprove"));
                    }
                    else
                    {
                        rdpDateApprove.Clear();
                    }
                    txtResponse.Text = clT.GetFields("response");
                    cboRateType.SelectedValue = clR.GetFields("ratetypeid");
                    if (Convert.ToInt32(clT.GetFields("responseid")) == 2)
                    {
                        btnCloseTicket.Visible = true;
                        btnCancelClaimDetail.Visible = true;
                        btnSaveClaimDetail.Visible = false;
                    }
                    else
                    {
                        btnCloseTicket.Visible = false;
                        btnCancelClaimDetail.Visible = true;
                        btnSaveClaimDetail.Visible = true;
                    }
                    pnlList.Visible = false;
                    pnlDetail.Visible = true;
                }
            }
        }
        private void GetResponse(long xResponseID)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from response where responseid = " + xResponseID;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtResponse.Text = clR.GetFields("response");
            }
        }
        private void CalcPayee(long xClaimPayeeID)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimpayee where claimpayeeid = " + xClaimPayeeID;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtPayeeNo.Text = clR.GetFields("payeeno");
                txtPayeeName.Text = clR.GetFields("payeename");
            }
        }
        private string GetLossCode(string xLossCode)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimlosscode where losscode = '" + xLossCode + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("losscodedesc");
            }
            return "";
        }
        protected void txtAuthQty_TextChanged(object sender, EventArgs e)
        {
            double dQty;
            double dCost;
            double dTax;
            double dTaxRate;
            if (txtAuthQty.Text.Length > 0) 
            {
                dQty = Convert.ToDouble(txtAuthQty.Text);
            } 
            else 
            {
                dQty = 0;
            }
            if (txtAuthCost.Text.Length > 0) 
            {
                dCost = Convert.ToDouble(txtAuthCost.Text);
            } 
            else 
            {
                dCost = 0;
            }
            if (txtTaxRate.Text.Length > 0) 
            {
                dTaxRate = Convert.ToDouble(txtTaxRate.Text) / 100;
            } 
            else
            {
                dTaxRate = 0;
            }
            dTax = dQty * dCost * dTaxRate;
            txtTaxAmt.Text = dTax.ToString();
            txtAuthAmt.Text = (dQty * dCost).ToString();
            txtTotalAmt.Text = ((dQty * dCost) + dTax).ToString();
            txtAuthCost.Focus();
        }

        protected void txtAuthCost_TextChanged(object sender, EventArgs e)
        {
            double dQty;
            double dCost;
            double dTax;
            double dTaxRate;
            if (txtAuthQty.Text.Length > 0) 
            {
                dQty = Convert.ToDouble(txtAuthQty.Text);
            } 
            else 
            {
                dQty = 0;
            }
            if (txtAuthCost.Text.Length > 0) 
            {
                dCost = Convert.ToDouble(txtAuthCost.Text);
            } 
            else 
            {
                dCost = 0;
            }
            if (txtTaxRate.Text.Length > 0) 
            {
                dTaxRate = Convert.ToDouble(txtTaxRate.Text) / 100;
            } 
            else 
            {
                dTaxRate = 0;
            }
            dTax = dQty * dCost * dTaxRate;
            dTax = Math.Round(dTax * 100) / 100;
            txtTaxAmt.Text = dTax.ToString();
            txtAuthAmt.Text = (dQty * dCost).ToString();
            txtTotalAmt.Text = ((dQty * dCost) + dTax).ToString();
            txtTaxRate.Focus();
        }

        protected void txtTaxRate_TextChanged(object sender, EventArgs e)
        {
            double dQty;
            double dCost;
            double dTax;
            double dTaxRate;
            if (txtAuthQty.Text.Length > 0) 
            {
                dQty = Convert.ToDouble(txtAuthQty.Text);
            } 
            else 
            {
                dQty = 0;
            }
            if (txtAuthCost.Text.Length > 0) 
            {
                dCost = Convert.ToDouble(txtAuthCost.Text);
            } 
            else 
            {
                dCost = 0;
            }
            if (txtTaxRate.Text.Length > 0) 
            {
                dTaxRate = Convert.ToDouble(txtTaxRate.Text) / 100;
            } 
            else 
            {
                dTaxRate = 0;
            }
            dTax = dQty * dCost * dTaxRate;
            dTax = Math.Round(dTax * 100) / 100;
            txtTaxAmt.Text = dTax.ToString();
            txtAuthAmt.Text = (dQty * dCost).ToString();
            txtTotalAmt.Text = ((dCost * dQty) + dTax).ToString();
            txtTotalAmt.Focus();
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnClaimSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnServiceCenters_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/servicecenters.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsopen.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimTeamOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimteamopen.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketMessage_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketResponse_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" + hfID.Value);
        }

        protected void btnUnlockClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/unlockclaim.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimAudit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnRFClaimSubmit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" + hfID.Value);
        }

        protected void btnLossCode_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimLossCode.aspx?sid=" + hfID.Value);
        }

        protected void btnAutonationACH_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/autonationach.aspx?sid=" + hfID.Value);
        }

        protected void btnInspectionWex_Click(object sender, EventArgs e)
        {

        }

        protected void btnCarfaxPayment_Click(object sender, EventArgs e)
        {

        }
        private void FillRateType()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select ratetypeid, RateTypeName from ratetype " +
                  "where ratetypeid in (1,2834, 1101) " +
                  "or ratetypeid in (select RateTypeID from ratetype " +
                  "where ratecategoryid = 5 " +
                  "and ratetypeid in (select ratetypeid " +
                  "from contractcommissions " +
                  "where agentid in (select agentsid from contract " +
                  "where contractid in (select contractid from claim " +
                  "where claimid = " + hfClaimID.Value + " " + ")))) " +
                  "or RateTypeID in (select 10 from contract " +
                  "where contractid in (select contractid from claim where claimid = " + hfClaimID.Value + " ) " +
                  "and (contractno like 'r%' or contractno like 'vel%')) " +
                  "or RateTypeID in (select 2868 from contract " +
                  "where contractid in (select contractid from claim where claimid = " + hfClaimID.Value + " ) " +
                  "and (contractno like 'va%' or contractno like 'vg%' or contractno like 'vep%')) " +
                  "or RateTypeID in (select 24 from contract c " +
                  "inner join contractamt ca on c.contractid = ca.contractid " +
                  "where c.contractid in (select contractid from claim where claimid =  " + hfClaimID.Value + " ) " + "and ca.RateTypeID = 24) " +
                  "or RateTypeID in (select 25 from contract c " +
                  "inner join contractamt ca on c.contractid = ca.contractid " +
                  "where c.contractid in (select contractid from claim where claimid = " + hfClaimID.Value + " ) " +
                  "and ca.RateTypeID = 25) " +
                  "order by ratetypeid ";
            cboRateType.Items.Clear();
            long a = cboRateType.Items.Count;
            Telerik.Web.UI.RadComboBoxItem i1 = new Telerik.Web.UI.RadComboBoxItem();
            i1.Value = "0";
            i1.Text = "";
            cboRateType.Items.Add(i1);
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    i1 = new Telerik.Web.UI.RadComboBoxItem();
                    i1.Value = clR.GetFields("ratetypeid");
                    i1.Text = clR.GetFields("ratetypename");
                    cboRateType.Items.Add(i1);
                }
            }
        }

        protected void btnSaveClaimDetail_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimdetail where claimdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                clR.SetFields("claimid", hfClaimID.Value);
                clR.SetFields("claimdesc", txtClaimDesc.Text);
                clR.SetFields("claimdetailtype", cboClaimDetailType.SelectedValue);
                if (txtJobNo.Text.Length > 0) 
                {
                    clR.SetFields("jobno", txtJobNo.Text);
                }
                clR.SetFields("losscode", txtLossCode.Text);
                clR.SetFields("reqamt", txtReqAmt.Text);
                clR.SetFields("authcost", txtAuthCost.Text);
                if (txtTaxRate.Text.Length > 0) 
                {
                    clR.SetFields("taxper", (Convert.ToDouble(txtTaxRate.Text) / 100).ToString());
                }
                if (txtTaxAmt.Text.Length == 0) 
                {
                    clR.SetFields("taxamt", "0");
                } 
                else 
                {
                    if (clR.GetFields("authqty").Length == 0) 
                    {
                        clR.SetFields("authqty", "0");
                    }
                    clR.SetFields("taxamt", txtTaxAmt.Text);
                }

                clR.SetFields("totalamt", txtTotalAmt.Text);
                clR.SetFields("Authamt", txtAuthAmt.Text);
                clR.SetFields("claimdetailstatus", cboClaimDetailStatus.SelectedValue);
                clR.SetFields("claimreasonid", cboReason.SelectedValue);
                if (rdpAuthorizedDate.SelectedDate != null) 
                {
                    clR.SetFields("dateauth", rdpAuthorizedDate.SelectedDate.ToString());
                } 
                else 
                {
                    clR.SetFields("dateauth", "");
                }
                if (rdpDateApprove.SelectedDate != null) 
                {
                    clR.SetFields("dateapprove", rdpDateApprove.SelectedDate.ToString());
                } 
                else 
                {
                    clR.SetFields("dateapprove", "");
                }
                if (clR.GetFields("ratetypeid") == "1") 
                {
                    if (cboRateType.SelectedValue != "1") 
                    {
                        clR.SetFields("slushbyid", hfUserID.Value);
                        clR.SetFields("slushdate", DateTime.Today.ToString());
                    }
                }
                clR.SetFields("ratetypeid", cboRateType.SelectedValue);
                clR.SetFields("reqqty", txtReqQty.Text);
                clR.SetFields("reqcost", txtReqCost.Text);
                clR.SetFields("authqty", txtAuthQty.Text);
                clR.SetFields("authcost", txtAuthCost.Text);
                clR.SetFields("moddate", DateTime.Today.ToString());
                clR.SetFields("modby", hfUserID.Value);
                //'if (cboClaimDetailStatus.SelectedValue = "Denied" Then
                //'    CheckClose()
                //'    GoTo MoveToSave
                //'End If
                //'If cboClaimDetailStatus.SelectedValue = "Cancelled" Then
                //'    CheckClose()
                //'    GoTo MoveToSave
                //'End If
                //'If cboClaimDetailStatus.SelectedValue = "Requested" Then
                //'    CheckClose()
                //'    GoTo MoveToSave
                //'End If
                if (clR.GetFields("claimdetailstatus") != "Paid") 
                {
                    if (cboClaimDetailStatus.SelectedValue == "Approved") 
                    {
                        if (txtAuthAmt.Text.Length == 0) 
                        {
                            txtAuthAmt.Text = "0";
                        }
                        if (clR.GetFields("claimdetailid").Length > 0)
                        {
                            if (!CheckLimitApproved(Convert.ToDouble(txtAuthAmt.Text), long.Parse(clR.GetFields("claimdetailid")))) 
                            {
                                lblApprovedError.Text = "Send to Manager for Approval";
                                goto MoveHere;
                            }
                        } 
                        else 
                        {
                            if (!CheckLimitApproved(Convert.ToDouble(txtAuthAmt.Text), 0)) 
                            {
                                lblApprovedError.Text = "Send to Manager for Approval";
                                goto MoveHere;
                            }
                        }
                    }
                    if (cboClaimDetailStatus.SelectedValue == "Authorized") 
                    {
                        if (txtAuthAmt.Text.Length == 0) 
                        {
                            txtAuthAmt.Text = "0";
                        }
                        if (!CheckLimitAuthorized(Convert.ToDouble(txtAuthAmt.Text), long.Parse(clR.GetFields("claimdetailid")))) 
                        {
                            lblApprovedError.Text = "Send to Manager for Approval";
                            clR.SetFields("claimdetailstatus", "Requested");
                        }
                    }

                }
        MoveToSave:;
                if (clR.RowCount() == 0) 
                {
                    clR.SetFields("credate", DateTime.Today.ToString());
                    clR.SetFields("creby", hfUserID.Value);
                    clR.AddRow();
                }
                clR.SaveDB();
            MoveHere:;
                pnlDetail.Visible = false;
                pnlList.Visible = true;
                FillList();
                rgJobs.Rebind();
                SQL = "update claim " +
                      "set moddate = '" + DateTime.Today + "', " +
                      "modby = " + hfUserID.Value + " " +
                      "where claimid = " + hfClaimID.Value;
                clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            }
            CloseTicket();
        }
        private bool CheckLimitApproved(double xAmt, long xClaimDetailID)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            double dLimit;
            SQL = "select claimapprove from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                dLimit = Convert.ToDouble(clR.GetFields("claimapprove"));
            }
            else
            {
                dLimit = 0;
            }
            SQL = "select sum(authamt) as Amt from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and (claimdetailstatus = 'approved' " +
                  "or claimdetailstatus = 'Authorized') " +
                  "and claimdetailid <> " + xClaimDetailID;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("amt").Length > 0)
                {
                    if (dLimit > Convert.ToDouble(clR.GetFields("amt")) + xAmt)
                    {
                        return true;
                    }
                }
                else
                {
                    if (dLimit > xAmt)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        private bool CheckLimitAuthorized(double xAmt, long xClaimDetailID)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            double dLimit;
            SQL = "select claimauth from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                dLimit = Convert.ToDouble(clR.GetFields("claimauth"));
            }
            else
            {
                dLimit = 0;
            }
            SQL = "select sum(authamt) as Amt from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and (claimdetailstatus = 'approved' " +
                  "or claimdetailstatus = 'Authorized') " +
                  "and claimdetailid <> " + xClaimDetailID;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("amt").Length > 0)
                {
                    if (dLimit > Convert.ToDouble(clR.GetFields("amt")) + xAmt)
                    {
                        return true;
                    }
                }
                else
                {
                    if (dLimit > xAmt)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        protected void btnCancelClaimDetail_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            FillList();
        }

        protected void btnCloseTicket_Click(object sender, EventArgs e)
        {
            CloseTicket();
            FillList();
            pnlDetail.Visible = false;
            pnlList.Visible = true;
        }
        private void CloseTicket()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update veritasclaimticket.dbo.ticket " +
                  "set statusid = 4 " +
                  "where claimdetailid = " + hfClaimDetailID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
    }
}