﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimSupplier.aspx.cs" Inherits="VeritasGeneratorCS.Claim.ClaimSupplier" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" Height="200px" Width="300px" PostBackControls="rgClaimDetail">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <telerik:RadGrid ID="rgClaimDetail" OnSelectedIndexChanged="rgClaimDetail_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="false" 
                                    AllowSorting="true"  Width="1500" ShowFooter="true" AllowPaging="true" DataSourceID="dsClaimDetail">
                                    <GroupingSettings CaseSensitive="false" />
                                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="false" DataSourceID="dsClaimDetail" DataKeyNames="ClaimDetailID" ShowFooter="true">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ClaimDetailID" UniqueName="ClaimDetailID" Visible="false"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="JobNo" UniqueName="JobNo" HeaderText="Job No"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PartNo" UniqueName="PartNo" HeaderText="Part No"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ClaimDesc" UniqueName="ClaimDesc" HeaderText="Part Desc"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ReqQty" UniqueName="ReqQty" HeaderText="Req Qty"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ReqCost" UniqueName="ReqCost" HeaderText="Req Cost" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings EnablePostBackOnRowClick="true">
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Font-Bold="true">
                                Quotes:
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <telerik:RadGrid ID="rgClaimDetailQuote" OnItemCommand="rgClaimDetailQuote_ItemCommand" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="false" 
                                    AllowSorting="true"  Width="1500" ShowFooter="true" AllowPaging="true">
                                    <GroupingSettings CaseSensitive="false" />
                                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="false" DataKeyNames="ClaimDetailQuoteID" ShowFooter="true">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ClaimDetailQuoteID" UniqueName="ClaimDetailQuoteID" Visible="false"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ClaimDetailID" UniqueName="ClaimDetailID" Visible="false"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CompanyName" UniqueName="Company" HeaderText="Company"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PartNo" UniqueName="PartNo" HeaderText="Part No"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PartDesc" UniqueName="PartDesc" HeaderText="Part Desc"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ListPrice" UniqueName="ListPrice" HeaderText="ListPrice" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="YourCost" UniqueName="YourCost" HeaderText="Your Cost" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                            <telerik:GridCheckBoxColumn DataField="OEM" UniqueName="OEM" HeaderText="OEM"></telerik:GridCheckBoxColumn>
                                            <telerik:GridButtonColumn ButtonType="PushButton" Text="Purchase" CommandName="Purchase"></telerik:GridButtonColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings EnablePostBackOnRowClick="true">
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Panel runat="server" ID="pnlAfterMarket">
                                    <asp:Table runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell Font-Bold="true">
                                                After Market
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Table runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell Font-Bold="true">
                                                            Order ID:
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtAMOrderID" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                        <asp:TableCell Font-Bold="true">
                                                            Order By:
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtAMOrderBy" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                        <asp:TableCell Font-Bold="true">
                                                            Order Date:
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtAMOrderDate" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadGrid ID="rgAMToBePurchase" OnItemCommand="rgAMToBePurchase_ItemCommand" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="false" 
                                                    AllowSorting="true"  Width="1500" ShowFooter="true" AllowPaging="true">
                                                    <GroupingSettings CaseSensitive="false" />
                                                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="false" DataKeyNames="ClaimDetailPurchaseID" ShowFooter="true">
                                                        <Columns>
                                                            <telerik:GridBoundColumn DataField="ClaimDetailPurchaseID" UniqueName="ClaimDetailQuoteID" Visible="false"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ClaimDetailQuoteID" UniqueName="ClaimDetailQuoteID" Visible="false"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ClaimDetailID" UniqueName="ClaimDetailID" Visible="false"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Company" UniqueName="Company" HeaderText="Company"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="PartNo" UniqueName="PartNo" HeaderText="Part No"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="PartDesc" UniqueName="PartDesc" HeaderText="Part Desc"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ListPrice" UniqueName="ListPrice" HeaderText="ListPrice" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="YourCost" UniqueName="YourCost" HeaderText="Your Cost" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                                            <telerik:GridCheckBoxColumn DataField="OEM" UniqueName="OEM" HeaderText="OEM"></telerik:GridCheckBoxColumn>
                                                            <telerik:GridButtonColumn ButtonType="PushButton" Text="Delete" CommandName="Delete"></telerik:GridButtonColumn>
                                                        </Columns>
                                                    </MasterTableView>
                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                        <Selecting AllowRowSelect="true" />
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Right">
                                                <asp:Table runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell Font-Bold="true">
                                                            Sub Total:
                                                        </asp:TableCell>
                                                        <asp:TableCell HorizontalAlign="Right">
                                                            <asp:TextBox ID="txtAMSubTotal" ReadOnly="true" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell Font-Bold="true">
                                                            Shipping Amt:
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtAMShippingAmt" ReadOnly="true" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell Font-Bold="true">
                                                            Total:
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtAMTotal" ReadOnly="true" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell ColumnSpan="2">
                                                            <asp:Button ID="btnAMCompletePurchase" OnClick="btnAMCompletePurchase_Click" BackColor="#1eabe2" runat="server" Text="Complete Purchase" />
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:Panel>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Panel runat="server" ID="pnlOEM">
                                    <asp:Table runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell Font-Bold="true">
                                                OEM
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Table runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell Font-Bold="true">
                                                            Order ID:
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtOEMOrderID" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                        <asp:TableCell Font-Bold="true">
                                                            Order By:
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtOEMOrderBy" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                        <asp:TableCell Font-Bold="true">
                                                            Order Date:
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtOEMOrderDate" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell Font-Bold="true">
                                                Purchased Items:
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadGrid ID="rgOEMPurchase" OnItemCommand="rgOEMPurchase_ItemCommand" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="false" 
                                                    AllowSorting="true"  Width="1500" ShowFooter="true" AllowPaging="true">
                                                    <GroupingSettings CaseSensitive="false" />
                                                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="false" DataKeyNames="ClaimDetailPurchaseID" ShowFooter="true">
                                                        <Columns>
                                                            <telerik:GridBoundColumn DataField="ClaimDetailPurchaseID" UniqueName="ClaimDetailQuoteID" Visible="false"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ClaimDetailQuoteID" UniqueName="ClaimDetailQuoteID" Visible="false"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ClaimDetailID" UniqueName="ClaimDetailID" Visible="false"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Company" UniqueName="Company" HeaderText="Company"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="PartNo" UniqueName="PartNo" HeaderText="Part No"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="PartDesc" UniqueName="PartDesc" HeaderText="Part Desc"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ListPrice" UniqueName="ListPrice" HeaderText="ListPrice" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="YourCost" UniqueName="YourCost" HeaderText="Your Cost" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                                            <telerik:GridCheckBoxColumn DataField="OEM" UniqueName="OEM" HeaderText="OEM"></telerik:GridCheckBoxColumn>
                                                            <telerik:GridButtonColumn ButtonType="PushButton" Text="Delete" CommandName="Delete"></telerik:GridButtonColumn>
                                                        </Columns>
                                                    </MasterTableView>
                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                        <Selecting AllowRowSelect="true" />
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Right">
                                                <asp:Table runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell Font-Bold="true">
                                                            Sub Total:
                                                        </asp:TableCell>
                                                        <asp:TableCell HorizontalAlign="Right">
                                                            <asp:TextBox ID="txtOEMSubtotal" ReadOnly="true" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell Font-Bold="true">
                                                            Shipping Amt:
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtOEMShippingAmt" ReadOnly="true" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell Font-Bold="true">
                                                            Total:
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtOEMTotal" ReadOnly="true" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell ColumnSpan="2">
                                                            <asp:Button ID="btnOEMCompletePurchase" OnClick="btnOEMCompletePurchase_Click" BackColor="#1eabe2" runat="server" Text="Complete Purchase" />
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:Panel>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <asp:SqlDataSource ID="dsClaimDetail" ProviderName="System.Data.SqlClient" 
                            SelectCommand="select claimdetailid, jobno, partno, claimdesc, reqqty, reqcost from claimdetail cl
                            where claimid = @ClaimID and claimdetailtype = 'Part' and claimdetailstatus <> 'Denied' " runat="server">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                    </asp:SqlDataSource>
                </telerik:RadAjaxPanel>
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfClaimDetailID" runat="server" />
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfClaimDetailQuoteID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>