﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimCustomer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfContractID.Value = Request.QueryString["contractid"];
            dsStates.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            if (!IsPostBack)
            {
                GetServerInfo();
                FillCustomer();
                btnUpdateCustomer.Visible = true;
                ReadOnlyButtons();
            }
        }
        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            btnUpdateCustomer.Enabled = true;
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + hfUserID.Value + " " +
                  "and readonly <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                btnUpdateCustomer.Enabled = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private void FillCustomer()
        {
            GetContract();
        }
        private void GetContract()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            if (hfContractID.Value.Length == 0)
            {
                return;
            }
            SQL = "select fname, lname, addr1, addr2, city, state, zip, phone " +
                  "from contract " +
                  "where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                txtFName.Text = clC.GetFields("fname");
                txtLName.Text = clC.GetFields("lname");
                txtAddr1.Text = clC.GetFields("addr1");
                txtAddr2.Text = clC.GetFields("addr2");
                txtCity.Text = clC.GetFields("city");
                cboState.SelectedValue = clC.GetFields("state");
                txtZip.Text = clC.GetFields("zip");
                txtPhone.Text = clC.GetFields("phone");
            }
        }

        protected void btnUpdateCustomer_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            VeritasGlobalToolsV2.clsMoxyAddressChange clMAC = new VeritasGlobalToolsV2.clsMoxyAddressChange();
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                clC.SetFields("fname", txtFName.Text);
                clC.SetFields("lname", txtLName.Text);
                clC.SetFields("addr1", txtAddr1.Text);
                clC.SetFields("addr2", txtAddr2.Text);
                clC.SetFields("city", txtCity.Text);
                clC.SetFields("state", cboState.SelectedValue);
                clC.SetFields("zip", txtZip.Text);
                clC.SetFields("phone", txtPhone.Text);
                clC.SaveDB();
                clMAC.UpdateMoxy(long.Parse(hfContractID.Value));
            }
        }
    }
}