﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimVINWarranty : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfContractID.Value = Request.QueryString["contractid"];
            if (!IsPostBack)
            {
                GetVIN();
                if (hfVIN.Value.Length > 0)
                {
                    FillPage();
                }
            }
        }
        private void GetVIN()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            SQL = "select vin from contract c where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfVIN.Value = clC.GetFields("vin");
            }
        }
        private void FillPage()
        {
            string SQL;
            clsDBO.clsDBO clV = new clsDBO.clsDBO();
            SQL = "select * from vin.dbo.vin v " +
                  "inner join vin.dbo.warranty bd on bd.vinid = v.vinid " +
                  "where vin = '" + hfVIN.Value.Substring(0, 11) + "' ";
            rgWarranty.DataSource = clV.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
    }
}