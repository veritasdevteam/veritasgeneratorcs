﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class Claim3C : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["claimid"];
            GetServerInfo();
            if (!IsPostBack)
            {
                FillNotes();
                if (CheckLock())
                {
                    btnUpdateNotes.Visible = false;
                }
                else
                {
                    btnUpdateNotes.Visible = true;
                }
            }
        }
        private bool CheckLock()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "Select claimid from claim " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "And lockuserid <> " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private void FillNotes()
        {
            GetComplaint();
            GetCause();
            GetCorrect();
        }
        private void GetCorrect()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select claimnoteid, note from claimnote " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimnotetypeid = 2 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfCorrectNoteID.Value = clR.GetFields("claimnoteid");
                txtCorrect.Text = clR.GetFields("note");
            }
        }
        private void GetCause()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select claimnoteid, note from claimnote " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimnotetypeid = 3 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfCauseNoteID.Value = clR.GetFields("claimnoteid");
                txtCause.Text = clR.GetFields("note");
            }
        }
        private void GetComplaint()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select claimnoteid, note from claimnote " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimnotetypeid = 1 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfComplaintNoteID.Value = clR.GetFields("claimnoteid");
                txtComplaint.Text = clR.GetFields("note");
            }
        }

        protected void btnUpdateNotes_Click(object sender, EventArgs e)
        {
            SaveComplaint();
            SaveCause();
            SaveCorrect();
            FillNotes();
        }
        private void SaveCorrect()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfCorrectNoteID.Value.Length == 0)
            {
                hfCorrectNoteID.Value = 0.ToString();
            }
            SQL = "select * from claimnote where claimnoteid = " + hfCorrectNoteID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("claimid", hfClaimID.Value);
                clR.SetFields("claimnotetypeid", 2.ToString());
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", hfUserID.Value);
            }
            else
            {
                clR.GetRow();
            }
            clR.SetFields("note", txtCorrect.Text);
            clR.SetFields("moddate", DateTime.Today.ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.SetFields("scsclaimnoteid", 0.ToString());
            if (clR.RowCount() == 0)
            {
                clR.AddRow();
            }
            clR.SaveDB();
        }
        private void SaveCause()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfCauseNoteID.Value.Length == 0)
            {
                hfCauseNoteID.Value = 0.ToString();
            }
            SQL = "select * from claimnote where claimnoteid = " + hfCauseNoteID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("claimid", hfClaimID.Value);
                clR.SetFields("claimnotetypeid", 3.ToString());
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", hfUserID.Value);
            }
            else
            {
                clR.GetRow();
            }
            clR.SetFields("note", txtCause.Text);
            clR.SetFields("moddate", DateTime.Today.ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.SetFields("scsclaimnoteid", 0.ToString());
            if (clR.RowCount() == 0)
            {
                clR.AddRow();
            }
            clR.SaveDB();
        }
        private void SaveComplaint()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfComplaintNoteID.Value.Length == 0)
            {
                hfComplaintNoteID.Value = 0.ToString();
            }

            SQL = "select * from claimnote where claimnoteid = " + hfComplaintNoteID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("claimid", hfClaimID.Value);
                clR.SetFields("claimnotetypeid", 1.ToString());
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", hfUserID.Value);
            }
            else
            {
                clR.GetRow();
            }
            clR.SetFields("note", txtComplaint.Text);
            clR.SetFields("moddate", DateTime.Today.ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.SetFields("scsclaimnoteid", 0.ToString());
            if (clR.RowCount() == 0)
            {
                clR.AddRow();
            }
            clR.SaveDB();
        }
    }
}