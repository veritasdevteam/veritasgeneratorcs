﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS
{
    public partial class AgentDashboard : System.Web.UI.UserControl
    {
        public string sID;

        protected void Page_Load(object sender, EventArgs e)
        {
               
        }

        public void Update()
        {
            hfID.Value = sID;
            GetServerInfo();
            SetDashboard();
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private void SetDashboard()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            try
            {
                trBlank.Visible = true;
                trDashboard.Visible = false;
                trOpen.Visible = false;
                SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() > 0)
                {
                    clR.GetRow();
                    trBlank.Visible = false;
                    trDashboard.Visible = true;
                    trOpen.Visible = true;
                    SQL = "select UserID, CallDate,'Inbound Calls' as title ,InboundCt as count, " +
                    "(case when(select count(goal) from UserGoals where GoalsID = 1 and UserID = ia.UserID) = 0 then '0' else (select goal from UserGoals where GoalsID = 1 and UserID = ia.UserID) end )as goal " +
                    "from VeritasPhone.dbo.AgentDailyCallSummary ia where UserID = " + hfUserID.Value + "and CallDate <= '" + DateTime.Today.AddDays(-1) + "' " +
                    "and CallDate > '" + DateTime.Today.AddDays(-8) + "' and((DATEPART(dw, CallDate) + @@DATEFIRST) % 7) NOT IN(0) ";
                    htmlInbound.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    htmlInbound.ChartTitle.Text = "Inbound Stats for " + DateTime.Today.AddDays(-1);
                    SQL = "select UserID, CallDate,'Outbound Calls' as title ,OutboundCt as count, " +
                    "(case when(select count(goal) from UserGoals where GoalsID = 2 and UserID = ia.UserID) = 0 then '0' else (select goal from UserGoals where GoalsID = 2 and UserID = ia.UserID) end )as goal " +
                    "from VeritasPhone.dbo.AgentDailyCallSummary ia where UserID = " + hfUserID.Value + "and CallDate <= '" + DateTime.Today.AddDays(-1) + "' " +
                    "and CallDate > '" + DateTime.Today.AddDays(-8) + "' and((DATEPART(dw, CallDate) + @@DATEFIRST) % 7) NOT IN(0) ";
                    htmlOutBound.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    htmlOutBound.ChartTitle.Text = "Outbound for " + DateTime.Today.AddDays(-1);
                    SQL = "select userID, date as CallDate, 'DnD Total' as title, dndInstanceCount as count, " +
                    "(case when(select count(goal) from UserGoals where GoalsID = 3 and UserID = ia.UserID) = 0 then '0' else (select goal from UserGoals where GoalsID = 3 and UserID = ia.UserID) end )as goal " +
                    "from VeritasPhone.dbo.AgentDnD ia where UserID = " + hfUserID.Value + "and Date <= '" + DateTime.Today.AddDays(-1) + "' " +
                    "and Date > '" + DateTime.Today.AddDays(-8) + "' and((DATEPART(dw, Date) + @@DATEFIRST) % 7) NOT IN(0) ";
                    htmlDnD.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    htmlDnD.ChartTitle.Text = "DnD for " + DateTime.Today.AddDays(-1);
                    SQL = "select claimid, ClaimNo, CreDate,ModDate, DATEDIFF(DAY,CreDate,GETDATE()) as ClaimAge, ca.ActivityDesc from Claim  " +
                    "cl left join ClaimActivity ca on cl.ClaimActivityID = ca.ClaimActivityID " +
                    "where Status = 'open' and AssignedTo = " + hfUserID.Value + " order by ClaimAge desc";
                    rgClaimOpen.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    rgClaimOpen.DataBind();
                }
            } 
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        protected void rgClaimOpen_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claim.aspx?sid=" + hfID.Value + "&ClaimID=" + Convert.ToString(rgClaimOpen.SelectedValue));
        }

        protected void rgClaimOpen_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            SetDashboard();
        }
    }
}