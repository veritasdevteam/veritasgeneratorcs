﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;

namespace VeritasGlobalToolsV2
{
    public class clsANClaim
    {
        private string sClaimID;
        private long lActivityID;
        private string SQL; 
        private string sEmail; 
        private string sActivity; 
        private string sClaimNo;
        private long lContractID;
        private string sAdvisorName;
        private long lUserID;
        public void ProcessANClaim(string xClaimID, long xActivityID, long xUserID)
        {
            lUserID = xUserID;
            lActivityID = xActivityID;
            sClaimID = xClaimID;
            if (!CheckAN())
            {
                return;
            }
            GetActivity();
            SendEmail();
            SaveToDB();
        }
        private void SaveToDB()
        {

        }
        private void SendEmail()
        {

        }
        private void GetUserName()
        {

        }
        private void GetActivity()
        {

        }
        private bool CheckAN()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select cl.contractid, cl.claimno, dealerno, sccontactemail from claim cl " +
                  "inner join servicecenter sc on cl.servicecenterid = sc.servicecenterid " +
                  "where cl.claimid = " + sClaimID + " " +
                  "and dealerno like '2%' " +
                  "and not sccontactemail is null ";
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                lContractID = long.Parse(clR.GetFields("contractid"));
                sClaimNo = clR.GetFields("claimno");
                sEmail = clR.GetFields("sccontactemail");
                return true;
            }
            return false;
        }
    }
}
