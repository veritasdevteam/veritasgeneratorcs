﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritasGlobalToolsV2
{
    public class clsPayee
    {
        private long lContractID;
        private long lPayeeID;
        private string SQL;
        private clsDBO.clsDBO clC = new clsDBO.clsDBO();
        private clsDBO.clsDBO clD = new clsDBO.clsDBO();
        private clsDBO.clsDBO clP = new clsDBO.clsDBO();
        private string sCompanyName;
        private string sFName;
        private string sLName; 
        private string sAddr1; 
        private string sAddr2; 
        private string sCity;
        private string sState; 
        private string sZip;
        private string sPhone;

        public string Addr1
        {
            get
            {
                Addr1 = sAddr1;
                return Addr1;
            }
            set
            {
                sAddr1 = value;
            }
        }
        public string Addr2
        {
            get
            {
                Addr2 = sAddr2;
                return Addr2;
            }
            set
            {
                sAddr2 = value;
            }
        }
        public string City
        {
            get
            {
                City = sCity;
                return City;
            }
            set
            {
                sCity = value;
            }
        }
        public string CompanyName
        {
            get
            {
                CompanyName = sCompanyName;
                return CompanyName;
            }
            set
            {
                sCompanyName = value;
            }
        }
        public long ContractID
        {
            get
            {
                ContractID = lContractID;
                return ContractID;
            }
            set
            {
                lContractID = value;
            }
        }
        public string FName
        {
            get
            {
                FName = sFName;
                return FName;
            }
            set
            {
                sFName = value;
            }
        }
        public string LName
        {
            get
            {
                LName = sLName;
                return LName;
            }
            set
            {
                sLName = value;
            }
        }
        public long PayeeID
        {
            get
            {
                PayeeID = lPayeeID;
                return PayeeID;
            }
            set
            {
                lPayeeID = value;
            }
        }
        public string Phone
        {
            get
            {
                Phone = sPhone;
                return Phone;
            }
            set
            {
                sPhone = value;
            }
        }
        public string State
        {
            get
            {
                State = sState;
                return State;
            }
            set
            {
                sState = value;
            }
        }
        public string Zip
        {
            get
            {
                Zip = sZip;
                return Zip;
            }
            set
            {
                sZip = value;
            }
        }

        public void AddCustomerToPayee()
        {
            OpenContract();
            if (clC.RowCount() > 0)
            {
                SQL = "select * from payee where contractid = " + clC.GetFields("contractid");
                clP.OpenDB(SQL, Global.sCON);
                if (clP.RowCount() == 0)
                {
                    clP.NewRow();
                }
                else
                {
                    clP.GetRow();
                }
                clP.SetFields("payeetype", "Customer");
                clP.SetFields("companyname", "");
                clP.SetFields("fname", clC.GetFields("fname"));
                clP.SetFields("lname", clC.GetFields("lname"));
                clP.SetFields("addr1", clC.GetFields("addr1"));
                clP.SetFields("addr2", clC.GetFields("addr2"));
                clP.SetFields("city", clC.GetFields("city"));
                clP.SetFields("state", clC.GetFields("state"));
                clP.SetFields("zip", clC.GetFields("zip"));
                clP.SetFields("phone", clC.GetFields("phone"));
                clP.SetFields("contractid", clC.GetFields("contractid"));
                if (clP.RowCount() == 0)
                {
                    clP.AddRow();
                }
                clP.SaveDB();
                SQL = "select * from payee where contractid = " + clC.GetFields("contractid");
                clP.OpenDB(SQL, Global.sCON);
                if (clP.RowCount() > 0)
                {
                    clP.GetRow();
                    lPayeeID = long.Parse(clP.GetFields("payeeid"));
                }
            }
        }

        public void AddDealerToPayee()
        {
            OpenContract();
            if (clC.RowCount() > 0)
            {
                OpenDealer();
                if (clD.RowCount() > 0)
                {
                    SQL = "select * from payee where dealerid = " + clC.GetFields("dealerid");
                    clP.OpenDB(SQL, Global.sCON);
                    if (clP.RowCount() == 0)
                    {
                        clP.NewRow();
                    }
                    else
                    {
                        clP.GetRow();
                    }
                    clP.SetFields("payeetype", "Dealer");
                    clP.SetFields("companyname", clD.GetFields("dealername"));
                    clP.SetFields("addr1", clD.GetFields("addr1"));
                    clP.SetFields("addr2", clD.GetFields("addr2"));
                    clP.SetFields("city", clD.GetFields("city"));
                    clP.SetFields("state", clD.GetFields("state"));
                    clP.SetFields("zip", clD.GetFields("zip"));
                    clP.SetFields("phone", clD.GetFields("phone"));
                    clP.SetFields("dealerid", clD.GetFields("dealerid"));
                    if (clP.RowCount() == 0)
                    {
                        clP.AddRow();
                    }
                    clP.SaveDB();
                    SQL = "select * from payee where dealerid = " + clD.GetFields("dealerid");
                    clP.OpenDB(SQL, Global.sCON);
                    if (clP.RowCount() > 0)
                    {
                        clP.GetRow();
                        lPayeeID = long.Parse(clP.GetFields("payeeid"));
                    }
                }
            }
        }
        public void AddOtherToPayee()
        {
            SQL = "select * from payee where payeetype = 'Other' ";
            if (sCompanyName.Length > 0)
            {
                SQL = SQL + "and companyname = '" + sCompanyName + "' ";
                goto MoveHere;
            }
            if (sFName.Length > 0 && sLName.Length > 0)
            {
                SQL = SQL + "and fname = '" + sFName + "' ";
                SQL = SQL + "and lname = '" + sLName + "' ";
                goto MoveHere;
            }
            return;
        MoveHere:;
            clP.OpenDB(SQL, Global.sCON);
            if (clP.RowCount() > 0)
            {
                clP.GetRow();
            }
            else
            {
                clP.NewRow();
            }
            clP.SetFields("payeetype", "Other");
            clP.SetFields("companyname", sCompanyName);
            clP.SetFields("fname", sFName);
            clP.SetFields("lname", sLName);
            clP.SetFields("addr1", sAddr1);
            clP.SetFields("addr2", sAddr2);
            clP.SetFields("city", sCity);
            clP.SetFields("state", sState);
            clP.SetFields("zip", sZip);
            clP.SetFields("phone", sPhone);
            if (clP.RowCount() == 0)
            {
                clP.AddRow();
            }
            clP.SaveDB();
            SQL = "select * from payee where payeetype = 'Other' ";
            if (sCompanyName.Length > 0)
            {
                SQL = SQL + "and companyname = '" + sCompanyName + "' ";
            }
            if (sFName.Length > 0 && sLName.Length > 0)
            {
                SQL = SQL + "and fname = '" + sFName + "' ";
                SQL = SQL + "and lname = '" + sLName + "' ";
            }
            clP.OpenDB(SQL, Global.sCON);
            if (clP.RowCount() > 0)
            {
                clP.GetRow();
                lPayeeID = long.Parse(clP.GetFields("payeeid"));
            }
        }
        private void OpenContract()
        {
            SQL = "select * from contract where contractid = " + lContractID;
            clC.OpenDB(SQL, Global.sCON);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
            }
        }
        private void OpenDealer()
        {
            SQL = "select * from dealer where dealerid = " + clC.GetFields("dealerid");
            clD.OpenDB(SQL, Global.sCON);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
            }
        }
        public void OpenPayeeInfo()
        {
            SQL = "select * from payee where payeeid = " + lPayeeID;
            clP.OpenDB(SQL, Global.sCON);
            if (clP.RowCount() > 0)
            {
                clP.GetRow();
                sCompanyName = clP.GetFields("companyname");
                sFName = clP.GetFields("fname");
                sLName = clP.GetFields("lname");
                sAddr1 = clP.GetFields("addr1");
                sAddr2 = clP.GetFields("addr2");
                sCity = clP.GetFields("city");
                sState = clP.GetFields("state");
                sZip = clP.GetFields("zip");
                sPhone = clP.GetFields("phone");
            }
        }
    }
}
