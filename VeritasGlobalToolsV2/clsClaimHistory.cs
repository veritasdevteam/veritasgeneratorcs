﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritasGlobalToolsV2
{
    public class clsClaimHistory
    {
        private long lClaimID;
        private string sFieldName;
        private string sPrevFieldInfo;
        private string sFieldInfo;
        private long lCreBy;

        public long ClaimID
        {
            get
            {
                ClaimID = lClaimID;
                return ClaimID;
            }
            set
            {
                lClaimID = value;
            }
        }
        public string FieldName
        {
            get
            {
                FieldName = sFieldName;
                return FieldName;
            }
            set
            {
                sFieldName = value;
            }
        }
        public string PrevFieldInfo
        {
            get
            {
                PrevFieldInfo = sPrevFieldInfo;
                return PrevFieldInfo;
            }
            set
            {
                sPrevFieldInfo = value;
            }
        }
        public string FieldInfo
        {
            get
            {
                FieldInfo = sFieldInfo;
                return FieldInfo;
            }
            set
            {
                sFieldInfo = value;
            }
        }
        public long CreBy
        {
            get
            {
                CreBy = lCreBy;
                return CreBy;
            }
            set
            {
                lCreBy = value;
            }
        }

        public void AddClaimHistory()
        {
            string SQL;
            clsDBO.clsDBO clCH = new clsDBO.clsDBO();

            SQL = "select * from claimhistory " +
                  "where claimhistoryid = 0 ";
            clCH.OpenDB(SQL, Global.sCON);
            if (clCH.RowCount() == 0)
            {
                clCH.NewRow();
                clCH.SetFields("claimid", lClaimID.ToString());
                clCH.SetFields("fieldname", sFieldName);
                clCH.SetFields("prevfieldinfo", sPrevFieldInfo);
                clCH.SetFields("fieldinfo", sFieldInfo);
                clCH.SetFields("credate", DateTime.Now.ToString());
                clCH.SetFields("creby", lCreBy.ToString());
                clCH.AddRow();
                clCH.SaveDB();
            }
        }
    }
}
