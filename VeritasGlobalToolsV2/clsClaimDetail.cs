﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritasGlobalToolsV2
{
    public class clsClaimDetail
    {
        private string SQL;
        public bool Refresh { get; private set; }
        public void ProcessClaim(long xClaimID)
        {
            string sStatus;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            Refresh = false;
            sStatus = "Closed";
            SQL = "select * from claimdetail " +
                  "where claimdetailstatus = 'Authorized' " +
                  "and claimid = " + xClaimID;
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() > 0)
            {
                sStatus = "Adjudicated";
            }
            SQL = "select * from claimdetail " +
                  "where claimdetailstatus = 'Approved' " +
                  "and claimid = " + xClaimID;
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() > 0)
            {
                sStatus = "Adjudicated";
            }
            SQL = "select * from claimdetail " +
                  "where claimdetailstatus = 'Transmitted' " +
                  "and claimid = " + xClaimID;
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() > 0)
            {
                sStatus = "Adjudicated";
            }
            SQL = "select * from claimdetail " +
                  "where claimdetailstatus = 'Requested' " +
                  "and claimid = " + xClaimID;
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() > 0)
            {
                sStatus = "Open";
            }
            SQL = "select * from claim where claimid = " + xClaimID;
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("status") != sStatus)
                {
                    Refresh = true;
                }
            }
            SQL = "update claim " +
                  "set status = '" + sStatus + "' " +
                  "where claimid = " + xClaimID;
            clR.RunSQL(SQL, Global.sCON);
            if (sStatus == "Closed")
            {
                SQL = "select * from claimdetail " +
                      "where claimdetailstatus = 'Paid' " +
                      "and claimid = " + xClaimID;
                clR.OpenDB(SQL, Global.sCON);
                if (clR.RowCount() > 0)
                {
                    SQL = "update claim " +
                          "set claimactivityid = 56 " +
                          "where claimid = " + xClaimID;
                    clR.RunSQL(SQL, Global.sCON);
                }
            }
        }
    }
}
